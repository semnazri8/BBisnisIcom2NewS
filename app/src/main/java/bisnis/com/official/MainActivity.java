package bisnis.com.official;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import bisnis.com.official.Fragment.FragmentDirectoryParent;
import bisnis.com.official.Fragment.FragmentPromo;
import bisnis.com.official.Fragment.Fragment_Chanel;
import bisnis.com.official.Fragment.Fragment_Home;
import bisnis.com.official.Fragment.Fragment_Profile;
import bisnis.com.official.Utilities.BottomNavigationViewHelper;

public class MainActivity extends AppCompatActivity {
    public static final String PREFS_PRIVATE = "PREFS_PRIVATE";
    public static BottomNavigationView btmView;
    FragmentManager fragmentManager;
    String aaaa;
    private FragmentTransaction ft;
    private SharedPreferences prefsprivate;
    private String darimantan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        btmView = findViewById(R.id.navigation);
        Intent i = getIntent();
        darimantan = i.getStringExtra("frgToLoad");

        BottomNavigationViewHelper.disableShiftMode(btmView);

        if (darimantan == null) {
            loadFragment(new Fragment_Home());
        } else {
            loadFragment(new Fragment_Profile());
            btmView.setSelectedItemId(R.id.profile);
        }

        btmView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.home:
                        loadFragment(new Fragment_Home());
                        break;
                    case R.id.directiroy:
                        loadFragment(new FragmentDirectoryParent());
                        break;
                    case R.id.chanels:
                        loadFragment(new Fragment_Chanel());
                        break;
                    case R.id.extras:
                        //TODO : Replace with Fragment
                        loadFragment(new FragmentPromo());
                        break;

                    case R.id.profile:
                        loadFragment(new Fragment_Profile());
                        //TODO : Replace with Fragment
                        break;
                }
                return true;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.container_body);
        fragment.onActivityResult(requestCode, resultCode, data);

    }

    private void loadFragment(Fragment fragment) {
        fragmentManager = getSupportFragmentManager();
        ft = fragmentManager.beginTransaction();
        ft.replace(R.id.container_body, fragment, "home").addToBackStack("menu");
        ft.commit();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

//        Fragment_Home.searchView.openSearch();
//        Fragment_Home.searchView.getCurrentQuery();
    }

    @Override
    public void onBackPressed() {

        int backStackCount = getSupportFragmentManager().getBackStackEntryCount();

        FragmentManager fm = getSupportFragmentManager();
        Fragment f = fm.findFragmentById(R.id.container_body); // get the fragment that is currently loaded in placeholder
        Object tag = f.getTag();

        if (darimantan != null) {
            finish();
        } else if (backStackCount >= 2) {
            if (tag.equals("chanel") || tag.equals("profile")) {
                fm.popBackStack();
            } else {
                fragmentManager.popBackStack();
                FragmentManager.BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(
                        1);
                getSupportFragmentManager().popBackStack(entry.getId(),
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager().executePendingTransactions();
//            btmView.setSelectedItemId(R.id.home);
                btmView.getMenu().findItem(R.id.home).setChecked(true);

            }

        } else
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage("Do you want to close this application?")
                    .setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int wich) {
                                    finish();

                                }
                            }).setNegativeButton("No", null).show();
    }
}

