package bisnis.com.official.Model;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 09/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class ModelChanel {

    public static final int ChanelList = 0;
    public static final int ChanelGrid = 1;


    public int type;
    public int data;
    public int kanal_id;
    public String text;

    public ModelChanel(int type, String text, int data,int kanal_id) {
        this.type = type;
        this.data = data;
        this.text = text;
        this.kanal_id = kanal_id;
    }

}
