package bisnis.com.official.Model;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 09/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class ModelData {

    public static final int News_Thumbnail = 0;
    public static final int News_list = 1;
    public static final int News_grid = 2;
    public static final int News_ads = 3;
    public static final int NewsRecycler = 4;
    public static final int NewsIndex = 5;



    public int type;
    public int data;
    public String text;

    public ModelData(int type, String text, int data) {
        this.type = type;
        this.data = data;
        this.text = text;
    }


}
