package bisnis.com.official.Model;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 26/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class ComentModel {
    private int img_user, comment_count, likes_count,like_satus;
    private String coment_date,coment_txt, user_name;

    public ComentModel(int img_user, int comment_count, int likes_count, String coment_date, String coment_txt, String user_name,int like_satus){
        this.img_user = img_user;
        this.comment_count = comment_count;
        this.likes_count = likes_count;
        this.coment_date = coment_date;
        this.coment_txt = coment_txt;
        this.user_name = user_name;
        this.like_satus = like_satus;
    }

    public void setComent_date(String coment_date) {
        this.coment_date = coment_date;
    }

    public int getComment_count() {
        return comment_count;
    }

    public void setComent_txt(String coment_txt) {
        this.coment_txt = coment_txt;
    }

    public int getImg_user() {
        return img_user;
    }

    public void setComment_count(int comment_count) {
        this.comment_count = comment_count;
    }

    public int getLikes_count() {
        return likes_count;
    }

    public void setImg_user(int img_user) {
        this.img_user = img_user;
    }

    public String getComent_date() {
        return coment_date;
    }

    public void setLikes_count(int likes_count) {
        this.likes_count = likes_count;
    }

    public String getComent_txt() {
        return coment_txt;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setLike_satus(int like_satus) {
        this.like_satus = like_satus;
    }

    public int getLike_satus() {
        return like_satus;
    }
}
