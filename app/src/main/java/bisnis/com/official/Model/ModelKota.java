package bisnis.com.official.Model;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 15/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class ModelKota {

    public static final int type_title = 0;
    public static final int type_list = 1;
    public static final int type_thumbnail = 2;


    public int type;
    public int img_data;
    public String text_title, text_lokasi, text_telepon,text_website;

    public ModelKota(int type, String text_title,String text_lokasi,String text_telepon,String text_website, int img_data) {
        this.type = type;
        this.img_data = img_data;
        this.text_title = text_title;
        this.text_lokasi = text_lokasi;
        this.text_telepon = text_telepon;
        this.text_website = text_website;
    }

}
