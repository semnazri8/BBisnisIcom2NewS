package bisnis.com.official.Model;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 12/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class ModelPromo {
    public static final int PromoList = 0;
    public static final int PromoGrid = 1;


    public int type;
    public int data;
    public String text_title_promo, txt_from_promo;

    public ModelPromo(int type, String text_title_promo, String txt_from_promo, int data) {
        this.type = type;
        this.data = data;
        this.text_title_promo = text_title_promo;
        this.txt_from_promo = txt_from_promo;
    }
}
