package bisnis.com.official.Model;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 14/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class ModelSpinnerKota {

    String item_name;
    int item_id;
    public ModelSpinnerKota() {
    }

    public ModelSpinnerKota(String item_name, int item_id) {
        this.item_name = item_name;
        this.item_id = item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_name() {
        return item_name;
    }
}
