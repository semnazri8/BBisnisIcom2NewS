package bisnis.com.official.Network;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class APIConstant {

    public static String API_PARENT = "http://services.bisnis.com/apps/";
    public static String API_Detail = "http://services.bisnis.com/json/newdetail/";
}
