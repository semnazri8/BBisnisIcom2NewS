package bisnis.com.official;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import bisnis.com.official.Adapter.SearchAdapter;
import bisnis.com.official.Fragment.Fragment_Home;
import bisnis.com.official.Listener.SearchOnClickListener;
import bisnis.com.official.Network.ConnectionDetector;
import bisnis.com.official.Pojo.Search;
import bisnis.com.official.Presenter.SearchPresenter;
import bisnis.com.official.Presenter.SearchPresenterImp;
import bisnis.com.official.Utilities.BisnisComOfficialPreferences;
import bisnis.com.official.Utilities.SpacesItemDecoration;
import bisnis.com.official.View.SearchView;

public class SeachActivity extends AppCompatActivity implements SearchView, SearchOnClickListener {

    public static final String PREFS_PRIVATE = "PREFS_PRIVATE";
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private ImageButton img_back, img_search;
    private SearchAdapter adapter;
    private EditText edt_search;
    private SearchPresenter presenter;
    private RecyclerView recyclerView, recent_rv;
    private LinearLayoutManager lm, lm_Recent;
    private MaterialDialog mDialog, dialog_muter;
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private SharedPreferences prefsprivate;
    private BisnisComOfficialPreferences preferences;
    private LinearLayout ll_recent;
    private String newString;
    private int page = 1;
    private int pagexx = 2;
    private boolean loading = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_search);

        cd = new ConnectionDetector(this);

        Intent x = getIntent();
        newString = x.getStringExtra("keyword");

        presenter = new SearchPresenterImp(this);
        preferences = new BisnisComOfficialPreferences();
        img_back = findViewById(R.id.img_back);
        img_search = findViewById(R.id.img_search);
        edt_search = findViewById(R.id.edt_search);
        recyclerView = findViewById(R.id.rv_search);
        recent_rv = findViewById(R.id.recent_rv);
        ll_recent = findViewById(R.id.ll_recent);

        lm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(lm);


        edt_search.setText(newString);

        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                        actionId == EditorInfo.IME_ACTION_DONE ||
                        actionId == EditorInfo.IME_ACTION_GO ||
                        actionId == EditorInfo.IME_ACTION_NEXT) {

                    String keyword_edt = edt_search.getText().toString();
                    checkconnection2(keyword_edt);
                    return true;
                }
                return false;
            }
        });


        if (!newString.equals("")) {
            String xoxo = newString.replace(" ", "%20");
            checkconnection(xoxo);
        }

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });
        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String xoxo = edt_search.getText().toString();
                checkconnection2(xoxo);

            }
        });


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    int lastIndex = lm.findLastCompletelyVisibleItemPosition() + 1;
                    int totalItem = adapter.getItemCount();

                    visibleItemCount = lm.getChildCount();
                    totalItemCount = adapter.getItemCount();
                    pastVisiblesItems = lm.findFirstVisibleItemPosition() + 1;

                    if (!loading && lastIndex == totalItem) {
//                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        loading = true;
//                        srl.setRefreshing(true);
                        Log.v("...", "Last Item Wow !");

                        String xoxo = edt_search.getText().toString().replace(" ", "%20");

                        presenter.getSearch(xoxo, String.valueOf(pagexx++));

                    }
                }


            }
        });
    }


    private void checkconnection(String keyhword) {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            //TODO: Update adapter biar bisa load more

            adapter = new SearchAdapter(this, this);
            recyclerView.setAdapter(adapter);
            loading = true;

            String newStrings = keyhword.replace(" ", "%20");
            presenter.getSearch(newStrings, String.valueOf(page));
            pagexx = 2;


        } else if (isInternetPresent.equals(false)) {
            getdialogerror("Tidak ada koneksi Internet");
        }
    }

    private void checkconnection2(String keyhword) {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            //TODO: Update adapter biar bisa load more

            adapter = new SearchAdapter(this, this);
            recyclerView.setAdapter(adapter);
            loading = true;

            String newStrings = keyhword.replace(" ", "%20");
            presenter.getSearch(newStrings, String.valueOf(page));
            pagexx = 2;
            Fragment_Home.searchView.saveQueryToDb(keyhword, System.currentTimeMillis());


        } else if (isInternetPresent.equals(false)) {
            getdialogerror("Tidak ada koneksi Internet");
        }
    }

    @Override
    public void ResultSearch(String response_message, ArrayList<Search> searchresponse) {
//        dialog_muter.dismiss();
        loading = false;
        if (searchresponse.size() > 0) {
            ll_recent.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            ParseType(searchresponse);
        }


    }

    private void ParseType(ArrayList<Search> searchresponse) {

        adapter.addOn(searchresponse);
    }

    @Override
    public void SearchError(String response_message) {
        dialog_muter.dismiss();
        getdialogerror(response_message);

    }

    public void getDialog_progress() {

        dialog_muter = new MaterialDialog.Builder(this)
                .title(R.string.app_name)
                .cancelable(false)
                .content(R.string.please_wait)
                .progress(true, 0)
                .show();
    }

    private void getdialogerror(String response_message) {
        dialog_muter.dismiss();
        mDialog = new MaterialDialog.Builder(this)
                .title(R.string.app_name)
                .content(response_message)
                .cancelable(false)
                .positiveText("Close")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mDialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void OnCick(String post_date, String post_category, String post_id, ArrayList<Search> dataset, String position) {
        Intent i = new Intent(this, ActivityDeatilBeritaSearch.class);

        i.putExtra("index", position);
        i.putExtra("type_get", "search");
        i.putExtra("kanal_id", post_category);
        i.putExtra("parcel", dataset);
        i.putExtra("helo", "breaking");
        startActivity(i);
    }
}
