package bisnis.com.official;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.messaging.FirebaseMessaging;

import bisnis.com.official.Network.ConnectionDetector;
import bisnis.com.official.Pojo.VersionControl;
import bisnis.com.official.Presenter.VersionControlPresenter;
import bisnis.com.official.Presenter.VersionControlPresenterImp;
import bisnis.com.official.Utilities.DatabaseHelper;
import bisnis.com.official.View.VersionControlView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 07/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class SplashScreen extends AppCompatActivity implements VersionControlView {
    private final int SPLASH_DISPLAY_LENGHT = 900;
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private MaterialDialog mDialog;
    private VersionControlPresenter versionControlPresenter;
    private DatabaseHelper db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash);

        cd = new ConnectionDetector(this);
        versionControlPresenter = new VersionControlPresenterImp(this);
        db = new DatabaseHelper(this);

//        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

//        Log.d("plerkadut",refreshedToken);

        checkconnections();

    }

    private void checkconnections() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {

            versionControlPresenter.doCheckVersion("android");
            //TODO : Dev
//            FirebaseMessaging.getInstance().subscribeToTopic("Android-Breaking");
            //TODO : Production
            FirebaseMessaging.getInstance().subscribeToTopic("breaking");


        } else if (isInternetPresent.equals(false)) {
            getdialogerror("Tidak ada koneksi Internet");

        }
    }


    private void getdialogerror(String response_message) {
        mDialog = new MaterialDialog.Builder(this)
                .title(R.string.app_name)
                .content(response_message)
                .positiveText("Close")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mDialog.dismiss();

                    }
                })
                .show();
    }

    @Override
    public void ResultVersion(String response_message, VersionControl versionControl) {
        String versionName = "";

        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
            Log.d("masuk", versionName);

//                        versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = versionControl.getVersion();
        Log.d("version__", versionName);
        Log.d("versi server", version);

        if (version.equals(versionName)) {

            Thread timerThread = new Thread() {
                public void run() {
                    try {
                        sleep(SPLASH_DISPLAY_LENGHT);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        mulailah();

                    }
                }
            };
            timerThread.start();
        } else {
            update();
        }
    }

    private void mulailah() {
        Intent intent = new Intent(SplashScreen.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void update() {
        new AlertDialog.Builder(SplashScreen.this)
                .setIcon(R.drawable.ic_new_releases_black_24dp)
                .setTitle("Please Update")
                .setMessage("New Version has Avaliable! Please update, to enjoy the newest features")
                .setPositiveButton("Update Now!",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int wich) {
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=bisnis.com.official")));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=bisnis.com.official")));
                                }
                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int wich) {
                        mulailah();
                    }
                }).show();

    }

    @Override
    public void DetailError(String response_message) {

    }
}

