package bisnis.com.official;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import bisnis.com.official.Adapter.ListBeritaParentAdapter;
import bisnis.com.official.Fragment.Fragment_detail;
import bisnis.com.official.Network.ConnectionDetector;
import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.Pojo.HeadlineResponse;
import bisnis.com.official.Presenter.BreakingPresenter;
import bisnis.com.official.Presenter.BreakingPresenterImp;
import bisnis.com.official.View.BreakingView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 14/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class ActivityDeatilBeritaH extends AppCompatActivity implements BreakingView {
    public static int halamannya;
    public static String type_json, type_get; //TODO : ini nanti buat dynamic load dari mana dia bisa buat ke kanal juga nih
    private ViewPager viewPager;
    private BreakingPresenter breakingPresenter;
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private MaterialDialog mDialog, dialog_muter;
    private ListBeritaParentAdapter adapter;
    private String date, cat_id, post_id, index, kanal_form;
    private ArrayList<HeadlineResponse> headline_sini;
    private Serializable parcel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detail);
        viewPager = findViewById(R.id.pager_halaman);
        cd = new ConnectionDetector(this);
        breakingPresenter = new BreakingPresenterImp(this);
        Intent i = getIntent();
        index = i.getStringExtra("index");
        type_get = i.getStringExtra("type_get");
        type_json = i.getStringExtra("kanal_id");
        kanal_form = i.getStringExtra("helo");
//        Log.d("jeas", kanal_form);
//        parcel = i.getSerializableExtra("parcel");
//        if (kanal_form.equals("breaking")) {
//            breaking_sini = (ArrayList<BreakingResponse>) i.getSerializableExtra("parcel");
//            createPage();
//        } else if (kanal_form.equals()) {
//            breaking_sini = (ArrayList<BreakingResponse>) i.getSerializableExtra("parcel");
        headline_sini = (ArrayList<HeadlineResponse>) i.getSerializableExtra("parcel");
//            createPage();
        createPageH();
//        }


//        kanal_form = i.getStringExtra("kanal_form");


        Log.d("type_get_bro", type_get);
        Log.d("kanal_id_bro", type_json);

//        checkConnections(type_json);

    }

    private void createPageH() {
        List<HashMap<String, Fragment>> halamannya = new ArrayList<HashMap<String, Fragment>>();

        for (int i = 0; i < headline_sini.size(); i++) {
            viewPager.setOffscreenPageLimit(0);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date mDate = sdf.parse(headline_sini.get(i).getPostDate());

                DateFormat format = new SimpleDateFormat("yyyyMMdd");
                String newformat = format.format(mDate);
                date = newformat;
                Log.d("datesa", date);

            } catch (ParseException e) {
                e.printStackTrace();
            }


            cat_id = headline_sini.get(i).getCategoryId();
            post_id = headline_sini.get(i).getPostId();

            Log.d("aaaaaassssuuu", i + "-" + post_id);


            Fragment_detail fragment = new Fragment_detail();
            Bundle bundle = new Bundle();
            bundle.putString("post_date", date);
            bundle.putString("categoty_id", cat_id);
            bundle.putString("post_id", post_id);
            bundle.putString("title", type_get);

            fragment.setArguments(bundle);
            HashMap<String, Fragment> halaman = new HashMap<String, Fragment>();
            halaman.put("HAL." + (i + 1), fragment);
            halamannya.add(halaman);

            adapter = new ListBeritaParentAdapter(getSupportFragmentManager());
            adapter.addFragment(halamannya);
            viewPager.setAdapter(adapter);
        }

        if (type_json.equals("0")) {

            if (Integer.parseInt(index) <= 3) {
                viewPager.setCurrentItem(Integer.parseInt(index));
            } else {
                viewPager.setCurrentItem(Integer.parseInt(index) - 1);

            }
        } else if (type_json.equals("244")) {
            viewPager.setCurrentItem(Integer.parseInt(index) - 1);
        } else {
            if (type_get.equals("Terbaru")) {
                viewPager.setCurrentItem(Integer.parseInt(index) - 1);
            } else {
                viewPager.setCurrentItem(Integer.parseInt(index));
            }
        }
    }


    private void checkConnections(String type_json) {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
//            getDialog_progress();
            //TODO : nanti ini untuk set breaking, headline, dll
            breakingPresenter.getBreaking(type_json, type_get);
            Log.d("shanks", type_get);

        } else if (isInternetPresent.equals(false)) {
            getdialogerror("Tidak ada koneksi Internet");

        }
    }

    @Override
    public void ResultBreaking(String response_message, ArrayList<BreakingResponse> breakingResponse) {
//        dialog_muter.dismiss();
        List<HashMap<String, Fragment>> halamannya = new ArrayList<HashMap<String, Fragment>>();

        for (int i = 0; i < breakingResponse.size(); i++) {
            String year = breakingResponse.get(i).getDate().getYear();
            String month = breakingResponse.get(i).getDate().getMonth();
            String day = breakingResponse.get(i).getDate().getDay();

            date = year + month + day;
            cat_id = breakingResponse.get(i).getCategoryId();
            post_id = breakingResponse.get(i).getPostId();

            Log.d("post_ida", post_id);

            Fragment_detail fragment = new Fragment_detail();
            Bundle bundle = new Bundle();
            bundle.putString("post_date", date);
            bundle.putString("categoty_id", cat_id);
            bundle.putString("post_id", post_id);
            bundle.putString("title", type_get);

            fragment.setArguments(bundle);
            HashMap<String, Fragment> halaman = new HashMap<String, Fragment>();
            halaman.put("HAL." + (i + 1), fragment);
            halamannya.add(halaman);

            adapter = new ListBeritaParentAdapter(getSupportFragmentManager());
            adapter.addFragment(halamannya);
            viewPager.setAdapter(adapter);

            if (type_json.equals("0")) {
                if (Integer.parseInt(index) <= 3) {
                    viewPager.setCurrentItem(Integer.parseInt(index));
                } else {
                    viewPager.setCurrentItem(Integer.parseInt(index) - 1);

                }
            } else if (type_json.equals("244")) {
                viewPager.setCurrentItem(Integer.parseInt(index) - 1);
            } else {
                if (type_get.equals("Terbaru")) {
                    viewPager.setCurrentItem(Integer.parseInt(index) - 1);
                } else {
                    viewPager.setCurrentItem(Integer.parseInt(index));
                }
            }
        }
    }

    @Override
    public void BreakingError(String response_message) {
        getdialogerror(response_message);
    }

    public void getDialog_progress() {

        dialog_muter = new MaterialDialog.Builder(this)
                .title(R.string.app_name)
                .content(R.string.please_wait)
                .progress(true, 0)
                .show();
    }

    private void getdialogerror(String response_message) {
        dialog_muter.dismiss();
        mDialog = new MaterialDialog.Builder(this)
                .title(R.string.app_name)
                .content(response_message)
                .positiveText("Close")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mDialog.dismiss();

                    }
                })
                .show();
    }
}
