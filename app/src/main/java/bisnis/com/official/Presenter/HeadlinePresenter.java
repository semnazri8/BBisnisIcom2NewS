package bisnis.com.official.Presenter;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface HeadlinePresenter {
    void getHeadline(String url, String type_get);
    void onDestroy();
}
