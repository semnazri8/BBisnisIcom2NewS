package bisnis.com.official.Presenter;

import bisnis.com.official.Pojo.LoginResponse;
import bisnis.com.official.Pojo.ReadResponse;
import bisnis.com.official.View.DetailView;
import bisnis.com.official.View.LoginView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 14/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class LoginPresenterImp implements LoginPresenter, LoginInteractor.OnSuccessLoginListener {
    private LoginView loginView;
    private LoginInteractor loginInteractor;

    public LoginPresenterImp(LoginView view) {
        this.loginView = view;
        this.loginInteractor = new LoginInteractorImp();
    }

    @Override
    public void doLogin(String username, String password) {
        loginInteractor.doLogin(this, username, password);
    }

    @Override
    public void onDestroy() {
        loginView = null;
    }


    @Override
    public void onAllError() {
        if (loginView != null){
            loginView.setAllError();
        }
    }

    @Override
    public void onUsernameError() {
        if (loginView != null){
            loginView.setUsernameError();
        }
    }

    @Override
    public void onUsernameInvalid() {
        if (loginView != null){
            loginView.setUsernameInvalid();
        }

    }

    @Override
    public void onPasswordError() {
        if (loginView != null){
            loginView.setPasswordError();
        }
    }

    @Override
    public void onPasswordInValid() {
        if (loginView != null){
            loginView.setPasswordInvalid();
        }
    }

    @Override
    public void onValid() {
        if (loginView != null){
            loginView.setvalid();
        }
    }

    @Override
    public void onSuccess(String response_message, LoginResponse loginResponse) {
        if (loginView != null){
            loginView.ResultLogin(response_message,loginResponse);
        }
    }

    @Override
    public void onelseError(String response_message) {
        if (loginView != null) {
            loginView.DetailError(response_message);
        }
    }
}
