package bisnis.com.official.Presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import bisnis.com.official.Interface.BreakingInterface;
import bisnis.com.official.Network.APIConstant;
import bisnis.com.official.Pojo.DateHT0;
import bisnis.com.official.Pojo.Search;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class SearchInteractorImp implements SearchInteractor {
    String response_message;
    ArrayList<Search> searches;

    @Override
    public void getSearch(final OnSuccessgetSearchListener listener, String keyword, String page) {
        searches = new ArrayList<>();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .retryOnConnectionFailure(true)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIConstant.API_PARENT)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();


        BreakingInterface service = retrofit.create(BreakingInterface.class);
        Call<ArrayList<Search>> call = service.getSearch(keyword, page);


        call.enqueue(new Callback<ArrayList<Search>>() {
            @Override
            public void onResponse(Call<ArrayList<Search>> call, Response<ArrayList<Search>> response) {

                if (response.isSuccessful()) {

//                    if (!String.valueOf(response.body().size()).equals(null)) {
                    for (int i = 0; i < response.body().size(); i++) {

                        Search data = new Search();
                        data.setPostId(response.body().get(i).getPostId());
                        data.setPostDate(response.body().get(i).getPostDate());
                        data.setTitle(response.body().get(i).getTitle());
                        data.setSummary(response.body().get(i).getSummary());
                        data.setCategoryId(response.body().get(i).getCategoryId());
                        data.setImageThumbnailUrl(response.body().get(i).getImageThumbnailUrl());


                        // For Date

                        DateHT0 date = new DateHT0();
                        date.setYear(response.body().get(i).getDate().getYear());
                        date.setMonth(response.body().get(i).getDate().getMonth());
                        date.setDay(response.body().get(i).getDate().getDay());
                        date.setHour(response.body().get(i).getDate().getHour());
                        date.setMinute(response.body().get(i).getDate().getMinute());
                        date.setSecond(response.body().get(i).getDate().getSecond());
                        date.setMinute(response.body().get(i).getDate().getMinute());
                        date.setDayOfWeek(response.body().get(i).getDate().getDayOfWeek());
                        date.setMonthIndName(response.body().get(i).getDate().getMonthIndName());
                        date.setDayIndName(response.body().get(i).getDate().getDayIndName());
                        data.setDate(date);

                        searches.add(data);


                    }
                    listener.onSuccess(response_message, searches);
//                    }

                } else {

                    switch (response.code()) {

                        case 401:
                            listener.onelseError("Something went Wrong 401!");
                            break;
                        case 404:
                            listener.onelseError("Cannot find the right path! Response code 404");
                            break;
                        case 500:
                            listener.onelseError("Server is broken! Response code 500");
                            break;
                        default:
                            listener.onelseError("Wrong Email or Password!");
                            break;
                    }
                }
            }
//            }

            @Override
            public void onFailure(Call<ArrayList<Search>> call, Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });
    }
}
