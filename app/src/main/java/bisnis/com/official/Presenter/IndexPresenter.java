package bisnis.com.official.Presenter;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface IndexPresenter {
    void getIndex(String day, String month,String year,String page,String limit, String category_id);
    void onDestroy();
}
