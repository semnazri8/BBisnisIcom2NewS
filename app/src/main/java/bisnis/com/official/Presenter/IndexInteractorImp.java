package bisnis.com.official.Presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import bisnis.com.official.Interface.BreakingInterface;
import bisnis.com.official.Network.APIConstant;
import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.Pojo.DateBreaking;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class IndexInteractorImp implements IndexInteractor {
    String response_message;
    ArrayList<BreakingResponse> responses_breaking;

    @Override
    public void getIndex(final OnSuccessgetIndexListener listener, String day, String month, String year, String page, String limit, String category_id) {
        responses_breaking = new ArrayList<>();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .retryOnConnectionFailure(true)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIConstant.API_PARENT)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();


        BreakingInterface service = retrofit.create(BreakingInterface.class);

        Call<ArrayList<BreakingResponse>> call = service.getIndex(day,month,year,page,limit,category_id);

        call.enqueue(new Callback<ArrayList<BreakingResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<BreakingResponse>> call, Response<ArrayList<BreakingResponse>> response) {

//                if (type_get.equals("Terbaru") || type_get.equals("Headline")) {
                if (response.isSuccessful()) {

                    for (int i = 0; i < response.body().size(); i++) {
                        BreakingResponse data = new BreakingResponse();
                        data.setPostId(response.body().get(i).getPostId());
                        data.setCategoryId(response.body().get(i).getCategoryId());
                        data.setCategoryName(response.body().get(i).getCategoryName());
                        data.setPostDate(response.body().get(i).getPostDate());
                        data.setCategoryParentName(response.body().get(i).getCategoryParentName());
                        data.setIsLive(response.body().get(i).getIsLive());
                        data.setTitle(response.body().get(i).getTitle());
                        data.setSubtitle(response.body().get(i).getSubtitle());
                        data.setSlug(response.body().get(i).getSlug());
                        data.setSummary(response.body().get(i).getSummary());
                        data.setImageThumbnail(response.body().get(i).getImageThumbnail());
                        data.setImageContent(response.body().get(i).getImageContent());
                        data.setImageThumbnailUrl(response.body().get(i).getImageThumbnailUrl());
                        data.setImageContentUrl(response.body().get(i).getImageContentUrl());


                        // For Date

                        DateBreaking dateBreaking = new DateBreaking();
                        dateBreaking.setYear(response.body().get(i).getDate().getYear());
                        dateBreaking.setMonth(response.body().get(i).getDate().getMonth());
                        dateBreaking.setDay(response.body().get(i).getDate().getDay());
                        dateBreaking.setHour(response.body().get(i).getDate().getHour());
                        dateBreaking.setMinute(response.body().get(i).getDate().getMinute());
                        dateBreaking.setSecond(response.body().get(i).getDate().getSecond());
                        dateBreaking.setMinute(response.body().get(i).getDate().getMinute());
                        dateBreaking.setDayOfWeek(response.body().get(i).getDate().getDayOfWeek());
                        dateBreaking.setMonthIndName(response.body().get(i).getDate().getMonthIndName());
                        dateBreaking.setDayIndName(response.body().get(i).getDate().getDayIndName());
                        data.setDate(dateBreaking);
                        data.setType(response.body().get(i).getType());

                        data.setType_get("index");
                        responses_breaking.add(data);
                    }

                    listener.onSuccess(response_message, responses_breaking);

                } else {

                    switch (response.code()) {

                        //TODO ini tolong contextnya di benerin

                        case 401:
                            listener.onelseError("Wrong Email or Password!");
                            break;
                        case 404:
                            listener.onelseError("Cannot find the right path! Response code 404");
                            break;
                        case 500:
                            listener.onelseError("Server is broken! Response code 500");
                            break;
                        default:
                            listener.onelseError("Wrong Email or Password!");
                            break;
                    }
                }
            }
//            }

            @Override
            public void onFailure(Call<ArrayList<BreakingResponse>> call, Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });
    }
}
