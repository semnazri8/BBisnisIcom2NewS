package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.Pojo.HeadlineResponse;
import bisnis.com.official.View.HeadlineVIew;
import bisnis.com.official.View.KanalView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class HeadlinePresenterImp implements HeadlinePresenter, HeadlineInteractor.OnSuccessgeHeadlineListener {

    private HeadlineVIew headlineVIew;
    private HeadlineInteractor headlineInteractor;

    public HeadlinePresenterImp(HeadlineVIew headlineVIew) {
        this.headlineVIew = headlineVIew;
        this.headlineInteractor = new HeadlineInteractorImp();
    }

    @Override
    public void getHeadline(String url, String type_get) {
        headlineInteractor.getHeadline(this, url,type_get);
    }

    @Override
    public void onDestroy() {
        headlineVIew = null;
    }

    @Override
    public void onSuccessHeadline(String response_message, ArrayList<HeadlineResponse> headlineResponses) {
        if (headlineVIew != null) {
            headlineVIew.ResultHeadline(response_message, headlineResponses);
        }
    }

    @Override
    public void onelseError(String response_message) {
        if (headlineVIew != null) {
        headlineVIew.HeadlineError(response_message);
        }
    }
}
