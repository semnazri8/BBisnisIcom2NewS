package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.View.BreakingView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class BreakingPresenterImp implements BreakingPresenter, BreakingInteractor.OnSuccessgetBreakingListener {

    private BreakingView breakingView;
    private BreakingInteractor breakingInteractor;

    public BreakingPresenterImp(BreakingView breakingView) {
        this.breakingView = breakingView;
        this.breakingInteractor = new BreakingInteractorImp();
    }

    @Override
    public void getBreaking(String url, String type_get) {
        breakingInteractor.getBreaking(this, url,type_get);
    }

    @Override
    public void onDestroy() {
        breakingView = null;
    }

    @Override
    public void onSuccess(String response_message, ArrayList<BreakingResponse> breakingResponse) {
        if (breakingView != null) {
            breakingView.ResultBreaking(response_message, breakingResponse);
        }
    }

    @Override
    public void onelseError(String response_message) {
        if (breakingView != null) {
        breakingView.BreakingError(response_message);
        }
    }
}
