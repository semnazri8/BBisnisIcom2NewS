package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.View.IndexView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class IndexPresenterImp implements IndexPresenter, IndexInteractor.OnSuccessgetIndexListener {

    private IndexView indexView;
    private IndexInteractor indexInteractor;

    public IndexPresenterImp(IndexView indexView) {
        this.indexView = indexView;
        this.indexInteractor = new IndexInteractorImp();
    }


    @Override
    public void getIndex(String day, String month, String year, String page, String limit, String category_id) {
        indexInteractor.getIndex(this, day, month, year, page, limit, category_id);
    }

    @Override
    public void onDestroy() {
        indexView = null;
    }

    @Override
    public void onSuccess(String response_message, ArrayList<BreakingResponse> breakingResponse) {
        if (indexView != null) {
            indexView.ResultIndex(response_message, breakingResponse);
        }
    }

    @Override
    public void onelseError(String response_message) {
        if (indexView != null) {
            indexView.IndexError(response_message);
        }
    }
}
