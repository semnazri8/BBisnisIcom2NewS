package bisnis.com.official.Presenter;

import bisnis.com.official.Pojo.VersionControl;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface VersionControlInteractor {
    void CheckVersionCOntrol(OnSuccessCheckListener listener, String platform);

    interface OnSuccessCheckListener {

        void onSuccess(String response_message, VersionControl versionControl);

        void onelseError(String response_message);

    }
}
