package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.Pojo.Search;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface SearchInteractor {
    void getSearch(OnSuccessgetSearchListener listener, String keyword,String page);

    interface OnSuccessgetSearchListener {

        void onSuccess(String response_message, ArrayList<Search> searchResponse);

        void onelseError(String response_message);

    }
}
