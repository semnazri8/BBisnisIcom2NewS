package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.Pojo.HotelResponse;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface HotelInteractor {
    void getHotel(OnSuccessgetHotelListener listener, String prov_id);

    interface OnSuccessgetHotelListener {

        void onSuccess(String response_message, ArrayList<HotelResponse> hotelResponses);

        void onelseError(String response_message);

    }
}
