package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.HotelResponse;
import bisnis.com.official.Pojo.ProvinceResponse;
import bisnis.com.official.View.HotelView;
import bisnis.com.official.View.ProvinceView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class ProvincePresenterImp implements ProvincePresenter, ProvinceInteractor.OnSuccessgetProvinceListener {

    private ProvinceView provinceView;
    private ProvinceInteractor provinceInteractor;

    public ProvincePresenterImp(ProvinceView provinceView) {
        this.provinceView = provinceView;
        this.provinceInteractor = new ProvinceInteractorImp();
    }



    @Override
    public void getProvince() {
        provinceInteractor.getProvince(this);
    }

    @Override
    public void onDestroy() {
        provinceView = null;
    }


    @Override
    public void onSuccess(String response_message, ArrayList<ProvinceResponse> provinceResponses) {
        if (provinceView != null) {
            provinceView.ResultProvince(response_message, provinceResponses);
        }
    }

    @Override
    public void onelseError(String response_message) {
        if (provinceView != null) {
        provinceView.ProvinceError(response_message);
        }
    }
}
