package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface IndexInteractor {
    void getIndex(OnSuccessgetIndexListener listener, String day, String month,String year,String page,String limit, String category_id);

    interface OnSuccessgetIndexListener {

        void onSuccess(String response_message, ArrayList<BreakingResponse> breakingResponse);

        void onelseError(String response_message);

    }
}
