package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface BreakingInteractor {
    void getBreaking(OnSuccessgetBreakingListener listener,String url,String type_get);

    interface OnSuccessgetBreakingListener {

        void onSuccess(String response_message, ArrayList<BreakingResponse> breakingResponse);

        void onelseError(String response_message);

    }
}
