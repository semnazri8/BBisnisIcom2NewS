package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.View.BreakingView;
import bisnis.com.official.View.KanalView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class KanalPresenterImp implements KanalPresenter, KanalInteractor.OnSuccessgetBreakingListener {

    private KanalView kanalView;
    private KanalInteractor kanalInteractor;

    public KanalPresenterImp(KanalView kanalView) {
        this.kanalView = kanalView;
        this.kanalInteractor = new KanalInteractorImp();
    }

    @Override
    public void getHeadline(String url, String type_get) {
        kanalInteractor.getKanal(this, url,type_get);
    }

    @Override
    public void onDestroy() {
        kanalView = null;
    }

    @Override
    public void onSuccess(String response_message, ArrayList<BreakingResponse> kanalResponse) {
        if (kanalView != null) {
            kanalView.ResultKanal(response_message, kanalResponse);
        }
    }

    @Override
    public void onelseError(String response_message) {
        if (kanalView != null) {
        kanalView.BreakingError(response_message);
        }
    }
}
