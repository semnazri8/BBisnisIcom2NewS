package bisnis.com.official.Presenter;

import bisnis.com.official.Pojo.DetailBeritaResponse;
import bisnis.com.official.Pojo.ReadResponse;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 14/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface DetailBeritaInteractor {
    void getDetail(String Periode,String Category_id, String Post_id,DetailBeritaInteractor.OnSuccessgetDetailListener listener);

    interface OnSuccessgetDetailListener {

        void onSuccess(String response_message, ReadResponse readResponse);

        void onelseError(String response_message);

    }
}
