package bisnis.com.official.Presenter;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 14/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface DetailBeritaPresenter {
    void getDetailBerita(String periode,String category_id,String post_id);
    void onDestroy();
}
