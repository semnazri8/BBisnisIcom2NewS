package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.Pojo.Search;
import bisnis.com.official.View.BreakingView;
import bisnis.com.official.View.SearchView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class SearchPresenterImp implements SearchPresenter, SearchInteractor.OnSuccessgetSearchListener {

    private SearchView searchView;
    private SearchInteractor searchInteractor;

    public SearchPresenterImp(SearchView searchView) {
        this.searchView = searchView;
        this.searchInteractor = new SearchInteractorImp();
    }


    @Override
    public void getSearch(String keyword,String page) {
        searchInteractor.getSearch(this, keyword,page);
    }

    @Override
    public void onDestroy() {
        searchView = null;
    }


    @Override
    public void onSuccess(String response_message, ArrayList<Search> searchResponse) {
        if (searchView != null) {
            searchView.ResultSearch(response_message, searchResponse);
        }
    }

    @Override
    public void onelseError(String response_message) {
        if (searchView != null) {
        searchView.SearchError(response_message);
        }
    }
}
