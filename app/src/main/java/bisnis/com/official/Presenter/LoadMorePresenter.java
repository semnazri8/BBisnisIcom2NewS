package bisnis.com.official.Presenter;

public interface LoadMorePresenter {
    void getLoadMore(String url,String type_get,String page);
    void onDestroy();
}
