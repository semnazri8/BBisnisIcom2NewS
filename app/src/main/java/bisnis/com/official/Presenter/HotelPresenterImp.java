package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.Pojo.HotelResponse;
import bisnis.com.official.View.BreakingView;
import bisnis.com.official.View.HotelView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class HotelPresenterImp implements HotelPresenter, HotelInteractor.OnSuccessgetHotelListener {

    private HotelView hotelView;
    private HotelInteractor hotelInteractor;

    public HotelPresenterImp(HotelView hotelView) {
        this.hotelView = hotelView;
        this.hotelInteractor = new HotelInteractorImp();
    }

    @Override
    public void getHotel(String prov_id) {
        hotelInteractor.getHotel(this,prov_id);

    }

    @Override
    public void onDestroy() {
        hotelView = null;
    }

    @Override
    public void onSuccess(String response_message, ArrayList<HotelResponse> hotelResponses) {
        if (hotelView != null) {
            hotelView.ResultHotel(response_message, hotelResponses);
        }
    }

    @Override
    public void onelseError(String response_message) {
        if (hotelView != null) {
        hotelView.HotelError(response_message);
        }
    }
}
