package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface HTInteractor {

    void getListHT(OnSuccessgetListHTListener listener, String topics_id, String page, String limit);

    interface OnSuccessgetListHTListener {

        void onSuccess(String response_message, ArrayList<BreakingResponse> breakingResponse);

        void onelseError(String response_message);

    }
}
