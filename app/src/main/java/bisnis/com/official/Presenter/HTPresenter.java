package bisnis.com.official.Presenter;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface HTPresenter {
    void getListHT(String topics_id, String page, String limit);
    void onDestroy();
}
