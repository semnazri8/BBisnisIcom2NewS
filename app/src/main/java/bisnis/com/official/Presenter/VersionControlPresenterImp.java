package bisnis.com.official.Presenter;

import bisnis.com.official.Pojo.LoginResponse;
import bisnis.com.official.Pojo.VersionControl;
import bisnis.com.official.View.LoginView;
import bisnis.com.official.View.VersionControlView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 14/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class VersionControlPresenterImp implements VersionControlPresenter, VersionControlInteractor.OnSuccessCheckListener {
    private VersionControlView versionControlView;
    private VersionControlInteractor versionControlInteractor;

    public VersionControlPresenterImp(VersionControlView view) {
        this.versionControlView = view;
        this.versionControlInteractor = new VersionControlInteractorImp();
    }


    @Override
    public void doCheckVersion(String platform) {
        versionControlInteractor.CheckVersionCOntrol(this, platform);
    }

    @Override
    public void onDestroy() {
        versionControlView = null;
    }

    @Override
    public void onSuccess(String response_message, VersionControl versionControl) {
        if (versionControlView != null){
            versionControlView.ResultVersion(response_message,versionControl);
        }
    }

    @Override
    public void onelseError(String response_message) {
        if (versionControlView != null) {
            versionControlView.DetailError(response_message);
        }
    }
}
