package bisnis.com.official.Presenter;

import bisnis.com.official.Pojo.LoginResponse;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface LoginInteractor {
    void doLogin(OnSuccessLoginListener listener, String username, String password);

    interface OnSuccessLoginListener {

        void onAllError();

        void onUsernameError();

        void onUsernameInvalid();

        void onPasswordError();

        void onPasswordInValid();

        void onValid();

        void onSuccess(String response_message, LoginResponse loginResponse);

        void onelseError(String response_message);

    }
}
