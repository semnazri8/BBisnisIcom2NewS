package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.HotelResponse;
import bisnis.com.official.Pojo.PromoResponse;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface PromoInteractor {
    void getPromo(OnSuccessgetPromoListener listener, String limit);

    interface OnSuccessgetPromoListener {

        void onSuccess(String response_message, ArrayList<PromoResponse> promoResponses);

        void onelseError(String response_message);

    }
}
