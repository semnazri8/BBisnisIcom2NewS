package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.View.HTView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class HTPresenterImp implements HTPresenter, HTInteractor.OnSuccessgetListHTListener {

    private HTView htView;
    private HTInteractor htInteractor;

    public HTPresenterImp(HTView htView) {
        this.htView = htView;
        this.htInteractor = new HTInteractorImp();
    }


    @Override
    public void getListHT(String topics_id, String page, String limit) {
        htInteractor.getListHT(this, topics_id, page, limit);
    }

    @Override
    public void onDestroy() {
        htInteractor = null;
    }

    @Override
    public void onSuccess(String response_message, ArrayList<BreakingResponse> breakingResponse) {
        if (htView != null) {
            htView.ResultListHT(response_message, breakingResponse);
        }
    }

    @Override
    public void onelseError(String response_message) {
        if (htView != null) {
            htView.BreakingError(response_message);
        }
    }
}
