package bisnis.com.official.Presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import bisnis.com.official.Interface.DirectoryInterface;
import bisnis.com.official.Network.APIConstant;
import bisnis.com.official.Pojo.HotelResponse;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class HotelInteractorImp implements HotelInteractor {
    String response_message;
    ArrayList<HotelResponse> responses_hotel;


    @Override
    public void getHotel(final OnSuccessgetHotelListener listener, String prov_id) {
        responses_hotel = new ArrayList<>();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .retryOnConnectionFailure(true)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIConstant.API_PARENT)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();


        DirectoryInterface service = retrofit.create(DirectoryInterface.class);

        Call<ArrayList<HotelResponse>> call = service.getHotel(prov_id);

        call.enqueue(new Callback<ArrayList<HotelResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<HotelResponse>> call, Response<ArrayList<HotelResponse>> response) {

//                if (type_get.equals("Terbaru") || type_get.equals("Headline")) {
                if (response.isSuccessful()) {

                    for (int i = 0; i < response.body().size(); i++) {

                        HotelResponse hotel = new HotelResponse();

                        hotel.setHotelId(response.body().get(i).getHotelId());
                        hotel.setName(response.body().get(i).getName());
                        hotel.setAddress1(response.body().get(i).getAddress1());
                        hotel.setAddress2(response.body().get(i).getAddress2());
                        hotel.setPhone(response.body().get(i).getPhone());
                        hotel.setFax(response.body().get(i).getFax());
                        hotel.setEmail(response.body().get(i).getEmail());
                        hotel.setWebsite(response.body().get(i).getWebsite());
                        hotel.setProvId(response.body().get(i).getProvId());
                        hotel.setProvNama(response.body().get(i).getProvNama());
                        hotel.setKabId(response.body().get(i).getKabId());
                        hotel.setKabNama(response.body().get(i).getKabNama());
                        hotel.setImageContentUrl(response.body().get(i).getImageContentUrl());
                        hotel.setImageThumbnailUrl(response.body().get(i).getImageThumbnailUrl());

                        responses_hotel.add(hotel);
                    }

                    listener.onSuccess(response_message, responses_hotel);

                } else {

                    switch (response.code()) {

                        //TODO ini tolong contextnya di benerin

                        case 401:
                            listener.onelseError("Wrong Email or Password!");
                            break;
                        case 404:
                            listener.onelseError("Cannot find the right path! Response code 404");
                            break;
                        case 500:
                            listener.onelseError("Server is broken! Response code 500");
                            break;
                        default:
                            listener.onelseError("Wrong Email or Password!");
                            break;
                    }
                }
            }
//            }

            @Override
            public void onFailure(Call<ArrayList<HotelResponse>> call, Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });
    }
}
