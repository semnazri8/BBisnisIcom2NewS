package bisnis.com.official.Presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import bisnis.com.official.Interface.DirectoryInterface;
import bisnis.com.official.Network.APIConstant;
import bisnis.com.official.Pojo.HotelResponse;
import bisnis.com.official.Pojo.ProvinceResponse;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class ProvinceInteractorImp implements ProvinceInteractor {
    String response_message;
    ArrayList<ProvinceResponse> response_province;


    @Override
    public void getProvince(final OnSuccessgetProvinceListener listener) {
        response_province = new ArrayList<>();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .retryOnConnectionFailure(true)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIConstant.API_PARENT)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();


        DirectoryInterface service = retrofit.create(DirectoryInterface.class);

        Call<ArrayList<ProvinceResponse>> call = service.getProvince();

        call.enqueue(new Callback<ArrayList<ProvinceResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<ProvinceResponse>> call, Response<ArrayList<ProvinceResponse>> response) {

                if (response.isSuccessful()) {

                    for (int i = 0; i < response.body().size(); i++) {

                        ProvinceResponse provinceResponse = new ProvinceResponse();

                        provinceResponse.setNamaProvinsi(response.body().get(i).getNamaProvinsi());
                        provinceResponse.setProvinsiId(response.body().get(i).getProvinsiId());



                        response_province.add(provinceResponse);
                    }

                    ProvinceResponse provinceResponse = new ProvinceResponse();
                    provinceResponse.setProvinsiId("0");
                    provinceResponse.setNamaProvinsi("ALL PROPINSI");
                    response_province.add(0,provinceResponse);


                    listener.onSuccess(response_message, response_province);

                } else {

                    switch (response.code()) {

                        //TODO ini tolong contextnya di benerin

                        case 401:
                            listener.onelseError("Wrong Email or Password!");
                            break;
                        case 404:
                            listener.onelseError("Cannot find the right path! Response code 404");
                            break;
                        case 500:
                            listener.onelseError("Server is broken! Response code 500");
                            break;
                        default:
                            listener.onelseError("Wrong Email or Password!");
                            break;
                    }
                }
            }
//            }

            @Override
            public void onFailure(Call<ArrayList<ProvinceResponse>> call, Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });
    }
}
