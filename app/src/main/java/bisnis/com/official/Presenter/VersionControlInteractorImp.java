package bisnis.com.official.Presenter;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import bisnis.com.official.Interface.VersionControlInterface;
import bisnis.com.official.Network.APIConstant;
import bisnis.com.official.Pojo.VersionControl;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class VersionControlInteractorImp implements VersionControlInteractor {
    String response_message;

    @Override
    public void CheckVersionCOntrol(final OnSuccessCheckListener listener, String platform) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .retryOnConnectionFailure(true)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIConstant.API_PARENT)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        VersionControlInterface service = retrofit.create(VersionControlInterface.class);
        Call<ResponseBody> call = service.getVersionControl(platform);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    Gson gson = new Gson();
                    try {
                        VersionControl aa = gson.fromJson(response.body().string(), VersionControl.class);
                        listener.onSuccess(response_message, aa);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    switch (response.code()) {
                        case 401:
                            listener.onelseError("Unauthorized!");
                            break;
                        case 404:
                            listener.onelseError("Cannot find the right path! Response code 404");
                            break;
                        case 500:
                            listener.onelseError("Server is broken! Response code 500");
                            break;
                        default:
                            listener.onelseError("Wrong Email or Password!");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                android.util.Log.d("onFailure", t.toString());
            }
        });
    }
}
