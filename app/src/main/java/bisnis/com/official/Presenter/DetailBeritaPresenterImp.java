package bisnis.com.official.Presenter;

import bisnis.com.official.Pojo.DetailBeritaResponse;
import bisnis.com.official.Pojo.ReadResponse;
import bisnis.com.official.View.DetailView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 14/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class DetailBeritaPresenterImp implements DetailBeritaPresenter, DetailBeritaInteractor.OnSuccessgetDetailListener {
    private DetailView detailView;
    private DetailBeritaInteractor detailBeritaInteractor;

    public DetailBeritaPresenterImp(DetailView view) {
        this.detailView = view;
        this.detailBeritaInteractor = new DeatilBeritaInteractorImp();
    }

    @Override
    public void getDetailBerita(String Periode,String Category_id, String Post_id) {
        detailBeritaInteractor.getDetail(Periode,Category_id,Post_id, this);
    }

    @Override
    public void onDestroy() {
        detailView = null;
    }

    @Override
    public void onSuccess(String response_message, ReadResponse readResponse) {
        if (detailView!=null){
            detailView.ResultDetail(response_message,readResponse);
        }
    }

    @Override
    public void onelseError(String response_message) {
        if (detailView != null) {
            detailView.DetailError(response_message);
        }
    }
}
