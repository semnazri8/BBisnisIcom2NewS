package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.View.BreakingView;
import bisnis.com.official.View.LoadMoreView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class LoadMorePresenterImp implements LoadMorePresenter, LoadMoreInteractor.OnSuccessgetLoadoreListener {

    private LoadMoreView loadMoreView;
    private LoadMoreInteractor loadMoreInteractor;

    public LoadMorePresenterImp(LoadMoreView loadMoreView) {
        this.loadMoreView = loadMoreView;
        this.loadMoreInteractor = new LoadMoreInteractorImp();
    }

    @Override
    public void getLoadMore(String url, String type_get, String page) {
        loadMoreInteractor.getLoadMore(this, url,type_get,page);
    }

    @Override
    public void onDestroy() {
        loadMoreView = null;
    }

    @Override
    public void onSuccess(String response_message, ArrayList<BreakingResponse> breakingResponse) {
        if (loadMoreView != null) {
            loadMoreView.ResultLoadMore(response_message, breakingResponse);
        }
    }

    @Override
    public void onelseError(String response_message) {
        if (loadMoreView != null) {
            loadMoreView.BreakingError(response_message);
        }
    }
}
