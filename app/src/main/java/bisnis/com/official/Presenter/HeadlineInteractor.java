package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.HeadlineResponse;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface HeadlineInteractor {
    void getHeadline(OnSuccessgeHeadlineListener listener, String url, String type_get);

    interface OnSuccessgeHeadlineListener {

        void onSuccessHeadline(String response_message, ArrayList<HeadlineResponse> headlineResponses);

        void onelseError(String response_message);

    }
}
