package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.PromoResponse;
import bisnis.com.official.View.PromoView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class PromoPresenterImp implements PromoPresenter, PromoInteractor.OnSuccessgetPromoListener {

    private PromoView promoView;
    private PromoInteractor promoInteractor;

    public PromoPresenterImp(PromoView promoView) {
        this.promoView = promoView;
        this.promoInteractor = new PromoInteractorImp();
    }

    @Override
    public void getPromo(String limit) {
        promoInteractor.getPromo(this, limit);
    }

    @Override
    public void onDestroy() {
        promoView = null;
    }

    @Override
    public void onSuccess(String response_message, ArrayList<PromoResponse> promoResponses) {
        if (promoView != null) {
            promoView.ResultPromo(response_message, promoResponses);
        }
    }

    @Override
    public void onelseError(String response_message) {
        if (promoView != null) {
            promoView.PromoError(response_message);
        }
    }
}
