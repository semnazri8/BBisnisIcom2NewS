package bisnis.com.official.Presenter;

import java.util.ArrayList;

import bisnis.com.official.Pojo.HotelResponse;
import bisnis.com.official.Pojo.ProvinceResponse;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface ProvinceInteractor {
    void getProvince(OnSuccessgetProvinceListener listener);

    interface OnSuccessgetProvinceListener {

        void onSuccess(String response_message, ArrayList<ProvinceResponse> provinceResponses);

        void onelseError(String response_message);

    }
}
