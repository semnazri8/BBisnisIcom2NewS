package bisnis.com.official.View;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.Pojo.HotelResponse;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface HotelView {

    void ResultHotel(String response_message, ArrayList<HotelResponse> hotelResponses);
    void HotelError(String response_message);
}
