package bisnis.com.official.View;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface BreakingView {

    void ResultBreaking(String response_message, ArrayList<BreakingResponse> breakingResponse);
    void BreakingError(String response_message);
}
