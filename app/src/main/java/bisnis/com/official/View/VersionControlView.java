package bisnis.com.official.View;

import bisnis.com.official.Pojo.VersionControl;

public interface VersionControlView {

    void ResultVersion(String response_message, VersionControl versionControl);

    void DetailError(String response_message);
}
