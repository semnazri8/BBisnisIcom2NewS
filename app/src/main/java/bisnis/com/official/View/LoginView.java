package bisnis.com.official.View;

import bisnis.com.official.Pojo.LoginResponse;

public interface LoginView {

    void ResultLogin(String response_message, LoginResponse loginResponse);

    void DetailError(String response_message);

    void setUsernameError();

    void setUsernameInvalid();

    void setPasswordError();

    void setPasswordInvalid();

    void setAllError();

    void setvalid();
}
