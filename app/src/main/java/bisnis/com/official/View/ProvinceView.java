package bisnis.com.official.View;

import java.util.ArrayList;

import bisnis.com.official.Pojo.ProvinceResponse;

public interface ProvinceView {
    void ResultProvince(String response_message, ArrayList<ProvinceResponse> provinceResponses);

    void ProvinceError(String response_message);
}
