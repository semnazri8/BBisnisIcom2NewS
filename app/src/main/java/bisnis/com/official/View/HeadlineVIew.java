package bisnis.com.official.View;

import java.util.ArrayList;

import bisnis.com.official.Pojo.HeadlineResponse;

public interface HeadlineVIew {
    void ResultHeadline(String response_message, ArrayList<HeadlineResponse> headlineResponses);

    void HeadlineError(String response_message);
}
