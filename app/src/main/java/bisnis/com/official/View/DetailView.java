package bisnis.com.official.View;

import bisnis.com.official.Pojo.DetailBeritaResponse;
import bisnis.com.official.Pojo.ReadResponse;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 14/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface DetailView {
    void ResultDetail(String response_message, ReadResponse readResponse);
    void DetailError(String response_message);
}
