package bisnis.com.official.View;

import java.util.ArrayList;

import bisnis.com.official.Pojo.HotelResponse;
import bisnis.com.official.Pojo.PromoResponse;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface PromoView {

    void ResultPromo(String response_message, ArrayList<PromoResponse> hotelResponses);
    void PromoError(String response_message);
}
