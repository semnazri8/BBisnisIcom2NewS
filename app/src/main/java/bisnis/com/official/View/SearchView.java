package bisnis.com.official.View;

import java.util.ArrayList;

import bisnis.com.official.Pojo.Search;

public interface SearchView {
    void ResultSearch(String response_message, ArrayList<Search> searchresponse);

    void SearchError(String response_message);
}
