package bisnis.com.official.Interface;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface LoadMoreInterface {
    @FormUrlEncoded
    @POST("breaking/loadmore")
    Call<ArrayList<BreakingResponse>> getBreakingLoadmore(@Field("category") String kanal_id, @Field("page") String page);

    @FormUrlEncoded
    @POST("headline/loadmore")
    Call<ArrayList<BreakingResponse>> getHeadlineLoadmore(@Field("category") String kanal_id, @Field("page") String page);

    @POST("hottopic/loadmore")
    Call<ArrayList<BreakingResponse>> getHotTopicsLoadmore();
}
