package bisnis.com.official.Interface;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 19/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface DetailInterface {
    @FormUrlEncoded
    @POST("read")
    Call<ResponseBody> getAuto(@Field("periode") String periode, @Field("category_id") String category_id, @Field("post_id") String post_id);
}
