package bisnis.com.official.Interface;

import java.util.ArrayList;

import bisnis.com.official.Pojo.PromoResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ExtrasInterface {

    @FormUrlEncoded
    @POST("promo")
    Call<ArrayList<PromoResponse>> getPromo(@Field("limit") String limit);

}
