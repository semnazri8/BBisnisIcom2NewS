package bisnis.com.official.Interface;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 8/21/17.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface VersionControlInterface {
    @FormUrlEncoded
    @POST("version")
    Call<ResponseBody> getVersionControl(@Field("platform") String platform);
}
