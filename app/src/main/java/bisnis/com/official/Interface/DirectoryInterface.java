package bisnis.com.official.Interface;

import java.util.ArrayList;

import bisnis.com.official.Pojo.HotelResponse;
import bisnis.com.official.Pojo.ProvinceResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface DirectoryInterface {

    @FormUrlEncoded
    @POST("hotels")
    Call<ArrayList<HotelResponse>> getHotel(@Field("prov_id") String province_id);


    @POST("hotels/listprov")
    Call<ArrayList<ProvinceResponse>> getProvince();




}
