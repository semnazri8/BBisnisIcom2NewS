package bisnis.com.official.Interface;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.Pojo.Search;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface BreakingInterface {
    @FormUrlEncoded
    @POST("breaking")
    Call<ArrayList<BreakingResponse>> getBreaking(@Field("category") String kanal_id);

    @FormUrlEncoded
    @POST("headline")
    Call<ArrayList<BreakingResponse>> getHeadline(@Field("category") String kanal_id);

    @FormUrlEncoded
    @POST("hottopic")
    Call<ArrayList<BreakingResponse>> getHotTopics(@Field("category") String kanal_id);

    @FormUrlEncoded
    @POST("hottopic/list")
    Call<ArrayList<BreakingResponse>> getListHotTopics(@Field("topic") String topic_id, @Field("page") String page, @Field("limit") String limit);

    @FormUrlEncoded
    @POST("populer")
    Call<ArrayList<BreakingResponse>> getTerpopuler(@Field("category") String kanal_id);


    @FormUrlEncoded
    @POST("search")
    Call<ArrayList<Search>> getSearch(@Field("keyword") String keyword,@Field("page") String page);

    @FormUrlEncoded
    @POST("indeks/index")
    Call<ArrayList<BreakingResponse>> getIndex(@Field("day") String day, @Field("month") String month, @Field("year") String year,
                                               @Field("page") String page, @Field("limit") String limit, @Field("category_id") String category_id);
}
