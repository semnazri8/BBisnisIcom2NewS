package bisnis.com.official.Interface;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 27/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public interface LikeInterface {

    void onLikeListener(int status);
}
