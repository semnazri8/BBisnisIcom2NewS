package bisnis.com.official.Interface;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginInterface {

    @FormUrlEncoded
    @POST("user")
    Call<ResponseBody> doLogin(@Field("username") String username, @Field("password") String password);
}
