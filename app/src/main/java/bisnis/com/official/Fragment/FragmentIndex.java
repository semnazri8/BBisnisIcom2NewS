package bisnis.com.official.Fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import bisnis.com.official.ActivityDeatilBerita;
import bisnis.com.official.Adapter.IndexAdapter;
import bisnis.com.official.Adapter.SpinnerChanelAdapter;
import bisnis.com.official.Listener.OnClickAdapterListener;
import bisnis.com.official.Model.ModelSpinnerKota;
import bisnis.com.official.Network.ConnectionDetector;
import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.Presenter.IndexPresenter;
import bisnis.com.official.Presenter.IndexPresenterImp;
import bisnis.com.official.R;
import bisnis.com.official.Utilities.SpacesItemDecoration;
import bisnis.com.official.View.IndexView;

public class FragmentIndex extends AppCompatActivity implements IndexView, OnClickAdapterListener {
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private Spinner spinner_kanal;
    private SpinnerChanelAdapter adapter;
    private ArrayList<ModelSpinnerKota> modelKotas;
    private TextView txt_date, txt_title;
    private Calendar myCalendar = Calendar.getInstance();
    private String month_final;
    private String category_id = "0";
    private IndexPresenter indexPresenter;
    private boolean loading = false;
    private int page = 1;
    private LinearLayoutManager lm;
    private MaterialDialog mDialog, dialog_muter;
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private IndexAdapter indexAdapter;
    private SwipeRefreshLayout srl;
    private RecyclerView recyclerView;
    private ArrayList<BreakingResponse> braking;
    private String tgl, bln, thn;
    private Button btn_search;
    private ImageView img_back;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.fragment_index);

        indexPresenter = new IndexPresenterImp(this);
        cd = new ConnectionDetector(this);

        spinner_kanal = findViewById(R.id.spinner_kanal);
        txt_date = findViewById(R.id.txt_date);
        srl = findViewById(R.id.srl_list_dash);
        recyclerView = findViewById(R.id.rv_index);
        btn_search = findViewById(R.id.btn_search);
        txt_title = findViewById(R.id.txt_title);
        img_back = findViewById(R.id.img_back);

        txt_title.setText("Indeks Berita");
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);

        modelKotas = getChanel();
        adapter = new SpinnerChanelAdapter(this, modelKotas);
        lm = new LinearLayoutManager(this);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        recyclerView.setLayoutManager(lm);
        spinner_kanal.setAdapter(adapter);

        spinner_kanal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item_id = String.valueOf(((TextView) view.findViewById(R.id.id_item)).getText().toString());
                category_id = item_id;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        srl.setColorSchemeColors(getResources().getIntArray(R.array.intarr_swipe_refresh_layout));
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                checkConnections(tgl, bln, thn, page, "20", category_id);
            }
        });


        updateLabel();
        checkConnections(tgl, bln, thn, page, "20", category_id);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        txt_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(FragmentIndex.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    int lastIndex = lm.findLastCompletelyVisibleItemPosition() + 1;
                    int totalItem = indexAdapter.getItemCount();

                    visibleItemCount = lm.getChildCount();
                    totalItemCount = indexAdapter.getItemCount();
                    pastVisiblesItems = lm.findFirstVisibleItemPosition() + 1;

                    if (!loading && lastIndex == totalItem) {
//                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        loading = true;
                        srl.setRefreshing(true);
                        Log.v("...", "Last Item Wow !");

                        indexPresenter.getIndex(tgl, bln, thn, String.valueOf(page++), "20", category_id);

                    }
                }


            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page = 1;
                checkConnections(tgl, bln, thn, page, "20", category_id);

            }
        });
    }


    private void checkConnections(String tanggal, String bulan, String tahun, int page, String limit, String category_id) {
        getDialog_progress();
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            //TODO : nanti ini untuk set breaking, headline, dll

            indexAdapter = new IndexAdapter(this,this);
            recyclerView.setAdapter(indexAdapter);
            loading = true;

            indexPresenter.getIndex(tanggal, bulan, tahun, String.valueOf(page), limit, category_id);

        } else if (isInternetPresent.equals(false)) {
            getdialogerror("Tidak ada koneksi Internet");
            srl.setRefreshing(false);
        }
    }

    private void updateLabel() {

        String myFormat = "dd-MMM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("yyyy/MM/dd");

        DateFormat outputDate = new SimpleDateFormat("dd");
        DateFormat outputMonth = new SimpleDateFormat("MMMM");
        DateFormat outputMonthstr = new SimpleDateFormat("MM");
        DateFormat outputYear = new SimpleDateFormat("yyyy");

        String inputDateStr = sdf.format(myCalendar.getTime());
        Date date1 = myCalendar.getTime();

        try {
            date1 = inputFormat.parse(inputDateStr);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        String tanggal = outputDate.format(date1);
        String bulan = outputMonth.format(date1);
        String bulanstr = outputMonthstr.format(date1);
        String tahun = outputYear.format(date1);

        if (bulan.contains("January")) {
            month_final = bulan.replace("January", "Januari");
        } else if (bulan.contains("February")) {
            month_final = bulan.replace("February", "Februari");
        } else if (bulan.contains("March")) {
            month_final = bulan.replace("March", "Maret");
        } else if (bulan.contains("April")) {
            month_final = bulan.replace("April", "April");
        } else if (bulan.contains("May")) {
            month_final = bulan.replace("May", "Mei");
        } else if (bulan.contains("June")) {
            month_final = bulan.replace("June", "Juni");
        } else if (bulan.contains("July")) {
            month_final = bulan.replace("July", "Juli");
        } else if (bulan.contains("August")) {
            month_final = bulan.replace("August", "Agustus");
        } else if (bulan.contains("September")) {
            month_final = bulan.replace("September", "September");
        } else if (bulan.contains("October")) {
            month_final = bulan.replace("October", "Oktober");
        } else if (bulan.contains("November")) {
            month_final = bulan.replace("November", "November");
        } else if (bulan.contains("December")) {
            month_final = bulan.replace("December", "Desember");
        }

//        txt_date.setText(sdf.format(myCalendar.getTime()));
        txt_date.setText(tanggal + " - " + month_final + " - " + tahun);


        tgl = tanggal;
        bln = bulanstr;
        thn = tahun;


    }


    private ArrayList<ModelSpinnerKota> getChanel() {

        ArrayList<ModelSpinnerKota> kota = new ArrayList<>();

        kota.add(new ModelSpinnerKota("All", 0));
        kota.add(new ModelSpinnerKota("Market", 194));
        kota.add(new ModelSpinnerKota("Finansial", 5));
        kota.add(new ModelSpinnerKota("Industri", 43));
        kota.add(new ModelSpinnerKota("Kabar24", 186));
        kota.add(new ModelSpinnerKota("Life & Style", 197));
        kota.add(new ModelSpinnerKota("Jakarta", 382));
        kota.add(new ModelSpinnerKota("Surabaya", 526));
        kota.add(new ModelSpinnerKota("Sumatra", 527));
        kota.add(new ModelSpinnerKota("Semarang", 528));
        kota.add(new ModelSpinnerKota("Bali", 529));
        kota.add(new ModelSpinnerKota("Sulawesi", 530));
        kota.add(new ModelSpinnerKota("Papua", 413));
        kota.add(new ModelSpinnerKota("Kalimantan", 406));
        kota.add(new ModelSpinnerKota("Banten", 420));
        kota.add(new ModelSpinnerKota("Properti", 47));
        kota.add(new ModelSpinnerKota("Bola", 392));
        kota.add(new ModelSpinnerKota("Gadget", 277));
        kota.add(new ModelSpinnerKota("Traveling", 222));
        kota.add(new ModelSpinnerKota("Sport", 57));
        kota.add(new ModelSpinnerKota("Otomotif", 272));
        kota.add(new ModelSpinnerKota("Manajemen", 52));
        kota.add(new ModelSpinnerKota("Enterpreneur", 258));
        kota.add(new ModelSpinnerKota("Info", 243));
        kota.add(new ModelSpinnerKota("Inforial", 73));
        kota.add(new ModelSpinnerKota("Syariah", 231));

        return kota;
    }

    @Override
    public void ResultIndex(String response_message, ArrayList<BreakingResponse> breakingResponse) {
        onLoadNewComplete();
        loading = false;
        dialog_muter.dismiss();
        if (breakingResponse.size() > 0)
            ParseType(breakingResponse);
    }

    private void onLoadNewComplete() {
        srl.setRefreshing(false);
    }


    private void ParseType(ArrayList<BreakingResponse> breakingResponse) {
        onLoadNewComplete();
        braking = breakingResponse;


        for (int i = 0; i < breakingResponse.size(); i++) {
            if (breakingResponse.get(i).getPostDate() != null) {
                breakingResponse.get(i).setType_get("Index");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    Date mDate = sdf.parse(breakingResponse.get(i).getPostDate());

                    DateFormat format = new SimpleDateFormat("dd MMM yyyy, HH:mm");
                    String newformat = format.format(mDate);

                    breakingResponse.get(i).setFull_short_month(newformat + " wib");
//                    }

                    long timeInMilliseconds = mDate.getTime();
                    breakingResponse.get(i).setMilis(timeInMilliseconds);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }


//        if (type_get.equals("Terbaru")) {
//            for (int i = 0; i < breakingResponse.size(); i++) {
//
//
//                breakingResponse.get(i).setType_get("Terbaru");
//                breakingResponse.get(i).setKanal_id("0");
//
//                if (i == 0) {
//                    breakingResponse.get(i).setType(0);
//                } else if (i == 1 || i == 2) {
//                    breakingResponse.get(i).setType(2);
//                } else if (i % 5 == 0) {
//                    breakingResponse.get(i).setType(0);
//                } else {
//                    breakingResponse.get(i).setType(1);
//                }
//            }
//
//            if (breakingResponse.size() != 0) {
//                BreakingResponse breaking = new BreakingResponse();
//                breaking.setImage_ads(R.drawable.banner_and);
//                breaking.setType(3);
//
//                breakingResponse.add(3, breaking);
//            }


//        } else if (type_get.equals("Headline")) {
//            for (int i = 0; i < breakingResponse.size(); i++) {
//
//                breakingResponse.get(i).setType_get("Headline");
//                breakingResponse.get(i).setType(0);
//                breakingResponse.get(i).setKanal_id("0");
//                breakingResponse.get(i).setMilis(000);
//            }
//        } else if (type_get.equals("Hot Topic")) {
//
//            for (int i = 0; i < breakingResponse.size(); i++) {
//                breakingResponse.get(i).setCategoryParentName(breakingResponse.get(i).getTopic_name());
//
//                breakingResponse.get(i).setType_get("hottopic");
//                breakingResponse.get(i).setKanal_id("0");
//                breakingResponse.get(i).setType(1);
//
//            }
//        } else if (type_get.equals("Terpopuler")) {
//            for (int i = 0; i < breakingResponse.size(); i++) {
//
//                breakingResponse.get(i).setCategoryParentName(breakingResponse.get(i).getCategoryName());
//
//                breakingResponse.get(i).setType_get("Terpopuler");
//                breakingResponse.get(i).setKanal_id("0");
//                breakingResponse.get(i).setType(1);
//            }
//        }
        indexAdapter.addOn(breakingResponse);

    }

    @Override
    public void IndexError(String response_message) {
        loading = false;
        getdialogerror(response_message);
    }

    public void getDialog_progress() {

        dialog_muter = new MaterialDialog.Builder(this)
                .title(R.string.app_name)
                .cancelable(false)
                .content(R.string.please_wait)
                .progress(true, 0)
                .show();
    }

    private void getdialogerror(String response_message) {
        dialog_muter.dismiss();
        mDialog = new MaterialDialog.Builder(this)
                .title(R.string.app_name)
                .cancelable(false)
                .content(response_message)
                .positiveText("Close")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mDialog.dismiss();

                    }
                })
                .show();
    }

    @Override
    public void onClick(String position, String type_get, String kanal_id) {

    }

    @Override
    public void onClickAja(String position, String type_get, String kanal_id, ArrayList<BreakingResponse> dataSet) {
        Intent i = new Intent(this, ActivityDeatilBerita.class);
        i.putExtra("index", position);
        i.putExtra("type_get", type_get);
        i.putExtra("kanal_id", kanal_id);
        i.putExtra("parcel", dataSet);
        i.putExtra("helo", "breaking");

//        i.putExtras(bundle);
        startActivity(i);
    }

    @Override
    public void onClickHeadline(String position, String type_get, String kanal_id) {

    }
}
