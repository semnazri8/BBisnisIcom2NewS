package bisnis.com.official.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import bisnis.com.official.ActivitySingleDetail;
import bisnis.com.official.Adapter.AdapterBeritaTerkait;
import bisnis.com.official.Adapter.AdapterLive;
import bisnis.com.official.Interface.LikeInterface;
import bisnis.com.official.MainActivity;
import bisnis.com.official.Model.ComentModel;
import bisnis.com.official.Network.ConnectionDetector;
import bisnis.com.official.Pojo.ReadResponse;
import bisnis.com.official.Presenter.DetailBeritaPresenter;
import bisnis.com.official.Presenter.DetailBeritaPresenterImp;
import bisnis.com.official.R;
import bisnis.com.official.Utilities.AnalyticsApplication;
import bisnis.com.official.Utilities.BisnisComOfficialPreferences;
import bisnis.com.official.View.DetailView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 19/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class Fragment_detail extends Fragment implements LikeInterface, DetailView {

    public static final String PREFS_PRIVATE = "PREFS_PRIVATE";
    //    private DatabaseHelper db;
    String newformat_time;
    private View view;
    private WebView txt_berita;
    private ImageView img_berita, img_ads, img_kanal, img_bookmark, arrow_left, arrow_right;
    private TextView txt_date, txt_author, txt_title_Berita, img_desc, txt_source, txt_editor, tv_title_berita_terkaid, islive, disini, series_count, series_titlel, series_titler, txt_series_counter, title_series;
    private AdapterBeritaTerkait adapterBeritaTerkait;
    private AdapterLive adapterLive;
    //    private AdapterKomen adapterKomen;
    private String date, cat_id, post_id;
    private RecyclerView rv_terkait, rv_komen, rv_live;
    private LinearLayoutManager lm, lmTerkait, lm_live;
    //    private List<ComentModel> comentModelList;
    private String url;
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private MaterialDialog mDialog, dialog_muter;
    private DetailBeritaPresenter presenter;
    private AppBarLayout aawer;
    private Toolbar toolbar;
    private WebSettings webSettings;
    private int fontsize = 18;
    private LinearLayout ll_terkait, korbanHide, ll_premium, series_ll, ll_left, ll_right, ll_series_counter;
    private RequestOptions myoptions_thumbnail;
    private SharedPreferences prefsprivate;
    //    private Boolean lockerd = false;
    private ImageButton img_size, img_share, img_back;
    private String baca1, baca2, baca3;
    private int position = 0;
    private AnalyticsApplication application;
    private Tracker mTracker;
    private String kanal_name;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cd = new ConnectionDetector(getActivity());
    }

    private boolean _hasLoadedOnce= false; // your boolean field

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);


        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                _hasLoadedOnce = true;
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fregment_detail, container, false);
//        db = new DatabaseHelper(getActivity());
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "Font/roboto_bold.ttf");
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "Font/roboto_regular.ttf");
        txt_berita = view.findViewById(R.id.txt_bodyBerita);
        txt_title_Berita = view.findViewById(R.id.txt_title_Berita);

        txt_title_Berita.setTypeface(bold);

        toolbar = view.findViewById(R.id.toolbar);

        img_berita = view.findViewById(R.id.img_berita);
        img_ads = view.findViewById(R.id.img_ads);

        img_size = view.findViewById(R.id.img_font);
        img_back = view.findViewById(R.id.img_back);
        img_share = view.findViewById(R.id.img_share);
        img_bookmark = view.findViewById(R.id.img_bookmark);
        img_kanal = view.findViewById(R.id.img_kanal);

        txt_date = view.findViewById(R.id.txt_date);
        img_desc = view.findViewById(R.id.img_desc);
        txt_author = view.findViewById(R.id.txt_author);
        txt_editor = view.findViewById(R.id.txt_editor);
        txt_source = view.findViewById(R.id.txt_source);
        disini = view.findViewById(R.id.klikdisini);

        ll_terkait = view.findViewById(R.id.ll_berita_terkait);

        series_ll = view.findViewById(R.id.series_ll);
        series_count = view.findViewById(R.id.series_count);
        series_titlel = view.findViewById(R.id.series_titlel);
        series_titler = view.findViewById(R.id.series_titler);
        arrow_left = view.findViewById(R.id.arrow_left);
        arrow_right = view.findViewById(R.id.arrow_right);
        ll_left = view.findViewById(R.id.ll_left);
        ll_right = view.findViewById(R.id.ll_right);
        ll_series_counter = view.findViewById(R.id.ll_series_counter);

        txt_series_counter = view.findViewById(R.id.txt_series_counter);
        title_series = view.findViewById(R.id.title_series);
        title_series.setTypeface(bold);
        series_titlel.setTypeface(bold);
        series_titler.setTypeface(bold);
        islive = view.findViewById(R.id.is_live);
        tv_title_berita_terkaid = view.findViewById(R.id.tv_title_berita_terkaid);
        tv_title_berita_terkaid.setTypeface(bold);
        islive.setTypeface(bold);
        disini.setTypeface(bold);


        rv_terkait = view.findViewById(R.id.rv_terkait);
        rv_komen = view.findViewById(R.id.rv_komentar);
        rv_live = view.findViewById(R.id.rv_live);

        aawer = view.findViewById(R.id.aawer);
        korbanHide = view.findViewById(R.id.korban_hide);
        ll_premium = view.findViewById(R.id.ll_premium);

        date = getArguments().getString("post_date");
        cat_id = getArguments().getString("categoty_id");
        post_id = getArguments().getString("post_id");

//        date = "20180521";
//        cat_id = "15";
//        post_id = "797519";
        application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

        txt_berita.getSettings().setJavaScriptEnabled(true);

        txt_berita.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        txt_berita.setWebViewClient(new MyWebViewClient());
        webSettings = txt_berita.getSettings();
        webSettings.setDefaultTextEncodingName("utf-8");

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        img_size.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fontsize == 18) {
                    fontsize = 20;
                    webSettings.setDefaultFontSize(20);
                } else if (fontsize == 20) {
                    fontsize = 22;
                    webSettings.setDefaultFontSize(22);
                } else if (fontsize == 22) {
                    fontsize = 18;
                    webSettings.setDefaultFontSize(18);
                }

            }
        });
        SpannableString ss = new SpannableString("[Klik Disini]");

        ClickableSpan cs = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent i = new Intent(getActivity(), MainActivity.class);
                i.putExtra("frgToLoad", "profile");
                startActivity(i);

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }


        };
        ss.setSpan(cs, 0, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_tone)), 0, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        disini.setText(ss);
        disini.setMovementMethod(LinkMovementMethod.getInstance());


        presenter = new DetailBeritaPresenterImp(this);
        aawer.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                float offsetAlpha = (appBarLayout.getY() / aawer.getTotalScrollRange());
//                img_berita.setAlpha((float) (1 - (offsetAlpha * -0.3)));

                Log.d("hehe", String.valueOf(1 - (offsetAlpha * -0.3)));
                Log.d("offset", String.valueOf(offsetAlpha));


                if (offsetAlpha == -1) {
                    img_kanal.setVisibility(View.VISIBLE);
                    img_berita.setColorFilter(ContextCompat.getColor(getActivity(), R.color.blacktras), PorterDuff.Mode.DARKEN);
                    img_kanal.setColorFilter(Color.argb(255, 255, 255, 255));
                    img_back.setColorFilter(Color.argb(255, 255, 255, 255));
                    img_share.setColorFilter(Color.argb(255, 255, 255, 255));
                    img_bookmark.setColorFilter(Color.argb(255, 255, 255, 255));
                    img_size.setColorFilter(Color.argb(255, 255, 255, 255));
                    toolbar.setBackgroundColor(getResources().getColor(R.color.transparent));
                } else if (offsetAlpha != -0.0) {
                    img_kanal.setVisibility(View.GONE);
                    img_berita.clearColorFilter();
                } else {
                    img_kanal.setVisibility(View.VISIBLE);
                    img_kanal.clearColorFilter();
                    img_berita.clearColorFilter();
                    img_back.clearColorFilter();
                    img_share.clearColorFilter();
                    img_bookmark.clearColorFilter();
                    img_size.clearColorFilter();

                }


            }
        });

        if (_hasLoadedOnce == true){

        }
        checkConnections();

//        Log.d("bhak", date + " - " + cat_id + " =  " + post_id);
//
//        if (db.getNotesCount() != 0){
//            Toast.makeText(getActivity(), "TIdak kosong" + String.valueOf(db.getNotesCount()), Toast.LENGTH_SHORT).show();
//        }else{
//            Toast.makeText(getActivity(), "Kosong", Toast.LENGTH_SHORT).show();
//        }

        img_bookmark.setVisibility(View.GONE);
        return view;
    }

    private void checkConnections() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
//            getDialog_progress();
            if (!cat_id.equals("") || !post_id.equals("")) {

//                presenter.getDetailBerita("20180413", "235", "783758");
                presenter.getDetailBerita(date, cat_id, post_id);
            }

        } else if (isInternetPresent.equals(false)) {
            getdialogerror("Tidak ada koneksi Internet");
        }

    }

    public void getDialog_progress() {

        dialog_muter = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .content(R.string.please_wait)
                .progress(true, 0)
                .show();
    }

    private void getdialogerror(String response_message) {
//        dialog_muter.dismiss();
        mDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .content(response_message)
                .positiveText("Close")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mDialog.dismiss();
                        getFragmentManager().popBackStack();
                    }
                })
                .show();
    }

    private void getparentCategory(String parent_id) {
        Log.d("dadadada", parent_id);

        if (parent_id.equals("186")) {
            img_kanal.setImageResource(R.drawable.ic_kabar24);
            kanal_name = "Kabar24";
        } else if (parent_id.equals("194")) {
            img_kanal.setImageResource(R.drawable.ic_marked);
            kanal_name = "Market";
        } else if (parent_id.equals("5")) {
            img_kanal.setImageResource(R.drawable.ic_finance);
            kanal_name = "Finance";
        } else if (parent_id.equals("43")) {
            img_kanal.setImageResource(R.drawable.ic_industri);
            kanal_name = "Industri";
        } else if (parent_id.equals("272")) {
            img_kanal.setImageResource(R.drawable.ic_otomotif);
            kanal_name = "Otomotif";
        } else if (parent_id.equals("382")) {
            img_kanal.setImageResource(R.drawable.ic_jakarta_raya);
            kanal_name = "JakartaRaya";
        } else if (parent_id.equals("392")) {
            img_kanal.setImageResource(R.drawable.ic_bola);
            kanal_name = "Sepakbnola";
        } else if (parent_id.equals("197")) {
            img_kanal.setImageResource(R.drawable.ic_lifestyle);
            kanal_name = "LifeStyle";
        } else if (parent_id.equals("277")) {
            img_kanal.setImageResource(R.drawable.ic_gadget);
            kanal_name = "Gadget";
        } else if (parent_id.equals("222")) {
            img_kanal.setImageResource(R.drawable.ic_treveling);
            kanal_name = "Traveling";
        } else if (parent_id.equals("47")) {
            img_kanal.setImageResource(R.drawable.ic_properti);
            kanal_name = "Properti";
        } else if (parent_id.equals("57")) {
            img_kanal.setImageResource(R.drawable.ic_sport);
            kanal_name = "Sport";
        } else if (parent_id.equals("242")) {
            img_kanal.setImageResource(R.drawable.ic_koran);
            kanal_name = "KoranBisnis";
        } else if (parent_id.equals("231")) {
            img_kanal.setImageResource(R.drawable.ic_syariah);
            kanal_name = "Syariah";
        } else if (parent_id.equals("52")) {
            img_kanal.setImageResource(R.drawable.ic_manajemen);
            kanal_name = "Manajemen";
        } else if (parent_id.equals("258")) {
            img_kanal.setImageResource(R.drawable.ic_enterpreneur);
            kanal_name = "Enterpreneur";
        } else if (parent_id.equals("243")) {
            img_kanal.setImageResource(R.drawable.ic_info);
            kanal_name = "Info";
        } else if (parent_id.equals("73")) {
            img_kanal.setImageResource(R.drawable.ic_inforial);
            kanal_name = "Inforial";
        } else if (parent_id.equals("526")) {
            img_kanal.setImageResource(R.drawable.jatim);
            kanal_name = "JawaTimur";
        } else if (parent_id.equals("527")) {
            img_kanal.setImageResource(R.drawable.sumatra);
            kanal_name = "Sumatera";
        } else if (parent_id.equals("528")) {
            img_kanal.setImageResource(R.drawable.jateng);
            kanal_name = "JawaTengah";
        } else if (parent_id.equals("529")) {
            img_kanal.setImageResource(R.drawable.bali);
            kanal_name = "Bali";
        } else if (parent_id.equals("530")) {
            img_kanal.setImageResource(R.drawable.sulawesi);
            kanal_name = "Sulawesi";
        } else if (parent_id.equals("413")) {
            img_kanal.setImageResource(R.drawable.papua);
            kanal_name = "Papua";
        } else if (parent_id.equals("406")) {
            img_kanal.setImageResource(R.drawable.kalimantan);
            kanal_name = "Kalimantan";
        } else if (parent_id.equals("420")) {
            img_kanal.setImageResource(R.drawable.banten);
            kanal_name = "Banten";
        }
    }

    private List<ComentModel> getComment() {

        List<ComentModel> cm = new ArrayList<>();

        cm.add(new ComentModel(R.drawable.ic_person_black_24dp, 200, 20, "20 April 1993 - 16:30 WIB", "Mantab Jiwa!", "Nevertari Vivi", 0));
        cm.add(new ComentModel(R.drawable.ic_person_black_24dp, 100, 15, "20 April 1993 - 16:30 WIB", "NICE INFO!", "Anynomous", 1));
        cm.add(new ComentModel(R.drawable.ic_person_black_24dp, 200, 3, "20 April 1993 - 16:30 WIB", "Mantab Jiwa!", "Nevertari Vivi", 0));


        return cm;
    }

    @Override
    public void onLikeListener(int status) {

    }

    @Override
    public void ResultDetail(String response_message, final ReadResponse readResponse) {
        rv_terkait.setHasFixedSize(true);
        lmTerkait = new LinearLayoutManager(getActivity());
        rv_terkait.setLayoutManager(lmTerkait);

        final String titlenya = readResponse.getTitle();
        final String url_image = readResponse.getImageContentUrl();
        final String kanal_name = readResponse.getParentCategory().getName();
        final String time_news = readResponse.getDate().get0();
        final String category = readResponse.getCategory().getId();
        final String post_id = readResponse.getPostId();
        mTracker.setScreenName("Android/" + kanal_name + "/" + titlenya);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        myoptions_thumbnail = new RequestOptions()
                .placeholder(R.drawable.placeholder_large)
                .error(R.drawable.placeholder_large);

        final String css_contents = "<style>" +
                "img{" +
                "width:100% !important;\n" +
                "}" +
                "table{" +
                "width:100% !important;\n" +
                "}</style>";

        if (readResponse.getIsSeries() != null) {
            if (readResponse.getIsSeries().equals("1")) {
                series_ll.setVisibility(View.VISIBLE);
                series_count.setText("1 dari " + readResponse.getSerial().size() + " halaman");
                series_titler.setText(readResponse.getSerial().get(position + 1).getPostSeriesTitle());
                ll_left.setVisibility(View.GONE);

                arrow_right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        position++;
                        ll_series_counter.setVisibility(View.VISIBLE);
                        txt_series_counter.setText(String.valueOf(position));
                        title_series.setText(readResponse.getSerial().get(position).getPostSeriesTitle());
                        Glide.with(getActivity()).load(readResponse.getSerial().get(position).getImageUrl()).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        }).apply(myoptions_thumbnail).into(img_berita);
                        series_count.setText(position + 1 + " dari " + readResponse.getSerial().size() + " halaman");
                        String conten_beritas = String.valueOf(Html.fromHtml(readResponse.getSerial().get(position).getPostContent()));
                        String[] splitz = conten_beritas.toString().split("</p><p>");
                        String contentBeritas = css_contents + "";

                        for (int i = 0; i < splitz.length; i++) {
                            contentBeritas = String.format(Locale.US, "%s%s</p>", contentBeritas, splitz[i]);
                        }

                        String css = "<style>\n" +
                                ".wrapper {\n" +
                                "    line-height: 1.8\n" +
                                "}\n" +
                                "</style>";

                        String wanabe = css + "<div class=\"wrapper\">" + contentBeritas + "</div>";

                        if (position == readResponse.getSerial().size() - 1) {
                            ll_right.setVisibility(View.GONE);
                            series_titlel.setText(readResponse.getSerial().get(position - 1).getPostSeriesTitle());

                            txt_berita.loadData(wanabe, "text/html; charset=utf-8", "utf-8");
//
                        } else {

                            ll_left.setVisibility(View.VISIBLE);
                            series_titler.setText(readResponse.getSerial().get(position + 1).getPostSeriesTitle());
                            txt_berita.loadData(wanabe, "text/html; charset=utf-8", "utf-8");
                            if (readResponse.getSerial().get(position - 1).getPostSeriesTitle().equals("")) {
                                series_titlel.setText("");
                            } else {
                                series_titlel.setText(readResponse.getSerial().get(position - 1).getPostSeriesTitle());
                            }

                        }


                    }
                });

                arrow_left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        position--;
                        txt_series_counter.setText(String.valueOf(position));
                        title_series.setText(readResponse.getSerial().get(position).getPostSeriesTitle());
                        Glide.with(getActivity()).load(readResponse.getSerial().get(position).getImageUrl()).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        }).apply(myoptions_thumbnail).into(img_berita);
                        series_count.setText(position + 1 + " dari " + readResponse.getSerial().size() + " halaman");
                        String conten_beritas = String.valueOf(Html.fromHtml(readResponse.getSerial().get(position).getPostContent()));
                        String[] splitz = conten_beritas.toString().split("</p><p>");
                        String contentBeritas = css_contents + "";

                        for (int i = 0; i < splitz.length; i++) {
                            contentBeritas = String.format(Locale.US, "%s%s</p>", contentBeritas, splitz[i]);
                        }

                        String css = "<style>\n" +
                                ".wrapper {\n" +
                                "    line-height: 1.8\n" +
                                "}\n" +
                                "</style>";

                        String wanabe = css + "<div class=\"wrapper\">" + contentBeritas + "</div>";


                        if (position == 0) {
                            ll_left.setVisibility(View.GONE);
                            ll_series_counter.setVisibility(View.GONE);
                            series_titler.setText(readResponse.getSerial().get(position + 1).getPostSeriesTitle());
                            txt_berita.loadData(wanabe, "text/html; charset=utf-8", "utf-8");
                        } else {
                            ll_right.setVisibility(View.VISIBLE);
                            txt_berita.loadData(wanabe, "text/html; charset=utf-8", "utf-8");
                            if (readResponse.getSerial().get(position - 1).getPostSeriesTitle().equals("")) {
                                series_titlel.setText("Intro");
                                series_titler.setText(readResponse.getSerial().get(position + 1).getPostSeriesTitle());
                            } else {
                                series_titlel.setText(readResponse.getSerial().get(position - 1).getPostSeriesTitle());
                                series_titler.setText(readResponse.getSerial().get(position + 1).getPostSeriesTitle());

                            }


                        }

//                    series_titlel.setText(readResponse.getSerial().get(position - 1).getPostSeriesTitle());
//                    series_titler.setText(readResponse.getSerial().get(position + 1).getPostSeriesTitle());
                    }
                });

//                series_titlel.setText(readResponse.getSerial().get(position - 1).getPostSeriesTitle());

            } else {
                series_ll.setVisibility(View.GONE);
            }

            rv_live.setHasFixedSize(true);
            lm_live = new LinearLayoutManager(getActivity());
            rv_live.setLayoutManager(lm_live);


            if (readResponse.getIsLive().equals("1")) {
                islive.setVisibility(View.VISIBLE);
            }

            if (readResponse.getImageCaption().equals("")) {
                img_desc.setVisibility(View.GONE);
            } else {
                img_desc.setText(readResponse.getImageCaption());
            }

            img_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
//                i.putExtra(Intent.EXTRA_SUBJECT, readResponse.getTitle());
                    i.putExtra(Intent.EXTRA_TEXT, readResponse.getTitle() + "\n" + readResponse.getPost_url());
                    startActivity(Intent.createChooser(i, "Share Via"));
                }
            });

            for (int i = 0; i < readResponse.getTerkait().size(); i++) {
                if (readResponse.getTerkait().get(i).getPostDate() != null) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        Date mDate = sdf.parse(readResponse.getTerkait().get(i).getPostDate());

                        DateFormat format = new SimpleDateFormat("dd MMM yyyy , HH:mm");
                        String newformat = format.format(mDate);

                        readResponse.getTerkait().get(i).setFull_short_month(newformat + " wib");


                        long timeInMilliseconds = mDate.getTime();
                        readResponse.getTerkait().get(i).setMilis_postdate(timeInMilliseconds);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }

            prefsprivate = getActivity().getSharedPreferences(PREFS_PRIVATE, Context.MODE_PRIVATE);
            String username_ss = prefsprivate.getString(BisnisComOfficialPreferences.Username, "Username");
            if (readResponse.getParentCategory().getName().equals("KORAN BISNIS")) {

                if (username_ss.equals("Username")) {
                    korbanHide.setVisibility(View.GONE);
                    ll_premium.setVisibility(View.VISIBLE);
                } else {
                    korbanHide.setVisibility(View.VISIBLE);
                    ll_premium.setVisibility(View.GONE);
                }


            }


            String conten_berita = readResponse.getContent();
            String[] splitz = conten_berita.toString().split("</p><p>");
            String css_border = "<style>\n" +
                    ".wrapper_border {\n" +
                    "border: 1px solid #888888;" +
                    "margin-bottom: 10px;\n" +
                    "    padding: 0px 15px 5px;\n" +
                    "}\n" + "ul {\n" +
                    "  list-style: none;\n" +
                    "  padding: 0;\n" +
                    "  margin: 0;\n" +
                    "}\n" +
                    "\n" +
                    "li {\n" +
                    "  padding-left: 1em; \n" +
                    "  text-indent: -.7em;\n" +
                    "}\n" +
                    "\n" +
                    "li::before {\n" +
                    "  content: \"• \";\n" +
                    "  color: #0D5B98;\n" +
                    "  transform:scale(3.7);\n" +
                    "</style>";

            String css_content = "<style>" +
                    "img{" +
                    "width:100% !important;\n" +
                    "}" +
                    "table{" +
                    "width:100% !important;\n" +
                    "}" +
                    "p{" +
                    "padding-bottom: 10px;\n" +
                    "}" +
                    "</style>";

            String contentBerita = css_content + "";
            for (int i = 0; i < splitz.length; i++) {
                if (i == 1) {

                    if (readResponse.getTitle1().equals("")) {
                        contentBerita = String.format(Locale.US, "%s%s</p>", contentBerita, splitz[i]);
                    } else if (readResponse.getTitle2().equals("")) {

                        baca1 = readResponse.getUrl1();

                        String html = splitz[i] +
                                "</p>" + css_border + "<div class=\"wrapper_border\">" + "<p>" +
                                "<h3 style=\"color:#0D5B98;\" >Baca Juga :</h3>" +
                                "<ul>" +
                                "<li><a href=\"" + baca1 + "\"style=\"text-decoration:none; color: #06274b;\">" + readResponse.getTitle1() + "</a></li>" +
                                "</ul></p></div>";
                        contentBerita = String.format(Locale.US, "%s%s", contentBerita, html);
                    } else if (readResponse.getTitle3().equals("")) {

                        baca1 = readResponse.getUrl1();
                        baca2 = readResponse.getUrl2();
                        String html = splitz[i] +
                                "</p>" + css_border + "<div class=\"wrapper_border\">" + "<p>" +
                                "<h3 style=\"color:#0D5B98;\" >Baca Juga :</h3>" +
                                "<ul>" +
                                "<li><a href=\"" + baca1 + "\"style=\"text-decoration:none; color: #06274b;\">" + readResponse.getTitle1() + "</a></li>" +
                                "<li><a href=\"" + baca2 + "\"style=\"text-decoration:none; color: #06274b;\">" + readResponse.getTitle2() + "</a></li>" +
                                "</ul></p></div>";
                        contentBerita = String.format(Locale.US, "%s%s", contentBerita, html);
                    } else {
                        baca1 = readResponse.getUrl1();
                        baca2 = readResponse.getUrl2();
                        baca3 = readResponse.getUrl3();
                        String html = splitz[i] +
                                "</p>" + css_border + "<div class=\"wrapper_border\">" + "<p>" +
                                "<h3 style=\"color:#0D5B98;\" >Baca Juga :</h3>" +
                                "<ul>" +
                                "<li><a href=\"" + baca1 + "\"style=\"text-decoration:none; color: #06274b;\">" + readResponse.getTitle1() + "</a></li>" +
                                "<li><a href=\"" + baca2 + "\"style=\"text-decoration:none; color: #06274b;\">" + readResponse.getTitle2() + "</a></li>" +
                                "<li><a href=\"" + baca3 + "\"style=\"text-decoration:none; color: #06274b;\">" + readResponse.getTitle3() + "</a></li>" +
                                "</ul></p></div>";
                        contentBerita = String.format(Locale.US, "%s%s", contentBerita, html);
                    }


                } else {


                    contentBerita = String.format(Locale.US, "%s%s</p>", contentBerita, splitz[i]);
                }
            }


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date mDate = sdf.parse(readResponse.getDate().get0());

                DateFormat format = new SimpleDateFormat("dd MMM yyyy , HH:mm");
                String newformat = format.format(mDate);
                txt_date.setText(newformat + " wib");

                newformat_time = newformat;


            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (readResponse.getSource().equals("")) {
                txt_source.setVisibility(View.GONE);
            }
            txt_author.setText(Html.fromHtml(readResponse.getAuthorName()));
            txt_source.setText("Source : " + readResponse.getSource());
            txt_editor.setText("Editor : " + readResponse.getEditorName());
            txt_title_Berita.setText(readResponse.getTitle());

            if (readResponse.getParentCategory().getName().equals("KORAN BISNIS")) {
                myoptions_thumbnail = new RequestOptions()
                        .placeholder(R.drawable.placeholder_koran)
                        .error(R.drawable.placeholder_koran);
            } else {

                myoptions_thumbnail = new RequestOptions()
                        .placeholder(R.drawable.placeholder_large)
                        .error(R.drawable.placeholder_large);
            }

            Glide.with(getActivity()).load(readResponse.getImageContentUrl()).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                img_berita.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    return false;
                }
            }).apply(myoptions_thumbnail).into(img_berita);


            String css = "<style>\n" +
                    ".wrapper {\n" +
                    "    line-height: 1.8;\n" +
//                    "    margin-bottom: 20;"+
                    "}\n" +
                    "</style>";

            String wanabe = css + "<div class=\"wrapper\">" + contentBerita + "</div>";


            txt_berita.loadData(wanabe, "text/html; charset=utf-8", "utf-8");

            if (readResponse.getTerkait() != null) {
                adapterBeritaTerkait = new AdapterBeritaTerkait(getActivity(), readResponse.getTerkait());
                rv_terkait.setAdapter(adapterBeritaTerkait);
            } else {
                ll_terkait.setVisibility(View.GONE);
            }

            if (readResponse.getLive() != null) {
                adapterLive = new AdapterLive(getActivity(), readResponse.getLive());
                rv_live.setAdapter(adapterLive);
            }


            String parent_id = readResponse.getParentCategory().getId();
            String cat_id = readResponse.getCategory().getId();

            if (!parent_id.equals("")) {
                getparentCategory(parent_id);
            } else {
                if (cat_id.equals("390")) {
                    img_kanal.setImageResource(R.drawable.c_ramadhan);
                }
            }


        } else {
            Toast.makeText(getActivity(), "Artikel Tidak ditemukan", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void DetailError(String response_message) {
        getdialogerror(response_message);
    }

    public class MyWebViewClient extends WebViewClient {
        @Override

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d("Digidaw", url);

            if (!url.equals("")) {
                String[] splitz = url.toString().split("/");

                Intent singledetail = new Intent(getActivity(), ActivitySingleDetail.class);
                singledetail.putExtra("post_date", splitz[4]);
                singledetail.putExtra("categoty_id", splitz[5]);
                singledetail.putExtra("post_id", splitz[6]);
                getActivity().startActivity(singledetail);
            }

            return true;
        }
    }
}
