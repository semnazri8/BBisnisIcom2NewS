package bisnis.com.official.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONObject;

import java.util.Arrays;

import bisnis.com.official.Network.ConnectionDetector;
import bisnis.com.official.Pojo.LoginResponse;
import bisnis.com.official.Presenter.LoginPresenter;
import bisnis.com.official.Presenter.LoginPresenterImp;
import bisnis.com.official.R;
import bisnis.com.official.Utilities.BisnisComOfficialPreferences;
import bisnis.com.official.View.LoginView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 07/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class Fragment_Profile extends Fragment implements LoginView {

    public static final String PREFS_PRIVATE = "PREFS_PRIVATE";
    private View view;
    private LoginPresenter loginPresenter;
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private MaterialDialog mDialog, dialog_muter;
    private EditText edt_username, edt_password;
    private Button btn_login, btn_login_fb, btn_login_gplus;
    private TextView txt_forgot, register;
    private String username, password;
    private LinearLayout parent;
    private ImageView img_eye, img_eye_slash;
    private FragmentManager fragmentManager;
    private FragmentTransaction ft;
    private SharedPreferences prefsprivate;
    private CallbackManager callbackManager;
    private String url = "https://id.bisnis.com/registrasi";
    private String urlF = "https://id.bisnis.com/forgot";
    private AccessTokenTracker accessTokenTracker;
    private AccessToken accessToken;
    private GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 2;
    private String TAG = "FragmentProfile";
    private TextView.OnEditorActionListener mOnEditorAction =
            new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                        checkconnection(edt_username.getText().toString(), edt_password.getText().toString());

// loginPresenter.doLogin(edt_username.getText().toString(), edt_password.getText().toString());
                        return true;
                    }
                    return false;
                }
            };

    public static void hideSoftKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((AppCompatActivity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(getActivity());
                    return false;
                }
            });
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cd = new ConnectionDetector(getActivity());
        loginPresenter = new LoginPresenterImp(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        edt_username = view.findViewById(R.id.input_username);
        edt_password = view.findViewById(R.id.input_password);
        txt_forgot = view.findViewById(R.id.txt_forgot);
        btn_login = view.findViewById(R.id.btn_login);
        parent = view.findViewById(R.id.parent);
        img_eye = view.findViewById(R.id.eye);
        img_eye_slash = view.findViewById(R.id.eye_slash);
        register = view.findViewById(R.id.register);
        btn_login_fb = view.findViewById(R.id.btn_login_fb);
        btn_login_gplus = view.findViewById(R.id.btn_login_gplus);

        FacebookSdk.sdkInitialize(getContext());
        callbackManager = CallbackManager.Factory.create();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
            }
        };
        // If the access token is available already assign it.

        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "Font/roboto_bold.ttf");
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "Font/roboto_regular.ttf");
        Typeface light = Typeface.createFromAsset(getActivity().getAssets(), "Font/roboto_light.ttf");

        edt_password.setTypeface(light);
        edt_username.setTypeface(light);
        edt_password.setOnEditorActionListener(mOnEditorAction);

        setupUI(parent);

        prefsprivate = getActivity().getSharedPreferences(PREFS_PRIVATE, Context.MODE_PRIVATE);
        String username_ss = prefsprivate.getString(BisnisComOfficialPreferences.Username, "Username");
        if (!username_ss.equals("Username")) {
            FragmentProgileLogin fragment = new FragmentProgileLogin();
            fragmentManager = getActivity().getSupportFragmentManager();
            ft = fragmentManager.beginTransaction();
            ft.replace(R.id.container_body, fragment, "home").addToBackStack("menu");
            ft.commit();
//
        }


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = edt_username.getText().toString();
                password = edt_password.getText().toString();

                checkconnection(username, password);
            }
        });

        edt_password.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);

        img_eye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edt_password.setInputType(InputType.TYPE_CLASS_TEXT);

                img_eye.setVisibility(View.GONE);
                img_eye_slash.setVisibility(View.VISIBLE);

            }
        });

        img_eye_slash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edt_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                img_eye.setVisibility(View.VISIBLE);
                img_eye_slash.setVisibility(View.GONE);

            }
        });

        txt_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();

                bundle.putString("url", urlF);
                Fragment d2 = new FragmentWebView();
                d2.setArguments(bundle);

                fragmentManager = getActivity().getSupportFragmentManager();
                ft = fragmentManager.beginTransaction();
                ft.replace(R.id.container_body, d2, "profile").addToBackStack("profile");
                ft.commit();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                startActivity(i);
                Bundle bundle = new Bundle();

                bundle.putString("url", url);
                Fragment d2 = new FragmentWebView();
                d2.setArguments(bundle);

                fragmentManager = getActivity().getSupportFragmentManager();
                ft = fragmentManager.beginTransaction();
                ft.replace(R.id.container_body, d2, "profile").addToBackStack("profile");
                ft.commit();

            }
        });

        btn_login_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile,email"));

                callbackManager = CallbackManager.Factory.create();

                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(final LoginResult loginResult) {
                                // App code
                                Log.d("pler",loginResult.getAccessToken().toString());

                                Log.d("pler2",loginResult.getAccessToken().getDeclinedPermissions().toString());
                                GraphRequest request = GraphRequest.newMeRequest(
                                        loginResult.getAccessToken(),
                                        new GraphRequest.GraphJSONObjectCallback() {
                                            @Override
                                            public void onCompleted(
                                                    JSONObject object,
                                                    GraphResponse response) {

                                                String fullname = object.optString("name");
                                                String EmailSocial = object.optString("email", "");
                                                String id = object.optString("id");

                                                Log.d("aa", object.toString());
                                                Log.d("pler1",loginResult.getAccessToken().getToken());
                                                String refreshedToken = FirebaseInstanceId.getInstance().getToken();


//                                        doCheckFbCallback(EmailSocial, refreshedToken, fullname);

                                            }
                                        });

                                Bundle parameters = new Bundle();
                                parameters.putString("fields", "id,name,email");
                                request.setParameters(parameters);
                                request.executeAsync();

                                Log.e("FacebookCallback", loginResult.toString());
                            }


                            @Override
                            public void onCancel() {
                                // App code
                                LoginManager.getInstance().logOut();
                                Log.d("cancel", "cancel");
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                // App code
                                LoginManager.getInstance().logOut();
                                Log.e("error", String.valueOf(exception));
                            }
                        });


            }
        });

        btn_login_gplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mGoogleSignInClient.signOut();
    }

    private void handleSignInResult(Task<GoogleSignInAccount> task) {
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);
            Log.d("email_plus", account.getEmail());
            Log.d("tokenGPL", account.getIdToken());
            Log.d("disply", account.getDisplayName());
            // Signed in successfully, show authenticated UI.
//            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
//            updateUI(null);
        }

    }

    private void checkconnection(String username, String password) {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {

            loginPresenter.doLogin(username, password);

        } else if (isInternetPresent.equals(false)) {
            getdialogerror("Tidak ada koneksi Internet");
        }
    }

    @Override
    public void ResultLogin(String response_message, LoginResponse loginResponse) {
        dialog_muter.dismiss();

        if (loginResponse.getIsSuccess().equals(1)) {
            prefsprivate = getActivity().getSharedPreferences(PREFS_PRIVATE, Context.MODE_PRIVATE);
            SharedPreferences.Editor preEditor = prefsprivate.edit();

            preEditor.putString(BisnisComOfficialPreferences.user_id, loginResponse.getData().getId());
            preEditor.putString(BisnisComOfficialPreferences.FullName, loginResponse.getData().getFullName());
            preEditor.putString(BisnisComOfficialPreferences.Username, loginResponse.getData().getUsername());
            preEditor.putString(BisnisComOfficialPreferences.Email, loginResponse.getData().getEmail());
            preEditor.putString(BisnisComOfficialPreferences.photo_url, loginResponse.getData().getPhotoUrl());
            preEditor.putString(BisnisComOfficialPreferences.Active, loginResponse.getData().getActive());
            preEditor.putString(BisnisComOfficialPreferences.Registerdate, loginResponse.getData().getRegisterdate());
            preEditor.putString(BisnisComOfficialPreferences.user_type, loginResponse.getData().getUserType());
            preEditor.putString(BisnisComOfficialPreferences.user_type_name, loginResponse.getData().getUserTypeName());


            preEditor.commit();


            FragmentProgileLogin fragment = new FragmentProgileLogin();
            fragmentManager = getActivity().getSupportFragmentManager();
            ft = fragmentManager.beginTransaction();
            ft.replace(R.id.container_body, fragment, "home").addToBackStack("menu");
            ft.commit();

            Toast.makeText(getActivity(), "Selamat Datang\n" + loginResponse.getData().getFullName(), Toast.LENGTH_SHORT).show();
        } else {
            getdialogerrorF(loginResponse.getResponseMessage());
        }


    }

    @Override
    public void DetailError(String response_message) {
        getdialogerror(response_message);

    }

    @Override
    public void setUsernameError() {
        getdialogerrorF(getString(R.string.username_error));
//        edt_username.setError(getString(R.string.username_error));

    }

    @Override
    public void setUsernameInvalid() {
        getdialogerrorF(getString(R.string.username_invalid));
//        edt_username.setError(getString(R.string.username_invalid));
    }

    @Override
    public void setPasswordError() {
        getdialogerrorF(getString(R.string.password_null));
//        edt_password.setError(getString(R.string.password_null));
    }

    @Override
    public void setPasswordInvalid() {
        getdialogerrorF(getString(R.string.password_invalid));
//        edt_password.setError(getString(R.string.password_invalid));
    }

    @Override
    public void setAllError() {
        getdialogerrorF(getString(R.string.u_p_null));
    }

    @Override
    public void setvalid() {
        getDialog_progress();
    }

    public void getDialog_progress() {

        dialog_muter = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .cancelable(false)
                .content(R.string.please_wait)
                .progress(true, 0)
                .show();
    }

    private void getdialogerror(String response_message) {
        dialog_muter.dismiss();
        mDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .content(response_message)
                .cancelable(false)
                .positiveText("Close")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mDialog.dismiss();
                    }
                })
                .show();
    }

    private void getdialogerrorF(String response_message) {
        mDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .cancelable(false)
                .content(response_message)
                .positiveText("Close")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mDialog.dismiss();
                    }
                })
                .show();
    }
}
