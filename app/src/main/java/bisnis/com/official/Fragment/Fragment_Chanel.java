package bisnis.com.official.Fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import bisnis.com.official.Adapter.ChanelAdapter;
import bisnis.com.official.Model.ModelChanel;
import bisnis.com.official.R;
import bisnis.com.official.Utilities.SpacesItemDecoration;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 09/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class Fragment_Chanel extends Fragment {
    private ArrayList<ModelChanel> modelDataList;
    private ChanelAdapter adapter;
    private LinearLayoutManager lm;
    private GridLayoutManager glm;
    private View view;
    private RecyclerView recyclerView;
    private ImageView img_src, img_logo, img_back, img_close_src;
    private LinearLayout ll_search, ll_kanal_nav;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_chanel, container, false);
        img_logo = view.findViewById(R.id.img_bisnis);
        img_back = view.findViewById(R.id.img_close_search);
        ll_search = view.findViewById(R.id.ll_search);
        img_src = view.findViewById(R.id.img_search);
        img_src.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_logo.setVisibility(View.GONE);
                ll_search.setVisibility(View.VISIBLE);
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_logo.setVisibility(View.VISIBLE);
                ll_search.setVisibility(View.GONE);
            }
        });
        modelDataList = getData();
        adapter = new ChanelAdapter(modelDataList, getActivity());
        glm = new GridLayoutManager(getActivity(), 2);
        glm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case ModelChanel.ChanelGrid:
                        return 1;
                    case ModelChanel.ChanelList:
                        return 2;
                    default:
                        return 1;
                }
            }
        });
        recyclerView = view.findViewById(R.id.rv_chanel);
        recyclerView.setLayoutManager(glm);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        recyclerView.setAdapter(adapter);

        return view;
    }

    private ArrayList<ModelChanel> getData() {

        ArrayList<ModelChanel> chanels = new ArrayList<>();

        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Market", R.drawable.c1, 194));
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Finansial", R.drawable.c2, 5));
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Industri", R.drawable.c3, 43));
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Kabar24", R.drawable.c4, 186));
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Life & Style", R.drawable.c5, 197));
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Regional", R.drawable.c6, 0)); //masih defeult
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Properti", R.drawable.c7, 47));
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Bola", R.drawable.c8, 392));
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Gadget", R.drawable.c9, 277));
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Traveling", R.drawable.c10, 222));
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Sport", R.drawable.c11, 57));
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Otomotif", R.drawable.c12, 272));
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Manajemen", R.drawable.c14, 52));
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Enterpreneur", R.drawable.c15, 258));
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Info", R.drawable.c16, 243));
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Inforial", R.drawable.c17, 73));
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Syariah", R.drawable.c18, 231));
        chanels.add(new ModelChanel(ModelChanel.ChanelList, "Koran Bisnis Indonesia", R.drawable.c20, 242));
//        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Foto", R.drawable.c13,1));//foto default
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Solopos", R.drawable.c21, 3));//solopos default intent browser
        chanels.add(new ModelChanel(ModelChanel.ChanelGrid, "Harian Jogja", R.drawable.c22, 4)); //harjo defaukt intent browser
        chanels.add(new ModelChanel(ModelChanel.ChanelList, "Indeks Berita", R.drawable.c32,2));//bisnis tv default


        return chanels;
    }
}
