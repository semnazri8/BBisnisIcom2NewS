package bisnis.com.official.Fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import bisnis.com.official.ActivityDeatilBerita;
import bisnis.com.official.Adapter.MultiViewAdapter;
import bisnis.com.official.Listener.OnClickAdapterListener;
import bisnis.com.official.Model.ModelData;
import bisnis.com.official.Network.ConnectionDetector;
import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.Presenter.BreakingPresenter;
import bisnis.com.official.Presenter.BreakingPresenterImp;
import bisnis.com.official.Presenter.LoadMorePresenter;
import bisnis.com.official.Presenter.LoadMorePresenterImp;
import bisnis.com.official.R;
import bisnis.com.official.Utilities.AnalyticsApplication;
import bisnis.com.official.Utilities.SpacesItemDecoration;
import bisnis.com.official.View.BreakingView;
import bisnis.com.official.View.LoadMoreView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 09/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class Fragment_Breaking extends Fragment implements BreakingView, LoadMoreView, OnClickAdapterListener {
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private ArrayList<BreakingResponse> braking;
    private MultiViewAdapter adapter;
    private GridLayoutManager glm;
    private View view;
    private RecyclerView recyclerView;
    private FloatingActionButton fab;
    private BreakingPresenter breakingPresenter;
    private LoadMorePresenter loadMorePresenter;
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private MaterialDialog mDialog, dialog_muter;
    private SwipeRefreshLayout srl;
    private String type_get;
    private boolean loading = false;
    private int page = 2;
    private int kurangin = 1;
    private AnalyticsApplication application;
    private Tracker mTracker;
    private boolean isFragmentLoaded = false;
    private boolean _hasLoadedOnce = false; // your boolean field

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        cd = new ConnectionDetector(getActivity());
        breakingPresenter = new BreakingPresenterImp(this);
        loadMorePresenter = new LoadMorePresenterImp(this);

    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser && isResumed()) { // fragment is visible and have created
//            checkConnections();
//        }
//    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_breaking, container, false);
        fab = view.findViewById(R.id.fab);
        srl = view.findViewById(R.id.srl_list_dash);
        recyclerView = view.findViewById(R.id.rv_berita);
        type_get = getArguments().getString("title");

//        if (getUserVisibleHint() && type_get.equals("Terbaru") && !isResumed()) {
        checkConnections();
//        }


        recyclerView.setItemAnimator(new DefaultItemAnimator());
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fab.getVisibility() == View.GONE) {
                    fab.show();
                } else if (dy < 0 && fab.getVisibility() == View.VISIBLE) {
                    fab.hide();
                }
            }
        });

        srl.setColorSchemeColors(getResources().getIntArray(R.array.intarr_swipe_refresh_layout));
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                checkConnections();
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);
            }
        });
        glm = new GridLayoutManager(getActivity(), 2);
        glm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case ModelData.News_grid:
                        return 1;
                    case ModelData.News_Thumbnail:
                        return 2;
                    case ModelData.News_list:
                        return 2;
                    case ModelData.News_ads:
                        return 2;
                    default:
                        return 1;
                }
            }
        });
        recyclerView.setLayoutManager(glm);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    int lastIndex = glm.findLastCompletelyVisibleItemPosition() + 1;
                    int totalItem = adapter.getItemCount();

                    visibleItemCount = glm.getChildCount();
                    totalItemCount = adapter.getItemCount();
                    pastVisiblesItems = glm.findFirstVisibleItemPosition() + 1;

                    if (!loading && lastIndex == totalItem) {
//                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        loading = true;
                        srl.setRefreshing(true);
                        Log.v("...", "Last Item Wow !");
                        //Do pagination.. i.e. fetch new data
                        if (type_get.equals("Terbaru")) {
                            loadMorePresenter.getLoadMore("0", type_get, String.valueOf(page++));
                        } else if (type_get.equals("Market")) {
                            loadMorePresenter.getLoadMore("194", type_get, String.valueOf(page++));
                        } else if (type_get.equals("Industri")) {
                            loadMorePresenter.getLoadMore("43", type_get, String.valueOf(page++));
                        } else if (type_get.equals("Finansial")) {
                            loadMorePresenter.getLoadMore("5", type_get, String.valueOf(page++));
                        } else if (type_get.equals("Headline")) {
                            srl.setRefreshing(false);
                        } else if (type_get.equals("Hot Topic")) {
                            srl.setRefreshing(false);
//                            loadMorePresenter.getLoadMore("0", type_get, String.valueOf(page++));
                        } else {
                            srl.setRefreshing(false);
//                            loadMorePresenter.getLoadMore("0", type_get, String.valueOf(page++));
                        }

//                        }
                    }
                }


            }
        });
        return view;
    }

    private void checkConnections() {
        getDialog_progress();
        adapter = new MultiViewAdapter(getActivity(), this);
        recyclerView.setAdapter(adapter);
        application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

        type_get = getArguments().getString("title");
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            //TODO : nanti ini untuk set breaking, headline, dll

            loading = true;
            if (type_get.equals("Terbaru")) {
                mTracker.setScreenName("Android/Home/Terbaru");
                breakingPresenter.getBreaking("0", type_get);
            } else if (type_get.equals("Headline")) {
                mTracker.setScreenName("Android/Home/Headline");
                breakingPresenter.getBreaking("0", type_get);
            } else if (type_get.equals("Market")) {
                breakingPresenter.getBreaking("194", type_get);
                mTracker.setScreenName("Android/Home/Market");
            } else if (type_get.equals("Industri")) {
                breakingPresenter.getBreaking("43", type_get);
                mTracker.setScreenName("Android/Home/Industri");
            } else if (type_get.equals("Finansial")) {
                breakingPresenter.getBreaking("5", type_get);
                mTracker.setScreenName("Android/Home/Finansial");
            } else if (type_get.equals("Hot Topic")) {
                mTracker.setScreenName("Android/Home/Hot_Topic");
                breakingPresenter.getBreaking("0", type_get);
            } else if (type_get.equals("Terpopuler")) {
                mTracker.setScreenName("Android/Home/Terpopuler");
                breakingPresenter.getBreaking("0", type_get);
            }


            mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        } else if (isInternetPresent.equals(false)) {
            getdialogerror("Tidak ada koneksi Internet");
            srl.setRefreshing(false);
        }
    }

    public void getDialog_progress() {

        dialog_muter = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .content(R.string.please_wait)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    private void getdialogerror(String response_message) {
        dialog_muter.dismiss();
        mDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .content(response_message)
                .positiveText("Close")
                .cancelable(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mDialog.dismiss();

                    }
                })
                .show();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("digidaw", type_get);
        if (getUserVisibleHint() && type_get.equals("Terbaru") && !isResumed()) {
            checkConnections();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        breakingPresenter.onDestroy();
        loadMorePresenter.onDestroy();
    }

    @Override
    public void ResultBreaking(String response_message, ArrayList<BreakingResponse> breakingResponse) {
        dialog_muter.dismiss();
        loading = false;
        if (breakingResponse.size() != 0) {
            braking = breakingResponse;
            ParseType1(breakingResponse);
        }

    }

    public void ParseType1(ArrayList<BreakingResponse> breakingResponse) {
        onLoadNewComplete();
        braking = breakingResponse;


        for (int i = 0; i < breakingResponse.size(); i++) {
            if (breakingResponse.get(i).getPostDate() != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    Date mDate = sdf.parse(breakingResponse.get(i).getPostDate());

//                    if (type_get.equals("Hot Topic")) {
//                        DateFormat format = new SimpleDateFormat("dd MMM yyyy");
//                        String newformat = format.format(mDate);
//
//                        breakingResponse.get(i).setFull_short_month(newformat);
//                    } else {

                    DateFormat format = new SimpleDateFormat("dd MMM yyyy, HH:mm");
                    String newformat = format.format(mDate);

                    breakingResponse.get(i).setFull_short_month(newformat + " WIB");
//                    }

                    long timeInMilliseconds = mDate.getTime();
                    breakingResponse.get(i).setMilis(timeInMilliseconds);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }


        if (type_get.equals("Terbaru")) {
            for (int i = 0; i < breakingResponse.size(); i++) {


                breakingResponse.get(i).setType_get("Terbaru");
                breakingResponse.get(i).setKanal_id("0");

                if (i == 0) {
                    breakingResponse.get(i).setType(0);
                } else if (i == 1 || i == 2) {
                    breakingResponse.get(i).setType(2);
                } else if (i % 5 == 0) {
                    breakingResponse.get(i).setType(0);
                } else {
                    breakingResponse.get(i).setType(1);
                }
            }

            if (breakingResponse.size() != 0) {
                BreakingResponse breaking = new BreakingResponse();
                breaking.setImage_ads(R.drawable.banner_and);
                breaking.setType(3);

                breakingResponse.add(3, breaking);
            }


        } else if (type_get.equals("Headline")) {
            for (int i = 0; i < breakingResponse.size(); i++) {

                breakingResponse.get(i).setType_get("Headline");
                breakingResponse.get(i).setType(0);
                breakingResponse.get(i).setKanal_id("0");
                breakingResponse.get(i).setMilis(000);
            }
        } else if (type_get.equals("Market")) {
            for (int i = 0; i < breakingResponse.size(); i++) {


                breakingResponse.get(i).setType_get("Market");
                breakingResponse.get(i).setKanal_id("194");
                breakingResponse.get(i).setType(1);
                breakingResponse.get(i).setCategoryParentName(breakingResponse.get(i).getCategoryName());
            }

        } else if (type_get.equals("Industri")) {
            for (int i = 0; i < breakingResponse.size(); i++) {


                breakingResponse.get(i).setType_get("Industri");
                breakingResponse.get(i).setKanal_id("43");
                breakingResponse.get(i).setCategoryParentName(breakingResponse.get(i).getCategoryName());
                breakingResponse.get(i).setType(1);
            }

        } else if (type_get.equals("Finansial")) {
            for (int i = 0; i < breakingResponse.size(); i++) {
                breakingResponse.get(i).setCategoryParentName(breakingResponse.get(i).getCategoryName());
                breakingResponse.get(i).setType(1);
                breakingResponse.get(i).setType_get("Finansial");
                breakingResponse.get(i).setKanal_id("5");

            }

        } else if (type_get.equals("Hot Topic")) {

            for (int i = 0; i < breakingResponse.size(); i++) {
                breakingResponse.get(i).setCategoryParentName(breakingResponse.get(i).getTopic_name());

                breakingResponse.get(i).setType_get("hottopic");
                breakingResponse.get(i).setKanal_id("0");
                breakingResponse.get(i).setType(1);

            }
        } else if (type_get.equals("Terpopuler")) {
            for (int i = 0; i < breakingResponse.size(); i++) {

                breakingResponse.get(i).setCategoryParentName(breakingResponse.get(i).getCategoryName());

                breakingResponse.get(i).setType_get("Terpopuler");
                breakingResponse.get(i).setKanal_id("0");
                breakingResponse.get(i).setType(1);
            }
        }
        adapter.addOn(breakingResponse);
    }

    public void ParseType2(ArrayList<BreakingResponse> breakingResponse) {
        onLoadNewComplete();
        braking = breakingResponse;


        for (int i = 0; i < breakingResponse.size(); i++) {
            if (breakingResponse.get(i).getPostDate() != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    Date mDate = sdf.parse(breakingResponse.get(i).getPostDate());

//                    if (type_get.equals("Hot Topic")) {
//                        DateFormat format = new SimpleDateFormat("dd MMM yyyy");
//                        String newformat = format.format(mDate);
//
//                        breakingResponse.get(i).setFull_short_month(newformat);
//                    } else {

                    DateFormat format = new SimpleDateFormat("dd MMM yyyy, HH:mm");
                    String newformat = format.format(mDate);

                    breakingResponse.get(i).setFull_short_month(newformat + " WIB");
//                    }

                    long timeInMilliseconds = mDate.getTime();
                    breakingResponse.get(i).setMilis(timeInMilliseconds);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        if (type_get.equals("Market")) {
            for (int i = 0; i < breakingResponse.size(); i++) {


                breakingResponse.get(i).setType_get("Market");
                breakingResponse.get(i).setKanal_id("194");
                breakingResponse.get(i).setType(1);
                breakingResponse.get(i).setCategoryParentName(breakingResponse.get(i).getCategoryName());
            }

        } else if (type_get.equals("Industri")) {
            for (int i = 0; i < breakingResponse.size(); i++) {


                breakingResponse.get(i).setType_get("Industri");
                breakingResponse.get(i).setKanal_id("43");
                breakingResponse.get(i).setCategoryParentName(breakingResponse.get(i).getCategoryName());
                breakingResponse.get(i).setType(1);
            }

        } else if (type_get.equals("Finansial")) {
            for (int i = 0; i < breakingResponse.size(); i++) {
                breakingResponse.get(i).setCategoryParentName(breakingResponse.get(i).getCategoryName());
                breakingResponse.get(i).setType(1);
                breakingResponse.get(i).setType_get("Finansial");
                breakingResponse.get(i).setKanal_id("5");

            }


        } else {

            for (int i = 0; i < breakingResponse.size(); i++) {


                breakingResponse.get(i).setType_get("Terbaru");
                breakingResponse.get(i).setKanal_id("0");

                if (i == 0) {
                    breakingResponse.get(i).setType(0);
                } else if (i == 1 || i == 2) {
                    breakingResponse.get(i).setType(2);
                } else if (i % 5 == 0) {
                    breakingResponse.get(i).setType(0);
                } else {
                    breakingResponse.get(i).setType(1);
                }

            }
        }
        adapter.addOn(breakingResponse);
    }

    private void onLoadNewComplete() {
        srl.setRefreshing(false);
    }

    @Override
    public void ResultLoadMore(String response_message, ArrayList<BreakingResponse> breakingResponse) {
        onLoadNewComplete();

        loading = false;
        if (breakingResponse.size() > 0) {
            ParseType2(breakingResponse);
        }
    }

    @Override
    public void BreakingError(String response_message) {
        loading = false;
        getdialogerror(response_message);
    }

    @Override
    public void onClick(String position, String type_get, String kanal_id) {

        Intent i = new Intent(getActivity(), ActivityDeatilBerita.class);
        i.putExtra("index", position);
        i.putExtra("type_get", type_get);
        i.putExtra("kanal_id", kanal_id);
        i.putExtra("parcel", braking);
        i.putExtra("helo", "breaking");

//        i.putExtras(bundle);

        getActivity().startActivity(i);

    }

    @Override
    public void onClickAja(String position, String type_get, String kanal_id, ArrayList<BreakingResponse> dataSet) {
        Intent i = new Intent(getActivity(), ActivityDeatilBerita.class);
        i.putExtra("index", position);
        i.putExtra("type_get", type_get);
        i.putExtra("kanal_id", kanal_id);
        i.putExtra("parcel", dataSet);
        i.putExtra("helo", "breaking");

//        i.putExtras(bundle);

        getActivity().startActivity(i);
    }

    @Override
    public void onClickHeadline(String position, String type_get, String kanal_id) {

    }
}
