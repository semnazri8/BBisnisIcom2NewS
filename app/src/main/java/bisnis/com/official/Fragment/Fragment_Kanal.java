package bisnis.com.official.Fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import bisnis.com.official.ActivityDeatilBerita;
import bisnis.com.official.ActivityDeatilBeritaH;
import bisnis.com.official.Adapter.HeadlineAdaterNew;
import bisnis.com.official.Adapter.KanalMultiViewAdapter;
import bisnis.com.official.Listener.OnClickAdapterListener;
import bisnis.com.official.Model.ModelData;
import bisnis.com.official.Network.ConnectionDetector;
import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.Pojo.HeadlineResponse;
import bisnis.com.official.Presenter.BreakingPresenter;
import bisnis.com.official.Presenter.BreakingPresenterImp;
import bisnis.com.official.Presenter.HeadlinePresenter;
import bisnis.com.official.Presenter.HeadlinePresenterImp;
import bisnis.com.official.Presenter.KanalPresenter;
import bisnis.com.official.Presenter.LoadMorePresenter;
import bisnis.com.official.Presenter.LoadMorePresenterImp;
import bisnis.com.official.R;
import bisnis.com.official.Utilities.SpacesItemDecoration;
import bisnis.com.official.View.BreakingView;
import bisnis.com.official.View.HeadlineVIew;
import bisnis.com.official.View.LoadMoreView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 09/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class Fragment_Kanal extends Fragment implements BreakingView, OnClickAdapterListener, HeadlineVIew, LoadMoreView {
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private KanalMultiViewAdapter adapter;
    private HeadlineAdaterNew adapter_headlineNew;
    private GridLayoutManager glm;
    private View view;
    private LinearLayoutManager lm;
    private RecyclerView recyclerView, rv_headline;
    private FloatingActionButton fab;
    private BreakingPresenter breakingPresenter;
    private HeadlinePresenter headlinePresenter;
    private KanalPresenter kanalPresenter;
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private MaterialDialog mDialog, dialog_muter;
    private SwipeRefreshLayout srl;
    private String kanal_id, type_get, type_id;
    private NestedScrollView nesterd;
    private ArrayList<BreakingResponse> braking;
    private ArrayList<HeadlineResponse> headlines;
    private boolean loading = false;
    private int page = 2;
    private LoadMorePresenter loadMorePresenter;
    private int canloadmore = 1;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        cd = new ConnectionDetector(getActivity());
        breakingPresenter = new BreakingPresenterImp(this);
//        kanalPresenter = new KanalPresenterImp(this);
        headlinePresenter = new HeadlinePresenterImp(this);
        loadMorePresenter = new LoadMorePresenterImp(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_breaking_kanal, container, false);
        fab = view.findViewById(R.id.fab);
        srl = view.findViewById(R.id.srl_list_dash);
        recyclerView = view.findViewById(R.id.rv_berita);
        rv_headline = view.findViewById(R.id.rv_headline);
        nesterd = view.findViewById(R.id.nesterd);

        kanal_id = getArguments().getString("kanal_id");
        type_get = getArguments().getString("title");
        type_id = getArguments().getString("title_id");


        checkConnections(kanal_id);
        srl.setColorSchemeColors(getResources().getIntArray(R.array.intarr_swipe_refresh_layout));
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                checkConnections(kanal_id);
            }
        });


        recyclerView.setHasFixedSize(true);
        rv_headline.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        rv_headline.setNestedScrollingEnabled(false);
        glm = new GridLayoutManager(getActivity(), 2);
        glm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case ModelData.News_grid:
                        return 1;
                    case ModelData.News_Thumbnail:
                        return 2;
                    case ModelData.News_list:
                        return 2;
                    case ModelData.News_ads:
                        return 2;
                    case ModelData.NewsIndex:
                        return 2;
                    default:
                        return -1;
                }
            }
        });
        recyclerView.setLayoutManager(glm);
        nesterd.setSmoothScrollingEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            nesterd.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (scrollY > 0 && fab.getVisibility() == View.GONE) {
                        fab.show();
                        srl.setEnabled(false);
                    } else if (scrollY <= 0 && fab.getVisibility() == View.VISIBLE) {
                        fab.hide();
                        srl.setEnabled(true);
                    }
                }
            });
        }

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        nesterd.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {

                        visibleItemCount = glm.getChildCount();
                        totalItemCount = glm.getItemCount();
                        pastVisiblesItems = glm.findFirstVisibleItemPosition() + 1;

                        if (oldScrollY > 0) {
                            int lastIndex = glm.findLastCompletelyVisibleItemPosition() + 1;
                            int totalItem = adapter.getItemCount();

                            visibleItemCount = glm.getChildCount();
                            totalItemCount = adapter.getItemCount();
                            pastVisiblesItems = glm.findFirstVisibleItemPosition() + 1;

                            if (!loading && lastIndex == totalItem) {
//                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                loading = true;
                                srl.setRefreshing(true);
                                Log.v("...", "Last Item Wow !");
                                //Do pagination.. i.e. fetch new data
                                if (type_get.equals("Terbaru")) {
                                    loadMorePresenter.getLoadMore(kanal_id, type_get, String.valueOf(page++));
                                } else {
//                                    canloadmore = 1;
                                    if (canloadmore == 1) {
                                        srl.setRefreshing(false);
                                        loadMorePresenter.getLoadMore(type_id, type_get, String.valueOf(page++));
                                    } else {
                                        srl.setRefreshing(false);
                                    }

                                }

                            }
                        }

                    }
                }
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nesterd.smoothScrollTo(0, 0);
                //TODO Harus cari cara lain nih
//                nesterd.fullScroll(View.FOCUS_UP);
            }

        });
        return view;
    }

    private void checkConnections(String kanal_id) {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            getDialog_progress();
            if (type_id.equals("0")) {
                if (kanal_id.equals("242")) {
                    breakingPresenter.getBreaking(kanal_id, type_get);
                    headlinePresenter.getHeadline(kanal_id, type_get);

                } else {
                    Log.d("kanal", kanal_id);
                    Log.d("type_get", type_get);
                    Log.d("type_id", type_id);
                    headlinePresenter.getHeadline(kanal_id, type_get);
                    breakingPresenter.getBreaking(kanal_id, type_get);

                }

            } else {
                breakingPresenter.getBreaking(type_id, type_get);
            }
            //TODO : nanti ini untuk set breaking, headline, dll
            adapter = new KanalMultiViewAdapter(getActivity(), this);
            recyclerView.setAdapter(adapter);
            loading = true;

        } else if (isInternetPresent.equals(false)) {
            getdialogerror("Tidak ada koneksi Internet");
            srl.setRefreshing(false);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        breakingPresenter.onDestroy();
//        kanalPresenter.onDestroy();
    }

    public void getDialog_progress() {

        dialog_muter = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .content(R.string.please_wait)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    private void getdialogerror(String response_message) {
        dialog_muter.dismiss();
        mDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .content(response_message)
                .positiveText("Close")
                .cancelable(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mDialog.dismiss();

                    }
                })
                .show();
    }


    @Override
    public void ResultBreaking(String response_message, ArrayList<BreakingResponse> breakingResponse) {
        dialog_muter.dismiss();
        loading = false;
        onLoadNewComplete();
        braking = breakingResponse;
        ParseType(breakingResponse);


    }

    private void onLoadNewComplete() {
        srl.setRefreshing(false);
    }


    @Override
    public void ResultLoadMore(String response_message, ArrayList<BreakingResponse> breakingResponse) {

//        Toast.makeText(getActivity(), String.valueOf(breakingResponse.size()), Toast.LENGTH_SHORT).show();

        braking = breakingResponse;
        loading = false;
        if (breakingResponse.size() > 0) {
            ParseType2(breakingResponse);
        } else {

            if (canloadmore == 1) {
                canloadmore = 0;
//                Toast.makeText(getActivity(), "Add View Here", Toast.LENGTH_SHORT).show();

                BreakingResponse breaking = new BreakingResponse();
                breaking.setType(5);
                breakingResponse.add(0, breaking);
//                ParseType2(breakingResponse);
                adapter.addOn(breakingResponse);
            }

            onLoadNewComplete();


        }
    }

    private void ParseType2(ArrayList<BreakingResponse> breakingResponse) {

        onLoadNewComplete();
        for (int i = 0; i < breakingResponse.size(); i++) {
            breakingResponse.get(i).setType_get(type_get);
            breakingResponse.get(i).setKanal_id(type_id);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date mDate = sdf.parse(breakingResponse.get(i).getPostDate());

                DateFormat format = new SimpleDateFormat("dd MMM yyyy, HH:mm");
                String newformat = format.format(mDate);

                breakingResponse.get(i).setFull_short_month(newformat + " wib");


                long timeInMilliseconds = mDate.getTime();
                breakingResponse.get(i).setMilis(timeInMilliseconds);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            Log.d("cat_par_nam", breakingResponse.get(i).getCategoryParentName());

            if (breakingResponse.get(i).getCategoryParentName().equals("KORAN")) {
                breakingResponse.get(i).setDiakoran("KORAN");
            }


            if (type_id.equals("0")) {
                breakingResponse.get(i).setCategoryParentName(breakingResponse.get(i).getCategoryName());
                breakingResponse.get(i).setKanal_id(kanal_id);

            } else if (!type_id.equals("0")) {
                breakingResponse.get(i).setCategoryParentName("");
            }

            if (i == 0) {
                breakingResponse.get(i).setType(1);
            } else if (i % 5 == 0) {
                breakingResponse.get(i).setType(0);
            } else {
                breakingResponse.get(i).setType(1);
            }

        }

//        if (type_id.equals("0") || type_id.equals("244")) {
//            BreakingResponse breaking = new BreakingResponse();
//            breaking.setImage_ads(R.drawable.banner_and);
//            breaking.setType(3);
//
//            breakingResponse.add(0, breaking);
//        }

        adapter.addOn(breakingResponse);


    }

    private void ParseType(ArrayList<BreakingResponse> breakingResponse) {
        onLoadNewComplete();
        for (int i = 0; i < breakingResponse.size(); i++) {
            breakingResponse.get(i).setType_get(type_get);
            breakingResponse.get(i).setKanal_id(type_id);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date mDate = sdf.parse(breakingResponse.get(i).getPostDate());

                DateFormat format = new SimpleDateFormat("dd MMM yyyy, HH:mm");
                String newformat = format.format(mDate);

                breakingResponse.get(i).setFull_short_month(newformat + " wib");


                long timeInMilliseconds = mDate.getTime();
                breakingResponse.get(i).setMilis(timeInMilliseconds);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            Log.d("cat_par_nam", breakingResponse.get(i).getCategoryParentName());

            if (breakingResponse.get(i).getCategoryParentName().equals("KORAN")) {
                breakingResponse.get(i).setDiakoran("KORAN");
            }


            if (type_id.equals("0")) {
                breakingResponse.get(i).setCategoryParentName(breakingResponse.get(i).getCategoryName());
                breakingResponse.get(i).setKanal_id(kanal_id);

            } else if (!type_id.equals("0")) {
                breakingResponse.get(i).setCategoryParentName("");
            }

            if (i == 0) {
                breakingResponse.get(i).setType(1);
            } else if (i % 5 == 0) {
                breakingResponse.get(i).setType(0);
            } else {
                breakingResponse.get(i).setType(1);
            }

        }

        if (type_id.equals("0") || type_id.equals("244")) {
            BreakingResponse breaking = new BreakingResponse();
            breaking.setImage_ads(R.drawable.banner_and);
            breaking.setType(3);

            breakingResponse.add(0, breaking);
        }

        adapter.addOn(breakingResponse);

    }

    @Override
    public void BreakingError(String response_message) {
        getdialogerror(response_message);
    }

    @Override
    public void onClick(String position, String type_get, String kanal_id) {

//        Toast.makeText(getActivity(), position, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onClickAja(String position, String type_get, String kanal_id, ArrayList<BreakingResponse> dataSet) {
        Intent i = new Intent(getActivity(), ActivityDeatilBerita.class);
        i.putExtra("index", position);
        i.putExtra("type_get", type_get);
        i.putExtra("kanal_id", kanal_id);
        i.putExtra("parcel", dataSet);
        i.putExtra("helo", "breaking");
//        i.putExtras(bundle);

        getActivity().startActivity(i);
    }

    @Override
    public void onClickHeadline(String position, String type_get, String kanal_id) {
        Intent i = new Intent(getActivity(), ActivityDeatilBeritaH.class);
        i.putExtra("index", position);
        i.putExtra("type_get", type_get);
        i.putExtra("kanal_id", kanal_id);
        i.putExtra("parcel", headlines);
        i.putExtra("helo", "headline");
        //        i.putExtras(bundle);

        getActivity().startActivity(i);
    }

    @Override
    public void ResultHeadline(String response_message, ArrayList<HeadlineResponse> headlineResponses) {
        headlines = headlineResponses;
        lm = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        for (int i = 0; i < headlineResponses.size(); i++) {
            headlineResponses.get(i).setKanal_id(kanal_id);
            headlineResponses.get(i).setType_get("Headline");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date mDate = sdf.parse(headlineResponses.get(i).getPostDate());
                long timeInMilliseconds = mDate.getTime();
                headlineResponses.get(i).setMilis(timeInMilliseconds);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

//        adapter.addOnHeadline(headlineResponses);
        adapter_headlineNew = new HeadlineAdaterNew(getActivity(), headlineResponses, this);
        rv_headline.setLayoutManager(lm);
        rv_headline.setAdapter(adapter_headlineNew);
    }

    @Override
    public void HeadlineError(String response_message) {
        getdialogerror(response_message);
    }
}
