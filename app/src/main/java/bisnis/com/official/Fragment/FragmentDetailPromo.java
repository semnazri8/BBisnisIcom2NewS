package bisnis.com.official.Fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import bisnis.com.official.R;

public class FragmentDetailPromo extends AppCompatActivity {

    private TextView txt_merchant, txt_summary, txt_detail,txt_title;
    private Button btn_gotoweb;
    private ImageView img_promo;
    private ImageButton img_share, img_back;
    private String merchant, img, summary, detail, urlpromo, title;
    private AppBarLayout aawer;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.fragment_detail_promo);

        Typeface bold = Typeface.createFromAsset(getAssets(), "Font/roboto_bold.ttf");
        Typeface regular = Typeface.createFromAsset(getAssets(), "Font/roboto_regular.ttf");

        merchant = getIntent().getStringExtra("merchant");
        img = getIntent().getStringExtra("image");
        summary = getIntent().getStringExtra("summary");
        detail = getIntent().getStringExtra("detail");
        urlpromo = getIntent().getStringExtra("url_promo");
        title = getIntent().getStringExtra("title");

        toolbar = findViewById(R.id.toolbar);
        img_back = findViewById(R.id.img_back);
        img_share = findViewById(R.id.img_share);
        txt_merchant = findViewById(R.id.txt_title);
        aawer = findViewById(R.id.aawer);
        txt_summary = findViewById(R.id.promo_summary);
        txt_detail = findViewById(R.id.promo_desc);
        btn_gotoweb = findViewById(R.id.gotoweb);
        txt_title = findViewById(R.id.promo_title);
        img_promo = findViewById(R.id.img_promo_detail);

        txt_merchant.setTypeface(regular);
        txt_detail.setTypeface(regular);
        txt_summary.setTypeface(regular);
        btn_gotoweb.setTypeface(regular);
        txt_title.setTypeface(regular);

        txt_merchant.setText(merchant);
        txt_summary.setText(summary);
        txt_title.setText(title);
        txt_detail.setText(Html.fromHtml(String.valueOf(Html.fromHtml(detail)
        )));

        Glide.with(this).load(img).into(img_promo);

        aawer.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                float offsetAlpha = (appBarLayout.getY() / aawer.getTotalScrollRange());
                if (offsetAlpha == -1) {

                    img_promo.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.blacktras), PorterDuff.Mode.DARKEN);
                    img_share.setColorFilter(Color.argb(255, 255, 255, 255));
                    img_back.setColorFilter(Color.argb(255, 255, 255, 255));
                    txt_merchant.setTextColor(getResources().getColor(R.color.white));
                    toolbar.setBackgroundColor(getResources().getColor(R.color.transparent));
                } else if (offsetAlpha != -0.0) {
                    txt_merchant.setTextColor(getResources().getColor(R.color.white));
                    img_promo.clearColorFilter();
                } else {

                    txt_merchant.setTextColor(getResources().getColor(R.color.black));
                    img_promo.clearColorFilter();
                    img_back.clearColorFilter();
                    img_share.clearColorFilter();

                }
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_gotoweb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(urlpromo));
                startActivity(intent);
            }
        });

        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
//                i.putExtra(Intent.EXTRA_SUBJECT, readResponse.getTitle());
                i.putExtra(Intent.EXTRA_TEXT, title + "\n" + urlpromo);
                startActivity(Intent.createChooser(i, "Share Via"));
            }
        });
    }

}
