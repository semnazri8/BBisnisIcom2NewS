package bisnis.com.official.Fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import bisnis.com.official.Adapter.PromoAdapter;
import bisnis.com.official.Model.ModelPromo;
import bisnis.com.official.Network.ConnectionDetector;
import bisnis.com.official.Pojo.PromoResponse;
import bisnis.com.official.Presenter.PromoPresenter;
import bisnis.com.official.Presenter.PromoPresenterImp;
import bisnis.com.official.R;
import bisnis.com.official.Utilities.SpacesItemDecoration;
import bisnis.com.official.View.PromoView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 12/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class FragmentPromo extends Fragment implements PromoView {
    private PromoAdapter adapter;
    private GridLayoutManager glm;
    private View view;
    private RecyclerView recyclerView;
    private PromoPresenter promoPresenter;
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private MaterialDialog mDialog, dialog_muter, dialog_izin;
    private ImageView img_src, img_logo, img_back, img_close_src;
    private TextView txt_title;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        promoPresenter = new PromoPresenterImp(this);
        cd = new ConnectionDetector(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_chanel, container, false);
        recyclerView = view.findViewById(R.id.rv_chanel);
        txt_title = view.findViewById(R.id.txt_title);
        img_back = view.findViewById(R.id.img_back);
        txt_title.setText("Extras");


        checkconnections();
//        modelDataList = getData();
        glm = new GridLayoutManager(getActivity(), 2);
        glm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case ModelPromo.PromoGrid:
                        return 1;
                    case ModelPromo.PromoList:
                        return 2;
                    default:
                        return 1;
                }
            }
        });

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        return view;
    }

    private void checkconnections() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            getDialog_progress();
            promoPresenter.getPromo("20");

        } else if (isInternetPresent.equals(false)) {
            getdialogerror("Tidak ada koneksi Internet");
        }
    }


    @Override
    public void ResultPromo(String response_message, ArrayList<PromoResponse> promoResponses) {
        dialog_muter.dismiss();

        adapter = new PromoAdapter(promoResponses, getActivity());
        recyclerView.setLayoutManager(glm);
        recyclerView.setAdapter(adapter);


    }

    @Override
    public void PromoError(String response_message) {
        getdialogerror(response_message);
    }

    public void getDialog_progress() {

        dialog_muter = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .cancelable(false)
                .content(R.string.please_wait)
                .progress(true, 0)
                .show();
    }

    private void getdialogerror(String response_message) {
//        dialog_muter.dismiss();
        mDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .cancelable(false)
                .content(response_message)
                .positiveText("Close")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mDialog.dismiss();
                        getFragmentManager().popBackStack();
                    }
                })
                .show();
    }

}
