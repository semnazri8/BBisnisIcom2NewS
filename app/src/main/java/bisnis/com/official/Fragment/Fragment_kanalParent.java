package bisnis.com.official.Fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bisnis.com.official.Adapter.ListBeritaParentAdapter;
import bisnis.com.official.R;
import bisnis.com.official.Utilities.AnalyticsApplication;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 07/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class Fragment_kanalParent extends Fragment {

    private View view;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ImageView img_src, img_logo, img_back, img_close_src;
    private EditText edt_search;
    private LinearLayout ll_search, ll_kanal_nav;
    private String kanal_id;
    private String[] tab_name, tab_id;
    private AnalyticsApplication application;
    private Tracker mTracker;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kanal, container, false);

        kanal_id = getArguments().getString("kanalid");
        tabLayout = view.findViewById(R.id.tab_halaman);
        viewPager = view.findViewById(R.id.pager_halaman);
        img_src = view.findViewById(R.id.img_search);
        img_logo = view.findViewById(R.id.img_kanal);
        img_back = view.findViewById(R.id.img_back);
        img_close_src = view.findViewById(R.id.img_close_search);
        ll_search = view.findViewById(R.id.ll_search);
        ll_kanal_nav = view.findViewById(R.id.ll_kanal_nav);

        application = (AnalyticsApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();


        getparentCategory(kanal_id);
        maketab(kanal_id);
        img_src.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_kanal_nav.setVisibility(View.GONE);
                ll_search.setVisibility(View.VISIBLE);
            }
        });

        img_close_src.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_kanal_nav.setVisibility(View.VISIBLE);
                ll_search.setVisibility(View.GONE);
            }
        });
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });


        return view;
    }

    private void getparentCategory(String parent_id) {

        if (parent_id.equals("186")) {
            Glide.with(getActivity()).load(R.drawable.ic_kabar24).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.kabar24));
            tab_name = getResources().getStringArray(R.array.kabar24);
            tab_id = getResources().getStringArray(R.array.kabar24_id);
            mTracker.setScreenName("Android/KABAR24");
        } else if (parent_id.equals("194")) {
            Glide.with(getActivity()).load(R.drawable.ic_marked).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.market));
            tab_name = getResources().getStringArray(R.array.market);
            tab_id = getResources().getStringArray(R.array.market_id);
            mTracker.setScreenName("Android/MARKET");
        } else if (parent_id.equals("5")) {
            Glide.with(getActivity()).load(R.drawable.ic_finance).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.finance));
            tab_name = getResources().getStringArray(R.array.finansial);
            tab_id = getResources().getStringArray(R.array.finansial_id);
            mTracker.setScreenName("Android/FINANSIAL");
        } else if (parent_id.equals("43")) {
            Glide.with(getActivity()).load(R.drawable.ic_industri).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.industri));
            tab_name = getResources().getStringArray(R.array.industri);
            tab_id = getResources().getStringArray(R.array.industri_id);
            mTracker.setScreenName("Android/INDUSTRI");
        } else if (parent_id.equals("272")) {
            Glide.with(getActivity()).load(R.drawable.ic_otomotif).into(img_logo);
            tab_name = getResources().getStringArray(R.array.otomotif);
            tab_id = getResources().getStringArray(R.array.otomotif_id);
            mTracker.setScreenName("Android/OTOMOTIF");
        } else if (parent_id.equals("392")) {
            Glide.with(getActivity()).load(R.drawable.ic_bola).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.bola));
            tab_name = getResources().getStringArray(R.array.bola);
            tab_id = getResources().getStringArray(R.array.bola_id);
            mTracker.setScreenName("Android/BOLA");
        } else if (parent_id.equals("197")) {
            Glide.with(getActivity()).load(R.drawable.ic_lifestyle).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.lifestyle));
            tab_name = getResources().getStringArray(R.array.lifestyle);
            tab_id = getResources().getStringArray(R.array.lifestyle_id);
            mTracker.setScreenName("Android/LIFESTYLE");
        } else if (parent_id.equals("277")) {
            Glide.with(getActivity()).load(R.drawable.ic_gadget).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.gadget));
            tab_name = getResources().getStringArray(R.array.gadget);
            tab_id = getResources().getStringArray(R.array.gadget_id);
            mTracker.setScreenName("Android/GADGET");
        } else if (parent_id.equals("222")) {
            Glide.with(getActivity()).load(R.drawable.ic_treveling).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.traveling));
            tab_name = getResources().getStringArray(R.array.traveling);
            tab_id = getResources().getStringArray(R.array.traveling_id);
            mTracker.setScreenName("Android/TREAVELING");
        } else if (parent_id.equals("47")) {
            Glide.with(getActivity()).load(R.drawable.ic_properti).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.properti));
            tab_name = getResources().getStringArray(R.array.properti);
            tab_id = getResources().getStringArray(R.array.properti_id);
            mTracker.setScreenName("Android/PROPERTI");
        } else if (parent_id.equals("57")) {
            Glide.with(getActivity()).load(R.drawable.ic_sport).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.sport));
            tab_name = getResources().getStringArray(R.array.sport);
            tab_id = getResources().getStringArray(R.array.sport_id);
            mTracker.setScreenName("Android/SPORT");
        } else if (parent_id.equals("242") || parent_id.equals("244")) {
            Glide.with(getActivity()).load(R.drawable.ic_koran).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.black));
            tab_name = getResources().getStringArray(R.array.kroan_bisnis);
            tab_id = getResources().getStringArray(R.array.kroan_bisnis_id);
            mTracker.setScreenName("Android/KORANBISNIS");
        } else if (parent_id.equals("231")) {
            Glide.with(getActivity()).load(R.drawable.ic_syariah).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.syariah));
            tab_name = getResources().getStringArray(R.array.syariah);
            tab_id = getResources().getStringArray(R.array.syariah_id);
            mTracker.setScreenName("Android/SYARIAH");
        } else if (parent_id.equals("52")) {
            Glide.with(getActivity()).load(R.drawable.ic_manajemen).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.manajemen));
            tab_name = getResources().getStringArray(R.array.manajemen);
            tab_id = getResources().getStringArray(R.array.manajemen_id);
            mTracker.setScreenName("Android/MANAJEMEN");
        } else if (parent_id.equals("258")) {
            Glide.with(getActivity()).load(R.drawable.ic_enterpreneur).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.enterpreneur));
            tab_name = getResources().getStringArray(R.array.enterpreneur);
            tab_id = getResources().getStringArray(R.array.enterpreneur_id);
            mTracker.setScreenName("Android/ENTERPENEUR");
        } else if (parent_id.equals("243")) {
            Glide.with(getActivity()).load(R.drawable.ic_info).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.info));
            tab_name = getResources().getStringArray(R.array.info);
            tab_id = getResources().getStringArray(R.array.info_id);
            mTracker.setScreenName("Android/INFO");
        } else if (parent_id.equals("73")) {
            Glide.with(getActivity()).load(R.drawable.ic_inforial).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.inforial));
            tab_name = getResources().getStringArray(R.array.inforial);
            tab_id = getResources().getStringArray(R.array.inforial_id);
            mTracker.setScreenName("Android/INFORIAL");
        } else if (parent_id.equals("382")) {
            Glide.with(getActivity()).load(R.drawable.ic_jakarta_raya).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.jakarta));
            tab_name = getResources().getStringArray(R.array.jakarta);
            tab_id = getResources().getStringArray(R.array.jakarta_id);
            mTracker.setScreenName("Android/JAKARTARAYA");
        } else if (parent_id.equals("526")) {
            Glide.with(getActivity()).load(R.drawable.jatim).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.jatim));
            tab_name = getResources().getStringArray(R.array.surabaya);
            tab_id = getResources().getStringArray(R.array.surabaya_id);
            mTracker.setScreenName("Android/SURABAYA");
        } else if (parent_id.equals("527")) {
            Glide.with(getActivity()).load(R.drawable.sumatra).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.sumatra));
            tab_name = getResources().getStringArray(R.array.sumatera);
            tab_id = getResources().getStringArray(R.array.sumatera_id);
            mTracker.setScreenName("Android/SUMATRA");
        } else if (parent_id.equals("528")) {
            Glide.with(getActivity()).load(R.drawable.jateng).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.semarang));
            tab_name = getResources().getStringArray(R.array.semarang);
            tab_id = getResources().getStringArray(R.array.semarang_id);
            mTracker.setScreenName("Android/SEMARANG");
        } else if (parent_id.equals("529")) {
            Glide.with(getActivity()).load(R.drawable.bali).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.bali));
            tab_name = getResources().getStringArray(R.array.bali);
            tab_id = getResources().getStringArray(R.array.bali_id);
            mTracker.setScreenName("Android/BALI");
        } else if (parent_id.equals("530")) {
            Glide.with(getActivity()).load(R.drawable.sulawesi).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.sulawesi));
            tab_name = getResources().getStringArray(R.array.sulawesi);
            tab_id = getResources().getStringArray(R.array.sulawesi_id);
            mTracker.setScreenName("Android/SULAWESI");
        } else if (parent_id.equals("413")) {
            Glide.with(getActivity()).load(R.drawable.papua).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.papua));
            tab_name = getResources().getStringArray(R.array.papua);
            tab_id = getResources().getStringArray(R.array.papua_id);
            mTracker.setScreenName("Android/PAPUA");
        } else if (parent_id.equals("406")) {
            Glide.with(getActivity()).load(R.drawable.kalimantan).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.kalimantan));
            tab_name = getResources().getStringArray(R.array.kalimantan);
            tab_id = getResources().getStringArray(R.array.kalimantan_id);
            mTracker.setScreenName("Android/KALIMANTAN");
        } else if (parent_id.equals("420")) {
            Glide.with(getActivity()).load(R.drawable.banten).into(img_logo);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.banten));
            tab_name = getResources().getStringArray(R.array.banten);
            tab_id = getResources().getStringArray(R.array.banten_id);
            mTracker.setScreenName("Android/BANTEN");
        }

        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void maketab(String kanal_id) {
        List<HashMap<String, Fragment>> halamannya = new ArrayList<HashMap<String, Fragment>>();
        for (int i = 0; i < tab_name.length; i++) {

            Fragment_Kanal fragment = new Fragment_Kanal();
            Bundle bundle = new Bundle();
            bundle.putString("kanal_id", kanal_id);
            bundle.putString("title", tab_name[i]);
            bundle.putString("title_id", tab_id[i]);
            fragment.setArguments(bundle);
            HashMap<String, Fragment> halaman = new HashMap<String, Fragment>();
            halaman.put(tab_name[i], fragment);
            halamannya.add(halaman);
        }

        ListBeritaParentAdapter adapternya = new ListBeritaParentAdapter(getChildFragmentManager());
        adapternya.addFragment(halamannya);
        viewPager.setAdapter(adapternya);
    }
}
