package bisnis.com.official.Fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import bisnis.com.official.Adapter.KotaAdapter;
import bisnis.com.official.Adapter.SpinnerKotaAdapter;
import bisnis.com.official.Listener.CallListener;
import bisnis.com.official.Model.ModelKota;
import bisnis.com.official.Model.ModelSpinnerKota;
import bisnis.com.official.Network.ConnectionDetector;
import bisnis.com.official.Pojo.HotelResponse;
import bisnis.com.official.Pojo.ProvinceResponse;
import bisnis.com.official.Presenter.HotelPresenter;
import bisnis.com.official.Presenter.HotelPresenterImp;
import bisnis.com.official.Presenter.ProvincePresenter;
import bisnis.com.official.Presenter.ProvincePresenterImp;
import bisnis.com.official.R;
import bisnis.com.official.View.HotelView;
import bisnis.com.official.View.ProvinceView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 14/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class Fragment_Hotel extends Fragment implements HotelView, ProvinceView, CallListener {

    private View view;
    private Spinner spinner_kota;
//    private List<HotelResponse> hotelResponsesss;
    private ArrayList<ModelKota> modelKotas;
    private SpinnerKotaAdapter spinnerAdapter;
    private KotaAdapter kotaAdapter;
    private RecyclerView recyclerView;
    private LinearLayoutManager lm;
    private HotelPresenter hotelPresenter;
    private ProvincePresenter provincePresenter;
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private MaterialDialog mDialog, dialog_muter,dialog_izin;
    private TextView txt_not;
    private EditText edt_search;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private String phone_nmbr;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hotelPresenter = new HotelPresenterImp(this);
        provincePresenter = new ProvincePresenterImp(this);

        cd = new ConnectionDetector(getActivity());
    }

    public static void hideSoftKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((AppCompatActivity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(getActivity());
                    return false;
                }
            });
        }
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_hotel, container, false);


//        modelKotas = getData();
        spinner_kota = view.findViewById(R.id.spinner_kota);
        recyclerView = view.findViewById(R.id.rv_directory);
        txt_not = view.findViewById(R.id.soon);
        edt_search = view.findViewById(R.id.edt_search);

        setupUI(recyclerView);

        lm = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

//        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacingMedium);
//        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        checkconnections();

        return view;
    }

    private void checkconnections() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            getDialog_progress();
            hotelPresenter.getHotel("0");
            provincePresenter.getProvince();

        } else if (isInternetPresent.equals(false)) {
            getdialogerror("Tidak ada koneksi Internet");
        }
    }

    private List<ModelSpinnerKota> getKota() {

        ArrayList<ModelSpinnerKota> kota = new ArrayList<>();

        kota.add(new ModelSpinnerKota("Pilih Kota", 0));
        kota.add(new ModelSpinnerKota("Jakarta", 1));
        kota.add(new ModelSpinnerKota("Bali", 2));
        kota.add(new ModelSpinnerKota("Bekasi", 3));
        kota.add(new ModelSpinnerKota("Bogor", 4));
        kota.add(new ModelSpinnerKota("Yogyakarta", 5));
        kota.add(new ModelSpinnerKota("Surabaya", 6));
        kota.add(new ModelSpinnerKota("Medan", 7));

        return kota;
    }

    @Override
    public void ResultHotel(String response_message, final ArrayList<HotelResponse> hotelResponses) {
        dialog_muter.dismiss();

        Log.d("digidaw", String.valueOf(hotelResponses));

        if (hotelResponses.size() != 0){
            recyclerView.setVisibility(View.VISIBLE);
            txt_not.setVisibility(View.GONE);

            for (int i = 0; i < hotelResponses.size(); i++) {
                if (!hotelResponses.get(i).getImageThumbnailUrl().equals("")) {
                    hotelResponses.get(i).setType(1);
                } else {
                    hotelResponses.get(i).setType(2);
                }
            }
        }else{
            recyclerView.setVisibility(View.GONE);
            txt_not.setVisibility(View.VISIBLE);
        }



        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

//                if (edt_search.getText().toString().length() >= 3){
                    fiter(charSequence.toString(), hotelResponses);
//                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        kotaAdapter = new KotaAdapter(hotelResponses, getActivity(),this);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(kotaAdapter);

        if (!edt_search.getText().toString().equals("")){
            fiter(edt_search.getText().toString(), hotelResponses);
        }
    }

    @Override
    public void HotelError(String response_message) {
        getdialogerror(response_message);

    }

    public void getDialog_progress() {

        dialog_muter = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .cancelable(false)
                .content(R.string.please_wait)
                .progress(true, 0)
                .show();
    }

    private void getdialogerror(String response_message) {
//        dialog_muter.dismiss();
        mDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .content(response_message)
                .cancelable(false)
                .positiveText("Close")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mDialog.dismiss();
                        getFragmentManager().popBackStack();
                    }
                })
                .show();
    }

    @Override
    public void ResultProvince(String response_message, ArrayList<ProvinceResponse> provinceResponses) {
        spinnerAdapter = new SpinnerKotaAdapter(getActivity(), provinceResponses);
        spinner_kota.setAdapter(spinnerAdapter);

        spinner_kota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String item_id = String.valueOf(((TextView) view.findViewById(R.id.id_item)).getText().toString());

                hotelPresenter.getHotel(item_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void ProvinceError(String response_message) {
        getdialogerror(response_message);
    }

    private void fiter(String text, List<HotelResponse> hotelResponseList) {

        List<HotelResponse> temp = new ArrayList<>();
        for (HotelResponse d : hotelResponseList) {
            if (d.getName().toLowerCase().contains(text.toLowerCase())) {
                temp.add(d);
            }
        }
        kotaAdapter.updatelist(temp);

        if (temp.size() == 0){
            recyclerView.setVisibility(View.GONE);
            txt_not.setVisibility(View.VISIBLE);
        }else{
            recyclerView.setVisibility(View.VISIBLE);
            txt_not.setVisibility(View.GONE);
        }
    }

    @Override
    public void call(String phone_number) {

        phone_nmbr = phone_number;
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" +phone_number));
            startActivity(callIntent);
        } else {
            checkLocationPermission(phone_number);
        }
    }

    private void checkLocationPermission(String phone_number) {

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CALL_PHONE)) {

                getDialog("Perbolehkan Bisnis.com membantu anda menelpon nomer ini " + phone_number
                );
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CALL_PHONE},
                        PERMISSION_REQUEST_CODE);
            }
        }

    }

    private void getDialog(String s) {
        dialog_izin = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .content(s)
                .positiveText("Allow")
                .negativeText("Denny")
                .cancelable(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog_izin.dismiss();
                        requestPermission();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog_izin.dismiss();
                    }
                })
                .show();
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0: {
                // If request is cancelled, the result arrays are empty.
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                switch (requestCode) {
                    case 0:
                        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:" +phone_nmbr));
                            startActivity(callIntent);
                        } else {
                            requestPermission();
                        }

                }
                return;
            }

        }
    }




}
