package bisnis.com.official.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import bisnis.com.official.R;
import bisnis.com.official.Utilities.BisnisComOfficialPreferences;

public class FragmentProgileLogin extends Fragment {

    public static final String PREFS_PRIVATE = "PREFS_PRIVATE";
    View view;
    private String userid, fullname, username, email, photo_url, phone, is_active, regis_date, user_type, user_type_name;
    private ImageView img_profile;
    private Switch switch_notif;
    private TextView txt_pengaturan_lainnya, txt_bisnis_id, txt_email, txt_phone, txt_logout;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private SharedPreferences prefsprivate;
    private FragmentManager fragmentManager;
    private FragmentTransaction ft;
    private Boolean is_login;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile_login, container, false);


        img_profile = view.findViewById(R.id.img_prof);
        switch_notif = view.findViewById(R.id.seitch_notif);
        txt_pengaturan_lainnya = view.findViewById(R.id.pengaturan_lainnya);
        txt_bisnis_id = view.findViewById(R.id.txt_bisnis_id);
        txt_email = view.findViewById(R.id.txt_email);
        txt_phone = view.findViewById(R.id.txt_phone);
        txt_logout = view.findViewById(R.id.txt_logout);
        collapsingToolbarLayout = view.findViewById(R.id.collapse_toolbar);

        prefsprivate = getActivity().getSharedPreferences(PREFS_PRIVATE, Context.MODE_PRIVATE);
        userid = prefsprivate.getString(BisnisComOfficialPreferences.user_id, "null");
        fullname = prefsprivate.getString(BisnisComOfficialPreferences.FullName, "null");
        username = prefsprivate.getString(BisnisComOfficialPreferences.Username, "null");
        email = prefsprivate.getString(BisnisComOfficialPreferences.Email, "null");
//        phone = prefsprivate.getString(BisnisComOfficialPreferences., "null");
        photo_url = prefsprivate.getString(BisnisComOfficialPreferences.photo_url, "null");
        is_active = prefsprivate.getString(BisnisComOfficialPreferences.Active, "null");
        regis_date = prefsprivate.getString(BisnisComOfficialPreferences.Registerdate, "null");
        user_type = prefsprivate.getString(BisnisComOfficialPreferences.user_type, "null");
        user_type_name = prefsprivate.getString(BisnisComOfficialPreferences.user_type_name, "null");


        txt_pengaturan_lainnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });
        txt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Fragment_Profile fragment = new Fragment_Profile();
                fragmentManager = getActivity().getSupportFragmentManager();
                ft = fragmentManager.beginTransaction();
                ft.replace(R.id.container_body, fragment, "home").addToBackStack("menu");
                ft.commit();
                prefsprivate = getActivity().getSharedPreferences(PREFS_PRIVATE, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefsprivate.edit();
                editor.remove("Username");
                editor.commit();

            }
        });

        txt_bisnis_id.setText(username);
        txt_email.setText(email);
        txt_phone.setText("null");

        collapsingToolbarLayout.setTitle(fullname);
        collapsingToolbarLayout.setCollapsedTitleTextColor(getResources().getColor(R.color.white));
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(R.color.white));
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };

        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
        }

        Glide.with(getActivity()).load(photo_url).into(img_profile);


        return view;
    }
}
