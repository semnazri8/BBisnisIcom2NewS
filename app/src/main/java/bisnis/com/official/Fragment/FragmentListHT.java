package bisnis.com.official.Fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import bisnis.com.official.ActivityDeatilBerita;
import bisnis.com.official.Adapter.MultiViewAdapter;
import bisnis.com.official.Listener.OnClickAdapterListener;
import bisnis.com.official.Model.ModelData;
import bisnis.com.official.Network.ConnectionDetector;
import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.Presenter.HTPresenter;
import bisnis.com.official.Presenter.HTPresenterImp;
import bisnis.com.official.R;
import bisnis.com.official.Utilities.SpacesItemDecoration;
import bisnis.com.official.View.HTView;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 09/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class FragmentListHT extends Fragment implements HTView, OnClickAdapterListener {
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private ArrayList<BreakingResponse> braking;
    private MultiViewAdapter adapter;
    private GridLayoutManager glm;
    private View view;
    private RecyclerView recyclerView;
    private FloatingActionButton fab;
    private HTPresenter htPresenter;
    //    private LoadMorePresenter loadMorePresenter;
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;
    private MaterialDialog mDialog, dialog_muter;
    private SwipeRefreshLayout srl;
    private String type_get, topics_id, topics_hastag;
    private boolean loading = false;
    private int page = 1;
    private int page2 = 2;
    private int kurangin = 1;
    private TextView tv_hashtag;
    private ImageView img_back;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        cd = new ConnectionDetector(getActivity());
        htPresenter = new HTPresenterImp(this);
//        loadMorePresenter = new LoadMorePresenterImp(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ht, container, false);
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "Font/roboto_bold.ttf");
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "Font/roboto_regular.ttf");

        fab = view.findViewById(R.id.fab);
        srl = view.findViewById(R.id.srl_list_dash);
        recyclerView = view.findViewById(R.id.rv_berita);
        tv_hashtag = view.findViewById(R.id.txt_title);
        img_back = view.findViewById(R.id.img_back);

        type_get = getArguments().getString("title");
        topics_id = getArguments().getString("topic_id");
        topics_hastag = getArguments().getString("topic_hesteg");

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        tv_hashtag.setTypeface(regular);

        tv_hashtag.setText(topics_hastag);
        checkConnections();
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fab.getVisibility() == View.GONE) {
                    fab.show();
                } else if (dy < 0 && fab.getVisibility() == View.VISIBLE) {
                    fab.hide();
                }
            }
        });

        srl.setColorSchemeColors(getResources().getIntArray(R.array.intarr_swipe_refresh_layout));
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                checkConnections();
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);
            }
        });
        glm = new GridLayoutManager(getActivity(), 2);
        glm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case ModelData.News_grid:
                        return 1;
                    case ModelData.News_Thumbnail:
                        return 2;
                    case ModelData.News_list:
                        return 2;
                    case ModelData.News_ads:
                        return 2;
                    default:
                        return 1;
                }
            }
        });
        recyclerView.setLayoutManager(glm);


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    int lastIndex = glm.findLastCompletelyVisibleItemPosition() + 1;
                    int totalItem = adapter.getItemCount();

                    visibleItemCount = glm.getChildCount();
                    totalItemCount = adapter.getItemCount();
                    pastVisiblesItems = glm.findFirstVisibleItemPosition() + 1;

                    if (!loading && lastIndex == totalItem) {
                        loading = true;
                        srl.setRefreshing(true);
                        htPresenter.getListHT(topics_id, String.valueOf(page2++), "20");
//                        Log.v("...", "Last Item Wow !");
//                        //Do pagination.. i.e. fetch new data
//                        if (type_get.equals("Terbaru")) {
//
//                        } else if (type_get.equals("Headline")) {
//                            srl.setRefreshing(false);
//                        } else if (type_get.equals("Hot Topic")) {
//                            srl.setRefreshing(false);
//                        } else {
//                            srl.setRefreshing(false);
//                        }
                    }
                }


            }
        });
        return view;
    }

    private void checkConnections() {
        getDialog_progress();
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            //TODO : nanti ini untuk set breaking, headline, dll

            adapter = new MultiViewAdapter(getActivity(), this);
            recyclerView.setAdapter(adapter);
            loading = true;

            htPresenter.getListHT(topics_id, String.valueOf(page), "20");


        } else if (isInternetPresent.equals(false)) {
            getdialogerror("Tidak ada koneksi Internet");
            srl.setRefreshing(false);
        }
    }

    public void getDialog_progress() {

        dialog_muter = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .content(R.string.please_wait)
                .progress(true, 0)
                .show();
    }

    private void getdialogerror(String response_message) {
        dialog_muter.dismiss();
        mDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.app_name)
                .content(response_message)
                .positiveText("Close")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mDialog.dismiss();

                    }
                })
                .show();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        htPresenter.onDestroy();
//        loadMorePresenter.onDestroy();
    }


    public void ParseType1(ArrayList<BreakingResponse> breakingResponse) {
        onLoadNewComplete();
        braking = breakingResponse;
        for (int i = 0; i < breakingResponse.size(); i++) {
            if (breakingResponse.get(i).getPostDate() != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    Date mDate = sdf.parse(breakingResponse.get(i).getPostDate());

                    DateFormat format = new SimpleDateFormat("dd MMM yyyy, HH:mm");
                    String newformat = format.format(mDate);

                    breakingResponse.get(i).setFull_short_month(newformat + " wib");


                    long timeInMilliseconds = mDate.getTime();
                    breakingResponse.get(i).setMilis(timeInMilliseconds);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        for (int i = 0; i < breakingResponse.size(); i++) {
            breakingResponse.get(i).setType_get("hottopics");
            breakingResponse.get(i).setKanal_id("0");
            breakingResponse.get(i).setType(1);

        }

        adapter.addOn(breakingResponse);
    }

    public void ParseType2(ArrayList<BreakingResponse> breakingResponse) {
        onLoadNewComplete();
        braking = breakingResponse;
        for (int i = 0; i < breakingResponse.size(); i++) {
            if (breakingResponse.get(i).getPostDate() != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    Date mDate = sdf.parse(breakingResponse.get(i).getPostDate());

                    DateFormat format = new SimpleDateFormat("dd MMM yyyy, HH:mm");
                    String newformat = format.format(mDate);

                    breakingResponse.get(i).setFull_short_month(newformat + " wib");


                    long timeInMilliseconds = mDate.getTime();
                    breakingResponse.get(i).setMilis(timeInMilliseconds);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        for (int i = 0; i < breakingResponse.size(); i++) {

            breakingResponse.get(i).setType_get("hottopics");
            breakingResponse.get(i).setKanal_id("0");
            breakingResponse.get(i).setType(1);

        }
        adapter.addOn(breakingResponse);
    }

    private void onLoadNewComplete() {
        srl.setRefreshing(false);
    }

//    @Override
//    public void ResultLoadMore(String response_message, ArrayList<BreakingResponse> breakingResponse) {

//
//        loading = false;
//        if (breakingResponse.size() > 0)
//            ParseType2(breakingResponse);
//    }

    @Override
    public void ResultListHT(String response_message, ArrayList<BreakingResponse> breakingResponse) {
        dialog_muter.dismiss();
        loading = false;
        if (page2 >= 2) {
            onLoadNewComplete();
            ParseType2(breakingResponse);

        } else
        if (breakingResponse.size() != 0) {
            braking = breakingResponse;
            ParseType1(breakingResponse);
        }

    }

    @Override
    public void BreakingError(String response_message) {
        loading = false;
        getdialogerror(response_message);
    }

    @Override
    public void onClick(String position, String type_get, String kanal_id) {

        Intent i = new Intent(getActivity(), ActivityDeatilBerita.class);
        i.putExtra("index", position);
        i.putExtra("type_get", type_get);
        i.putExtra("kanal_id", kanal_id);
        i.putExtra("parcel", braking);
        i.putExtra("helo", "breaking");

//        i.putExtras(bundle);

        getActivity().startActivity(i);

    }

    @Override
    public void onClickAja(String position, String type_get, String kanal_id, ArrayList<BreakingResponse> dataSet) {
        Intent i = new Intent(getActivity(), ActivityDeatilBerita.class);
        i.putExtra("index", position);
        i.putExtra("type_get", type_get);
        i.putExtra("kanal_id", kanal_id);
        i.putExtra("parcel", dataSet);
        i.putExtra("helo", "breaking");

//        i.putExtras(bundle);

        getActivity().startActivity(i);
    }

    @Override
    public void onClickHeadline(String position, String type_get, String kanal_id) {

    }
}
