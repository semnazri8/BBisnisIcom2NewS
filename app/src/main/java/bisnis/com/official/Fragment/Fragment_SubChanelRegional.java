package bisnis.com.official.Fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import bisnis.com.official.Adapter.SubChanelAdapter;
import bisnis.com.official.Model.ModelSubChanel;
import bisnis.com.official.R;
import bisnis.com.official.Utilities.SpacesItemDecoration;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 02/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class Fragment_SubChanelRegional extends Fragment {
    private ArrayList<ModelSubChanel> modelDataList;
    private SubChanelAdapter adapter;
    private LinearLayoutManager lm;
    private GridLayoutManager glm;
    private View view;
    private RecyclerView recyclerView;
    private ImageView img_src, img_logo, img_back, img_close_src;
    private LinearLayout ll_search, ll_kanal_nav;
    private TextView txt_title;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sub_chanel, container, false);
        img_src = view.findViewById(R.id.img_search);
        img_back = view.findViewById(R.id.img_back);
        img_close_src = view.findViewById(R.id.img_close_search);
        ll_search = view.findViewById(R.id.ll_search);
        txt_title = view.findViewById(R.id.txt_title);
        ll_kanal_nav = view.findViewById(R.id.ll_kanal_nav);

        txt_title.setText("Regional");
        img_src.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_kanal_nav.setVisibility(View.GONE);
                ll_search.setVisibility(View.VISIBLE);
            }
        });

        img_close_src.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_kanal_nav.setVisibility(View.VISIBLE);
                ll_search.setVisibility(View.GONE);
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        modelDataList = getData();
        adapter = new SubChanelAdapter(modelDataList, getActivity());
        glm = new GridLayoutManager(getActivity(), 2);
        glm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case ModelSubChanel.ChanelGrid:
                        return 1;
                    case ModelSubChanel.ChanelList:
                        return 2;
                    default:
                        return 1;
                }
            }
        });
        recyclerView = view.findViewById(R.id.rv_chanel);
        recyclerView.setLayoutManager(glm);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        recyclerView.setAdapter(adapter);

        return view;
    }

    private ArrayList<ModelSubChanel> getData() {

        ArrayList<ModelSubChanel> chanels = new ArrayList<>();

        chanels.add(new ModelSubChanel(ModelSubChanel.ChanelGrid, "Jakarta", R.drawable.c23, 382));
        chanels.add(new ModelSubChanel(ModelSubChanel.ChanelGrid, "Surabaya", R.drawable.c24, 526));
        chanels.add(new ModelSubChanel(ModelSubChanel.ChanelGrid, "Sumatra", R.drawable.c25, 527));
        chanels.add(new ModelSubChanel(ModelSubChanel.ChanelGrid, "Semarang", R.drawable.c26, 528));
        chanels.add(new ModelSubChanel(ModelSubChanel.ChanelGrid, "Bali", R.drawable.c27, 529));
        chanels.add(new ModelSubChanel(ModelSubChanel.ChanelGrid, "Sulawesi", R.drawable.c28, 530));
        chanels.add(new ModelSubChanel(ModelSubChanel.ChanelGrid, "Papua", R.drawable.c29, 413));
        chanels.add(new ModelSubChanel(ModelSubChanel.ChanelGrid, "Kalimantan", R.drawable.c30, 406));
        chanels.add(new ModelSubChanel(ModelSubChanel.ChanelGrid, "Banten", R.drawable.c31, 420));


        return chanels;
    }

}
