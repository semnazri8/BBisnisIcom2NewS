package bisnis.com.official.Fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import bisnis.com.official.R;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 19/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class Fragment_ComingSoon extends Fragment {

    private View view;
    private TextView coming, soon;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_comingsoon,container,false);
        coming = view.findViewById(R.id.coming);
        soon = view.findViewById(R.id.soon);

        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "Font/roboto_bold.ttf");
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "Font/roboto_regular.ttf");

        coming.setTypeface(bold);
        soon.setTypeface(regular);


        return view;
    }
}
