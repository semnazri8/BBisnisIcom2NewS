package bisnis.com.official.Fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import bisnis.com.official.Adapter.ListBeritaParentAdapter;
import bisnis.com.official.R;
import bisnis.com.official.SeachActivity;
import br.com.mauker.materialsearchview.MaterialSearchView;

import static androidx.appcompat.app.AppCompatActivity.RESULT_OK;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 07/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class Fragment_Home extends Fragment {

    public static MaterialSearchView searchView;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private View view;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ImageView img_src, img_logo, img_back;
    private EditText edt_search;
    private LinearLayout ll_search, ll_clear;
    private CoordinatorLayout coordinatorLayout;


    public static void hideSoftKeyboard(AppCompatActivity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        AppCompatActivity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    Log.d("touch", "true");

                    hideSoftKeyboard(getActivity());
                    img_logo.setVisibility(View.VISIBLE);
                    ll_search.setVisibility(View.GONE);
                    edt_search.setText("");
                    return false;
                }
            });
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        tabLayout = view.findViewById(R.id.tab_halaman);
        viewPager = view.findViewById(R.id.pager_halaman);
        img_src = view.findViewById(R.id.img_search);
        img_logo = view.findViewById(R.id.img_bisnis);
        img_back = view.findViewById(R.id.img_close_search);
        ll_search = view.findViewById(R.id.ll_search);
//        ll_clear = view.findViewById(R.id.ll_clear);
        edt_search = view.findViewById(R.id.edt_search_news);
        coordinatorLayout = view.findViewById(R.id.parent);


        searchView = view.findViewById(R.id.search_view);

        setupUI(coordinatorLayout);
        maketab();
//        Log.d("fcm_token", FirebaseInstanceId.getInstance().getToken());


        searchView.setMaxHistoryResults(5);
//        searchView.setBackgroundColor(getResources().getColor(R.color.white));
        searchView.setBackIcon(R.drawable.ic_back);
        searchView.setVoiceIcon(R.drawable.ic_keyboard_voice_black_24dp);
        searchView.setClearIcon(R.drawable.ic_clear_blue_24dp);


        img_src.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.openSearch();

//                Intent i = new Intent(getActivity(), SeachActivity.class);
//                getActivity().startActivity(i);
//                img_logo.setVisibility(View.GONE);
//                ll_search.setVisibility(View.VISIBLE);
            }
        });


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_logo.setVisibility(View.VISIBLE);
                ll_search.setVisibility(View.GONE);
                edt_search.setText("");
                hideSoftKeyboard(getActivity());
            }
        });
//        viewPager.setAdapter(new Adapter(getChildFragmentManager()));
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });
        wrapTabIndicatorToTitle(tabLayout, 0, 0);
        searchView.setOnVoiceClickedListener(new MaterialSearchView.OnVoiceClickedListener() {
            @Override
            public void onVoiceClicked() {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                        getResources().getString(R.string.speech_prompt));
                try {
                    startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
                } catch (ActivityNotFoundException a) {
                    Toast.makeText(getActivity(),
                            getResources().getString(R.string.speech_not_supported),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

//               Toast.makeText(getActivity(), query, Toast.LENGTH_SHORT).show();

                Intent i = new Intent(getActivity(), SeachActivity.class);
                i.putExtra("keyword", query);
                startActivity(i);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    searchView.setQuery(result.get(0), false);
                    Intent i = new Intent(getActivity(), SeachActivity.class);
                    i.putExtra("keyword", result.get(0));
                    startActivity(i);
                }
                break;
            }

        }

    }

    public void wrapTabIndicatorToTitle(TabLayout tabLayout, int externalMargin, int internalMargin) {
        View tabStrip = tabLayout.getChildAt(0);
        if (tabStrip instanceof ViewGroup) {
            ViewGroup tabStripGroup = (ViewGroup) tabStrip;
            int childCount = ((ViewGroup) tabStrip).getChildCount();
            for (int i = 0; i < childCount; i++) {
                View tabView = tabStripGroup.getChildAt(i);
                //set minimum width to 0 for instead for small texts, indicator is not wrapped as expected
                tabView.setMinimumWidth(0);
                // set padding to 0 for wrapping indicator as title
                tabView.setPadding(0, tabView.getPaddingTop(), 0, tabView.getPaddingBottom());
                // setting custom margin between tabs
                if (tabView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
                    ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) tabView.getLayoutParams();
                    if (i == 0) {
                        // left
                        settingMargin(layoutParams, externalMargin, internalMargin);
                    } else if (i == childCount - 1) {
                        // right
                        settingMargin(layoutParams, internalMargin, externalMargin);
                    } else {
                        // internal
                        settingMargin(layoutParams, internalMargin, internalMargin);
                    }
                }
            }

            tabLayout.requestLayout();
        }
    }

    private void settingMargin(ViewGroup.MarginLayoutParams layoutParams, int start, int end) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            layoutParams.setMarginStart(start);
            layoutParams.setMarginEnd(end);
        } else {
            layoutParams.leftMargin = start;
            layoutParams.rightMargin = end;
        }
    }


    @Override
    public void onResume() {
        super.onResume();

    }


    private void maketab() {

        String size[] = getResources().getStringArray(R.array.breaking);
        Log.d("haha", String.valueOf(size.length));

        List<HashMap<String, Fragment>> halamannya = new ArrayList<HashMap<String, Fragment>>();
        for (int i = 0; i < size.length; i++) {


            viewPager.setOffscreenPageLimit(1);

            Fragment_Breaking fragment = new Fragment_Breaking();
            Bundle bundle = new Bundle();
            bundle.putString("title", size[i]);
            fragment.setArguments(bundle);
            HashMap<String, Fragment> halaman = new HashMap<String, Fragment>();
            halaman.put(size[i], fragment);
            halamannya.add(halaman);

        }

        ListBeritaParentAdapter adapternya = new ListBeritaParentAdapter(getChildFragmentManager());
        adapternya.addFragment(halamannya);
        viewPager.setAdapter(adapternya);
    }

}
