package bisnis.com.official.Fragment;

import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bisnis.com.official.Adapter.ListBeritaParentAdapter;
import bisnis.com.official.MainActivity;
import bisnis.com.official.R;

public class FragmentDirectoryParent extends Fragment {

    private TextView txt_title;
    private ImageView img_src, img_logo, img_back, img_close_src;
    private View view;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_directory_parent, container, false);
        txt_title = view.findViewById(R.id.txt_title);
        img_back = view.findViewById(R.id.img_back);
        txt_title.setText("Directory");
        tabLayout = view.findViewById(R.id.tab_halaman);
        viewPager = view.findViewById(R.id.pager_halaman);

        maketab();

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
                MainActivity.btmView.getMenu().findItem(R.id.home).setChecked(true);
            }
        });


        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });
        wrapTabIndicatorToTitle(tabLayout, 0, 0);


        return view;
    }

    public void wrapTabIndicatorToTitle(TabLayout tabLayout, int externalMargin, int internalMargin) {
        View tabStrip = tabLayout.getChildAt(0);
        if (tabStrip instanceof ViewGroup) {
            ViewGroup tabStripGroup = (ViewGroup) tabStrip;
            int childCount = ((ViewGroup) tabStrip).getChildCount();
            for (int i = 0; i < childCount; i++) {
                View tabView = tabStripGroup.getChildAt(i);
                //set minimum width to 0 for instead for small texts, indicator is not wrapped as expected
                tabView.setMinimumWidth(0);
                // set padding to 0 for wrapping indicator as title
                tabView.setPadding(0, tabView.getPaddingTop(), 0, tabView.getPaddingBottom());
                // setting custom margin between tabs
                if (tabView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
                    ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) tabView.getLayoutParams();
                    if (i == 0) {
                        // left
                        settingMargin(layoutParams, externalMargin, internalMargin);
                    } else if (i == childCount - 1) {
                        // right
                        settingMargin(layoutParams, internalMargin, externalMargin);
                    } else {
                        // internal
                        settingMargin(layoutParams, internalMargin, internalMargin);
                    }
                }
            }

            tabLayout.requestLayout();
        }
    }

    private void settingMargin(ViewGroup.MarginLayoutParams layoutParams, int start, int end) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            layoutParams.setMarginStart(start);
            layoutParams.setMarginEnd(end);
        } else {
            layoutParams.leftMargin = start;
            layoutParams.rightMargin = end;
        }
    }

    private void maketab() {

        String size[] = getResources().getStringArray(R.array.directory);
        Log.d("haha", String.valueOf(size.length));

        List<HashMap<String, Fragment>> halamannya = new ArrayList<HashMap<String, Fragment>>();
        for (int i = 0; i < size.length; i++) {

            Fragment_Hotel fragment = new Fragment_Hotel();
            Bundle bundle = new Bundle();
            bundle.putString("title", size[i]);
            fragment.setArguments(bundle);
            HashMap<String, Fragment> halaman = new HashMap<String, Fragment>();
            halaman.put(size[i], fragment);
            halamannya.add(halaman);
        }

        ListBeritaParentAdapter adapternya = new ListBeritaParentAdapter(getChildFragmentManager());
        adapternya.addFragment(halamannya);
        viewPager.setAdapter(adapternya);
    }


}
