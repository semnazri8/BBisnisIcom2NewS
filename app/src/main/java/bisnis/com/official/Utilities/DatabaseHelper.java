package bisnis.com.official.Utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "bookmarks_db";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Bookmarks.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Bookmarks.TABLE_NAME);
        // Create tables again
        onCreate(db);
    }


    public long insertNote(String title, String url_img, String kanal_name, String time_news, String time_requests, String category_request, String post_id_request) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(Bookmarks.COLUMN_NEWS_TITLE, title);
        values.put(Bookmarks.COLUMN_NEWS_URL_IMG, url_img);
        values.put(Bookmarks.COLUMN_NEWS_KANAL_NAME, kanal_name);
        values.put(Bookmarks.COLUMN_NEWS_TIME, time_news);
        values.put(Bookmarks.COLUMN_NEWS_TIME_FORMATED_REQUEST, time_requests);
        values.put(Bookmarks.COLUMN_NEWS_CATEGORY_REQUEST, category_request);
        values.put(Bookmarks.COLUMN_NEWS_POST_ID_REQUEST, post_id_request);

        // insert row
        long id = db.insert(Bookmarks.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public List<Bookmarks> getAllBookmarks() {
        List<Bookmarks> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + Bookmarks.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Bookmarks note = new Bookmarks();
                note.setId(cursor.getInt(cursor.getColumnIndex(Bookmarks.COLUMN_ID)));
                note.setTitle(cursor.getString(cursor.getColumnIndex(Bookmarks.COLUMN_NEWS_TITLE)));
                note.setUrl_img(cursor.getString(cursor.getColumnIndex(Bookmarks.COLUMN_NEWS_URL_IMG)));
                note.setKanal_name(cursor.getString(cursor.getColumnIndex(Bookmarks.COLUMN_NEWS_KANAL_NAME)));
                note.setTitle(cursor.getString(cursor.getColumnIndex(Bookmarks.COLUMN_NEWS_TIME)));
                note.setTime_format_request(cursor.getString(cursor.getColumnIndex(Bookmarks.COLUMN_NEWS_TIME_FORMATED_REQUEST)));
                note.setCategory_request(cursor.getString(cursor.getColumnIndex(Bookmarks.COLUMN_NEWS_CATEGORY_REQUEST)));
                note.setPost_id_request(cursor.getString(cursor.getColumnIndex(Bookmarks.COLUMN_NEWS_POST_ID_REQUEST)));


                notes.add(note);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return notes;
    }


    public int getNotesCount() {
        String countQuery = "SELECT  * FROM " + Bookmarks.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

    public void deleteNote(Bookmarks bookmarks) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Bookmarks.TABLE_NAME, Bookmarks.COLUMN_ID + " = ?",
                new String[]{String.valueOf(bookmarks.getId())});
        db.close();
    }

    public void isStore(String post_id) {
        String findPostId = "SELECT  * FROM " + Bookmarks.TABLE_NAME + " WHERE " + Bookmarks.COLUMN_NEWS_POST_ID_REQUEST + " = " + post_id;
        SQLiteDatabase db = this.getReadableDatabase();

    }
}
