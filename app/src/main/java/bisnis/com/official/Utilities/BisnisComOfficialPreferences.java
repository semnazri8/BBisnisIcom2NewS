package bisnis.com.official.Utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import bisnis.com.official.Model.SearchModel;

public class BisnisComOfficialPreferences {

    public static final String user_id = "userid";
    public static final String FullName = "FullName";
    public static final String Username = "Username";
    public static final String Email = "Email";
    public static final String photo_url = "photo_url";
    public static final String Active = "Active";
    public static final String Registerdate = "Registerdate";
    public static final String user_type = "user_type";
    public static final String user_type_name = "user_type_name";
    public static final String is_notif = "is_notif";
    public static final Boolean is_login = false;

    public static final String PREFS_NAME = "NKDROID_APP";
    public static final String FAVORITES = "Favorite";

    public BisnisComOfficialPreferences() {
        super();
    }

    public void storeFavorites(Context context, List<SearchModel> favorites) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);

        editor.putString(FAVORITES, jsonFavorites);

        editor.commit();
    }

    public ArrayList<SearchModel> loadFavorites(Context context) {
        SharedPreferences settings;
        List<SearchModel> favorites;

        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        if (settings.contains(FAVORITES)) {
            String jsonFavorites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            SearchModel[] favoriteItems = gson.fromJson(jsonFavorites, SearchModel[].class);
            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<SearchModel>(favorites);
        } else
            return null;

        return (ArrayList<SearchModel>) favorites;
    }


    public void addFavorite(Context context, SearchModel beanSampleList) {
        List<SearchModel> favorites = loadFavorites(context);
        if (favorites == null)
            favorites = new ArrayList<SearchModel>();
        favorites.add(beanSampleList);
        storeFavorites(context, favorites);
    }

    public void removeFavorite(Context context, SearchModel beanSampleList) {
        ArrayList<SearchModel> favorites = loadFavorites(context);
        if (favorites != null) {
            favorites.remove(beanSampleList);
            storeFavorites(context, favorites);
        }
    }
}
