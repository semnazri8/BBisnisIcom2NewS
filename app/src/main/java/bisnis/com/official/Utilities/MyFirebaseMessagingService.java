package bisnis.com.official.Utilities;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import bisnis.com.official.R;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 12/5/17.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "This is Sucks";

    private NotificationsUtil notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG, "From: " + remoteMessage.getFrom());

        String click_actions = remoteMessage.getNotification().getClickAction();
        String Body = remoteMessage.getNotification().getBody();

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + Body);
            handleNotification(Body);
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData());
            String category_id = remoteMessage.getData().get("category_id");
            String periode = remoteMessage.getData().get("periode");
            String post_id = remoteMessage.getData().get("post_id");

            sendNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle(), category_id, periode, post_id, click_actions);

//            try {
//                JSONObject json = new JSONObject(remoteMessage.getData().toString());
//                handleDataMessage(json, click_actions);
//            } catch (Exception e) {
//                Log.e(TAG, "Exception: " + e.getMessage());
//            }
        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
//        if (remoteMessage.getData().size() > 0) {
//
//        }
    }

    private void sendNotification(String messageBody, String messageTitle, String category_id, String periode, String post_id, String click_action) {
        Intent intent = new Intent(click_action);
        intent.putExtra("category_id", category_id);
        intent.putExtra("periode", periode);
        intent.putExtra("post_id", post_id);

        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setSmallIcon(R.drawable.logo_b_benings)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }

    private void handleNotification(String message) {
        if (!NotificationsUtil.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationsUtil notificationUtils = new NotificationsUtil(getApplicationContext());
            notificationUtils.playNotificationSound();
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json, String click_actions) {
        Log.e(TAG, "push json: " + json.toString());

        try {
            JSONObject data = json.getJSONObject("data");
            String title = data.getString("title");
            String subtitle = data.getString("text");
            String post_date = data.getString("periode");
            String categoty_id = data.getString("categoty_id");
            String post_id = data.getString("post_id");

            Log.e(TAG, "post_date: " + post_date);
            Log.e(TAG, "categoty_id: " + categoty_id);
            Log.e(TAG, "periode: " + post_id);


            if (!NotificationsUtil.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", post_date);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
                NotificationsUtil notificationUtils = new NotificationsUtil(getApplicationContext());
                notificationUtils.playNotificationSound();


            } else {
                // app is in background, show the notification in notification tray
                Intent resultIntent = new Intent(click_actions);
                resultIntent.putExtra("message", post_date);

                // check for image attachment
//                if (TextUtils.isEmpty(imageUrl)) {
                showNotificationMessage(getApplicationContext(), title, subtitle, post_date, categoty_id, post_id, resultIntent);
//                } else {
                // image is present, show notification with image
//                    showNotificationMessageWithBigImage(getApplicationContext(), title, subtitle, post_date, categoty_id, post_id);
//                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String subtitle, String post_date, String categoty_id, String post_id, Intent intent) {
        notificationUtils = new NotificationsUtil(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, subtitle, post_date, categoty_id, post_id, intent);

//        Intent go = new Intent(context, MainActivity.class);
        Intent broadcast = new Intent("broadcaster");
        LocalBroadcastManager.getInstance(context).sendBroadcast(broadcast);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String subtitle, String post_date, Intent intent, String imageUrl) {
        notificationUtils = new NotificationsUtil(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
        Intent broadcast = new Intent("broadcaster");
        LocalBroadcastManager.getInstance(context).sendBroadcast(broadcast);
    }
}
