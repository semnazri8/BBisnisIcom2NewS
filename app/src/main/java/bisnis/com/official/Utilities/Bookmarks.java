package bisnis.com.official.Utilities;

public class Bookmarks {

    public static final String TABLE_NAME = "tbl_bookmark";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NEWS_TITLE = "title";
    public static final String COLUMN_NEWS_URL_IMG = "url_img";
    public static final String COLUMN_NEWS_KANAL_NAME = "kanal_name";
    public static final String COLUMN_NEWS_TIME = "time_news";
    public static final String COLUMN_NEWS_TIME_FORMATED_REQUEST = "time_request";
    public static final String COLUMN_NEWS_CATEGORY_REQUEST = "category_request";
    public static final String COLUMN_NEWS_POST_ID_REQUEST = "post_id_request";
    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_NEWS_TITLE + " TEXT," + COLUMN_NEWS_URL_IMG + " TEXT," + COLUMN_NEWS_KANAL_NAME + " TEXT,"
                    + COLUMN_NEWS_TIME + " TEXT," + COLUMN_NEWS_TIME_FORMATED_REQUEST + " TEXT," + COLUMN_NEWS_CATEGORY_REQUEST + " TEXT,"
                    + COLUMN_NEWS_POST_ID_REQUEST + " TEXT"
                    + ")";
    private int id;
    private String title, url_img, kanal_name, time_news, time_format_request, category_request, post_id_request;

    public Bookmarks() {
    }

    public Bookmarks(int id, String title, String url_img, String kanal_name, String time_news, String time_format_request, String category_request, String post_id_request) {
        this.id = id;
        this.title = title;
        this.url_img = url_img;
        this.kanal_name = kanal_name;
        this.time_news = time_news;
        this.time_format_request = time_format_request;
        this.category_request = category_request;
        this.post_id_request = post_id_request;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory_request() {
        return category_request;
    }

    public void setCategory_request(String category_request) {
        this.category_request = category_request;
    }

    public String getKanal_name() {
        return kanal_name;
    }

    public void setKanal_name(String kanal_name) {
        this.kanal_name = kanal_name;
    }

    public String getPost_id_request() {
        return post_id_request;
    }

    public void setPost_id_request(String post_id_request) {
        this.post_id_request = post_id_request;
    }

    public String getTime_format_request() {
        return time_format_request;
    }

    public void setTime_format_request(String time_format_request) {
        this.time_format_request = time_format_request;
    }

    public String getTime_news() {
        return time_news;
    }

    public void setTime_news(String time_news) {
        this.time_news = time_news;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl_img() {
        return url_img;
    }

    public void setUrl_img(String url_img) {
        this.url_img = url_img;
    }

}
