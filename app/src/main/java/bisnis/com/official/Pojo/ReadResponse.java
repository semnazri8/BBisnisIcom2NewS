package bisnis.com.official.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReadResponse {

    @SerializedName("date")
    @Expose
    private DateRead date;
    @SerializedName("site")
    @Expose
    private Site site;
    @SerializedName("category")
    @Expose
    private Category category;
    @SerializedName("parent_category")
    @Expose
    private ParentCategory parentCategory;
    @SerializedName("author_id")
    @Expose
    private String authorId;
    @SerializedName("author_name")
    @Expose
    private String authorName;
    @SerializedName("editor_id")
    @Expose
    private String editorId;
    @SerializedName("editor_name")
    @Expose
    private String editorName;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("tags")
    @Expose
    private List<Tag> tags = null;
    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("subtitle")
    @Expose
    private String subtitle;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("keywords")
    @Expose
    private String keywords;
    @SerializedName("is_live")
    @Expose
    private String isLive;
    @SerializedName("is_series")
    @Expose
    private String isSeries;
    @SerializedName("is_kolom")
    @Expose
    private String isKolom;
    @SerializedName("image_thumb")
    @Expose
    private String imageThumb;
    @SerializedName("image_content")
    @Expose
    private String imageContent;
    @SerializedName("image_caption")
    @Expose
    private String imageCaption;
    @SerializedName("image_type")
    @Expose
    private String imageType;
    @SerializedName("image_video")
    @Expose
    private String imageVideo;
    @SerializedName("title_1")
    @Expose
    private String title1;
    @SerializedName("title_2")
    @Expose
    private String title2;
    @SerializedName("title_3")
    @Expose
    private String title3;
    @SerializedName("url_1")
    @Expose
    private String url1;
    @SerializedName("url_2")
    @Expose
    private String url2;
    @SerializedName("url_3")
    @Expose
    private String url3;
    @SerializedName("image_thumbnail_url")
    @Expose
    private String imageThumbnailUrl;
    @SerializedName("image_content_url")
    @Expose
    private String imageContentUrl;
    @SerializedName("post_url")
    @Expose
    private String post_url;

    @SerializedName("terkait")
    @Expose
    private List<Terkait> terkait = null;

    @SerializedName("live")
    @Expose
    private List<Live> live = null;

    @SerializedName("serial")
    @Expose
    private List<Serial> serial = null;


    public void setSerial(List<Serial> serial) {
        this.serial = serial;
    }

    public List<Serial> getSerial() {
        return serial;
    }

    public List<Live> getLive() {
        return live;
    }

    public void setLive(List<Live> live) {
        this.live = live;
    }

    public String getPost_url() {
        return post_url;
    }

    public void setPost_url(String post_url) {
        this.post_url = post_url;
    }

    public DateRead getDate() {
        return date;
    }

    public void setDate(DateRead date) {
        this.date = date;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public ParentCategory getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(ParentCategory parentCategory) {
        this.parentCategory = parentCategory;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getEditorId() {
        return editorId;
    }

    public void setEditorId(String editorId) {
        this.editorId = editorId;
    }

    public String getEditorName() {
        return editorName;
    }

    public void setEditorName(String editorName) {
        this.editorName = editorName;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getIsLive() {
        return isLive;
    }

    public void setIsLive(String isLive) {
        this.isLive = isLive;
    }

    public String getIsSeries() {
        return isSeries;
    }

    public void setIsSeries(String isSeries) {
        this.isSeries = isSeries;
    }

    public String getIsKolom() {
        return isKolom;
    }

    public void setIsKolom(String isKolom) {
        this.isKolom = isKolom;
    }

    public String getImageThumb() {
        return imageThumb;
    }

    public void setImageThumb(String imageThumb) {
        this.imageThumb = imageThumb;
    }

    public String getImageContent() {
        return imageContent;
    }

    public void setImageContent(String imageContent) {
        this.imageContent = imageContent;
    }

    public String getImageCaption() {
        return imageCaption == null ? "" : imageCaption;
    }

    public void setImageCaption(String imageCaption) {
        this.imageCaption = imageCaption;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getImageVideo() {
        return imageVideo;
    }

    public void setImageVideo(String imageVideo) {
        this.imageVideo = imageVideo;
    }

    public String getTitle1() {
        return title1;
    }

    public void setTitle1(String title1) {
        this.title1 = title1;
    }

    public String getTitle2() {
        return title2;
    }

    public void setTitle2(String title2) {
        this.title2 = title2;
    }

    public String getTitle3() {
        return title3;
    }

    public void setTitle3(String title3) {
        this.title3 = title3;
    }

    public String getUrl1() {
        return url1;
    }

    public void setUrl1(String url1) {
        this.url1 = url1;
    }

    public String getUrl2() {
        return url2;
    }

    public void setUrl2(String url2) {
        this.url2 = url2;
    }

    public String getUrl3() {
        return url3;
    }

    public void setUrl3(String url3) {
        this.url3 = url3;
    }

    public String getImageThumbnailUrl() {
        return imageThumbnailUrl;
    }

    public void setImageThumbnailUrl(String imageThumbnailUrl) {
        this.imageThumbnailUrl = imageThumbnailUrl;
    }

    public String getImageContentUrl() {
        return imageContentUrl;
    }

    public void setImageContentUrl(String imageContentUrl) {
        this.imageContentUrl = imageContentUrl;
    }

    public List<Terkait> getTerkait() {
        return terkait;
    }

    public void setTerkait(List<Terkait> terkait) {
        this.terkait = terkait;
    }
}
