package bisnis.com.official.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Live {

    @SerializedName("plive_id")
    @Expose
    private String pliveId;
    @SerializedName("plive_title")
    @Expose
    private String pliveTitle;
    @SerializedName("plive_summary")
    @Expose
    private String pliveSummary;
    @SerializedName("plive_image")
    @Expose
    private Object pliveImage;
    @SerializedName("plive_image_thumb")
    @Expose
    private Object pliveImageThumb;
    @SerializedName("plive_date")
    @Expose
    private String pliveDate;
    @SerializedName("date")
    @Expose
    private DateHT1 date;
    @SerializedName("plive_author")
    @Expose
    private String pliveAuthor;
    @SerializedName("image_thumbnail_url")
    @Expose
    private String imageThumbnailUrl;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;

    public String getPliveId() {
        return pliveId;
    }

    public void setPliveId(String pliveId) {
        this.pliveId = pliveId;
    }

    public String getPliveTitle() {
        return pliveTitle;
    }

    public void setPliveTitle(String pliveTitle) {
        this.pliveTitle = pliveTitle;
    }

    public String getPliveSummary() {
        return pliveSummary;
    }

    public void setPliveSummary(String pliveSummary) {
        this.pliveSummary = pliveSummary;
    }

    public Object getPliveImage() {
        return pliveImage;
    }

    public void setPliveImage(Object pliveImage) {
        this.pliveImage = pliveImage;
    }

    public Object getPliveImageThumb() {
        return pliveImageThumb;
    }

    public void setPliveImageThumb(Object pliveImageThumb) {
        this.pliveImageThumb = pliveImageThumb;
    }

    public String getPliveDate() {
        return pliveDate;
    }

    public void setPliveDate(String pliveDate) {
        this.pliveDate = pliveDate;
    }

    public DateHT1 getDate() {
        return date;
    }

    public void setDate(DateHT1 date) {
        this.date = date;
    }

    public String getPliveAuthor() {
        return pliveAuthor;
    }

    public void setPliveAuthor(String pliveAuthor) {
        this.pliveAuthor = pliveAuthor;
    }

    public String getImageThumbnailUrl() {
        return imageThumbnailUrl;
    }

    public void setImageThumbnailUrl(String imageThumbnailUrl) {
        this.imageThumbnailUrl = imageThumbnailUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
