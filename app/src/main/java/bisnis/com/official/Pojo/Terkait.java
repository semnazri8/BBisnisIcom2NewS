package bisnis.com.official.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Terkait {

    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("category_link")
    @Expose
    private String categoryLink;
    @SerializedName("post_date")
    @Expose
    private String postDate;
    @SerializedName("date")
    @Expose
    private DateHT0 date;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("subtitle")
    @Expose
    private String subtitle;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("is_live")
    @Expose
    private String isLive;
    @SerializedName("image_thumbnail")
    @Expose
    private String imageThumbnail;
    @SerializedName("image_thumbnail_url")
    @Expose
    private String imageThumbnailUrl;

    private String Full_short_month;
    private Long milis_postdate;
    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryLink() {
        return categoryLink;
    }

    public void setCategoryLink(String categoryLink) {
        this.categoryLink = categoryLink;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public DateHT0 getDate() {
        return date;
    }

    public void setDate(DateHT0 date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getIsLive() {
        return isLive;
    }

    public void setIsLive(String isLive) {
        this.isLive = isLive;
    }

    public String getImageThumbnail() {
        return imageThumbnail;
    }

    public void setImageThumbnail(String imageThumbnail) {
        this.imageThumbnail = imageThumbnail;
    }

    public String getImageThumbnailUrl() {
        return imageThumbnailUrl;
    }

    public void setImageThumbnailUrl(String imageThumbnailUrl) {
        this.imageThumbnailUrl = imageThumbnailUrl;
    }

    public void setMilis_postdate(Long milis_postdate) {
        this.milis_postdate = milis_postdate;
    }

    public Long getMilis_postdate() {
        return milis_postdate;
    }

    public void setFull_short_month(String full_short_month) {
        Full_short_month = full_short_month;
    }

    public String getFull_short_month() {
        return Full_short_month;
    }
}
