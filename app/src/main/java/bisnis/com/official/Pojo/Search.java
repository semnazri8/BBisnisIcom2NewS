package bisnis.com.official.Pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Search implements Parcelable {

    public static final Creator<Search> CREATOR = new Creator<Search>() {
        @Override
        public Search createFromParcel(Parcel in) {
            return new Search(in);
        }

        @Override
        public Search[] newArray(int size) {
            return new Search[size];
        }
    };
    @SerializedName("id")
    @Expose
    private String postId;
    @SerializedName("post_date")
    @Expose
    private String postDate;
    @SerializedName("date")
    @Expose
    private DateHT0 date;
    @SerializedName("title")
    @Expose
    private List<String> title = null;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("image_thumbnail_url")
    @Expose
    private List<String> imageThumbnailUrl = null;

    public Search() {

    }

    protected Search(Parcel in) {
        postId = in.readString();
        postDate = in.readString();
        title = in.createStringArrayList();
        summary = in.readString();
        categoryId = in.readString();
        imageThumbnailUrl = in.createStringArrayList();
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public DateHT0 getDate() {
        return date;
    }

    public void setDate(DateHT0 date) {
        this.date = date;
    }

    public List<String> getTitle() {
        return title;
    }

    public void setTitle(List<String> title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public List<String> getImageThumbnailUrl() {
        return imageThumbnailUrl;
    }

    public void setImageThumbnailUrl(List<String> imageThumbnailUrl) {
        this.imageThumbnailUrl = imageThumbnailUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(postId);
        dest.writeString(postDate);
        dest.writeStringList(title);
        dest.writeString(summary);
        dest.writeString(categoryId);
        dest.writeStringList(imageThumbnailUrl);
    }
}
