package bisnis.com.official.Pojo;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailBeritaResponse {

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("data_terkait")
    @Expose
    private List<DataTerkait> dataTerkait = null;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public List<DataTerkait> getDataTerkait() {
        return dataTerkait;
    }

    public void setDataTerkait(List<DataTerkait> dataTerkait) {
        this.dataTerkait = dataTerkait;
    }

}
