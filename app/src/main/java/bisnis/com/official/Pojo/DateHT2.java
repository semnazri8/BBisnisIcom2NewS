
package bisnis.com.official.Pojo;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DateHT2 {

    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("day")
    @Expose
    private String day;
    @SerializedName("hour")
    @Expose
    private String hour;
    @SerializedName("minute")
    @Expose
    private String minute;
    @SerializedName("second")
    @Expose
    private String second;
    @SerializedName("day_of_week")
    @Expose
    private String dayOfWeek;
    @SerializedName("month_ind_name")
    @Expose
    private String monthIndName;
    @SerializedName("day_ind_name")
    @Expose
    private String dayIndName;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getMonthIndName() {
        return monthIndName;
    }

    public void setMonthIndName(String monthIndName) {
        this.monthIndName = monthIndName;
    }

    public String getDayIndName() {
        return dayIndName;
    }

    public void setDayIndName(String dayIndName) {
        this.dayIndName = dayIndName;
    }

}
