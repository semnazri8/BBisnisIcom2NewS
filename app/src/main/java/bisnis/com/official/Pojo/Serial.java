package bisnis.com.official.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Serial {
    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("site_id")
    @Expose
    private String siteId;
    @SerializedName("post_date")
    @Expose
    private String postDate;
    @SerializedName("date")
    @Expose
    private DateHT1 date;
    @SerializedName("post_date_modified")
    @Expose
    private String postDateModified;
    @SerializedName("post_title")
    @Expose
    private String postTitle;
    @SerializedName("post_subtitle")
    @Expose
    private String postSubtitle;
    @SerializedName("post_summary")
    @Expose
    private String postSummary;
    @SerializedName("post_content")
    @Expose
    private String postContent;
    @SerializedName("post_image")
    @Expose
    private Object postImage;
    @SerializedName("post_image_type")
    @Expose
    private String postImageType;
    @SerializedName("post_video_content")
    @Expose
    private Object postVideoContent;
    @SerializedName("post_image_thumb")
    @Expose
    private String postImageThumb;
    @SerializedName("post_image_content")
    @Expose
    private String postImageContent;
    @SerializedName("post_name")
    @Expose
    private String postName;
    @SerializedName("post_status")
    @Expose
    private String postStatus;
    @SerializedName("post_status_xml")
    @Expose
    private String postStatusXml;
    @SerializedName("post_type")
    @Expose
    private String postType;
    @SerializedName("post_news_type")
    @Expose
    private String postNewsType;
    @SerializedName("post_feature")
    @Expose
    private String postFeature;
    @SerializedName("post_keyword")
    @Expose
    private String postKeyword;
    @SerializedName("post_author")
    @Expose
    private String postAuthor;
    @SerializedName("post_author_name")
    @Expose
    private String postAuthorName;
    @SerializedName("post_series")
    @Expose
    private String postSeries;
    @SerializedName("post_series_order")
    @Expose
    private String postSeriesOrder;
    @SerializedName("post_series_title")
    @Expose
    private String postSeriesTitle;
    @SerializedName("post_order")
    @Expose
    private String postOrder;
    @SerializedName("post_hits")
    @Expose
    private String postHits;
    @SerializedName("post_live")
    @Expose
    private String postLive;
    @SerializedName("post_editor")
    @Expose
    private String postEditor;
    @SerializedName("post_source")
    @Expose
    private String postSource;
    @SerializedName("post_features")
    @Expose
    private Object postFeatures;
    @SerializedName("post_date_created")
    @Expose
    private String postDateCreated;
    @SerializedName("post_level")
    @Expose
    private String postLevel;
    @SerializedName("post_postin1_title")
    @Expose
    private String postPostin1Title;
    @SerializedName("post_postin1_url")
    @Expose
    private String postPostin1Url;
    @SerializedName("post_postin2_title")
    @Expose
    private String postPostin2Title;
    @SerializedName("post_postin2_url")
    @Expose
    private String postPostin2Url;
    @SerializedName("post_postin3_title")
    @Expose
    private String postPostin3Title;
    @SerializedName("post_postin3_url")
    @Expose
    private String postPostin3Url;
    @SerializedName("post_editing")
    @Expose
    private String postEditing;
    @SerializedName("post_kolom")
    @Expose
    private String postKolom;
    @SerializedName("ps_id")
    @Expose
    private String psId;
    @SerializedName("tag_series_id")
    @Expose
    private String tagSeriesId;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public DateHT1 getDate() {
        return date;
    }

    public void setDate(DateHT1 date) {
        this.date = date;
    }

    public String getPostDateModified() {
        return postDateModified;
    }

    public void setPostDateModified(String postDateModified) {
        this.postDateModified = postDateModified;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostSubtitle() {
        return postSubtitle;
    }

    public void setPostSubtitle(String postSubtitle) {
        this.postSubtitle = postSubtitle;
    }

    public String getPostSummary() {
        return postSummary;
    }

    public void setPostSummary(String postSummary) {
        this.postSummary = postSummary;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public Object getPostImage() {
        return postImage;
    }

    public void setPostImage(Object postImage) {
        this.postImage = postImage;
    }

    public String getPostImageType() {
        return postImageType;
    }

    public void setPostImageType(String postImageType) {
        this.postImageType = postImageType;
    }

    public Object getPostVideoContent() {
        return postVideoContent;
    }

    public void setPostVideoContent(Object postVideoContent) {
        this.postVideoContent = postVideoContent;
    }

    public String getPostImageThumb() {
        return postImageThumb;
    }

    public void setPostImageThumb(String postImageThumb) {
        this.postImageThumb = postImageThumb;
    }

    public String getPostImageContent() {
        return postImageContent;
    }

    public void setPostImageContent(String postImageContent) {
        this.postImageContent = postImageContent;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }

    public String getPostStatusXml() {
        return postStatusXml;
    }

    public void setPostStatusXml(String postStatusXml) {
        this.postStatusXml = postStatusXml;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getPostNewsType() {
        return postNewsType;
    }

    public void setPostNewsType(String postNewsType) {
        this.postNewsType = postNewsType;
    }

    public String getPostFeature() {
        return postFeature;
    }

    public void setPostFeature(String postFeature) {
        this.postFeature = postFeature;
    }

    public String getPostKeyword() {
        return postKeyword;
    }

    public void setPostKeyword(String postKeyword) {
        this.postKeyword = postKeyword;
    }

    public String getPostAuthor() {
        return postAuthor;
    }

    public void setPostAuthor(String postAuthor) {
        this.postAuthor = postAuthor;
    }

    public String getPostAuthorName() {
        return postAuthorName;
    }

    public void setPostAuthorName(String postAuthorName) {
        this.postAuthorName = postAuthorName;
    }

    public String getPostSeries() {
        return postSeries;
    }

    public void setPostSeries(String postSeries) {
        this.postSeries = postSeries;
    }

    public String getPostSeriesOrder() {
        return postSeriesOrder;
    }

    public void setPostSeriesOrder(String postSeriesOrder) {
        this.postSeriesOrder = postSeriesOrder;
    }

    public String getPostSeriesTitle() {
        return postSeriesTitle;
    }

    public void setPostSeriesTitle(String postSeriesTitle) {
        this.postSeriesTitle = postSeriesTitle;
    }

    public String getPostOrder() {
        return postOrder;
    }

    public void setPostOrder(String postOrder) {
        this.postOrder = postOrder;
    }

    public String getPostHits() {
        return postHits;
    }

    public void setPostHits(String postHits) {
        this.postHits = postHits;
    }

    public String getPostLive() {
        return postLive;
    }

    public void setPostLive(String postLive) {
        this.postLive = postLive;
    }

    public String getPostEditor() {
        return postEditor;
    }

    public void setPostEditor(String postEditor) {
        this.postEditor = postEditor;
    }

    public String getPostSource() {
        return postSource;
    }

    public void setPostSource(String postSource) {
        this.postSource = postSource;
    }

    public Object getPostFeatures() {
        return postFeatures;
    }

    public void setPostFeatures(Object postFeatures) {
        this.postFeatures = postFeatures;
    }

    public String getPostDateCreated() {
        return postDateCreated;
    }

    public void setPostDateCreated(String postDateCreated) {
        this.postDateCreated = postDateCreated;
    }

    public String getPostLevel() {
        return postLevel;
    }

    public void setPostLevel(String postLevel) {
        this.postLevel = postLevel;
    }

    public String getPostPostin1Title() {
        return postPostin1Title;
    }

    public void setPostPostin1Title(String postPostin1Title) {
        this.postPostin1Title = postPostin1Title;
    }

    public String getPostPostin1Url() {
        return postPostin1Url;
    }

    public void setPostPostin1Url(String postPostin1Url) {
        this.postPostin1Url = postPostin1Url;
    }

    public String getPostPostin2Title() {
        return postPostin2Title;
    }

    public void setPostPostin2Title(String postPostin2Title) {
        this.postPostin2Title = postPostin2Title;
    }

    public String getPostPostin2Url() {
        return postPostin2Url;
    }

    public void setPostPostin2Url(String postPostin2Url) {
        this.postPostin2Url = postPostin2Url;
    }

    public String getPostPostin3Title() {
        return postPostin3Title;
    }

    public void setPostPostin3Title(String postPostin3Title) {
        this.postPostin3Title = postPostin3Title;
    }

    public String getPostPostin3Url() {
        return postPostin3Url;
    }

    public void setPostPostin3Url(String postPostin3Url) {
        this.postPostin3Url = postPostin3Url;
    }

    public String getPostEditing() {
        return postEditing;
    }

    public void setPostEditing(String postEditing) {
        this.postEditing = postEditing;
    }

    public String getPostKolom() {
        return postKolom;
    }

    public void setPostKolom(String postKolom) {
        this.postKolom = postKolom;
    }

    public String getPsId() {
        return psId;
    }

    public void setPsId(String psId) {
        this.psId = psId;
    }

    public String getTagSeriesId() {
        return tagSeriesId;
    }

    public void setTagSeriesId(String tagSeriesId) {
        this.tagSeriesId = tagSeriesId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
