
package bisnis.com.official.Pojo;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataTerkait {

    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("post_date")
    @Expose
    private String postDate;
    @SerializedName("post_title")
    @Expose
    private String postTitle;
    @SerializedName("post_subtitle")
    @Expose
    private String postSubtitle;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("post_type")
    @Expose
    private String postType;
    @SerializedName("category_id")
    @Expose
    private String categoryId;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostSubtitle() {
        return postSubtitle;
    }

    public void setPostSubtitle(String postSubtitle) {
        this.postSubtitle = postSubtitle;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

}
