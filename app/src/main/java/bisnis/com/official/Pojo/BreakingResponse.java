package bisnis.com.official.Pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class BreakingResponse implements Parcelable {

    public static final Creator<BreakingResponse> CREATOR = new Creator<BreakingResponse>() {
        @Override
        public BreakingResponse createFromParcel(Parcel in) {
            return new BreakingResponse(in);
        }

        @Override
        public BreakingResponse[] newArray(int size) {
            return new BreakingResponse[size];
        }
    };


    public static final int News_Thumbnail = 0;
    public static final int News_list = 1;
    public static final int News_grid = 2;
    public static final int News_ads = 3;
    public static final int NewsRV = 4;
    public static final int NewsIndex = 5;

    public int type, image_ads;
    public String type_get, kanal_id, full_short_month;
    public long milis;
    public String diakoran;
    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("topic_name")
    @Expose
    private String topic_name;
    @SerializedName("topic_id")
    @Expose
    private String topic_id;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("category_link")
    @Expose
    private String categoryLink;
    @SerializedName("category_parent_name")
    @Expose
    private String categoryParentName;
    @SerializedName("post_date")
    @Expose
    private String postDate;
    @SerializedName("post_created")
    @Expose
    private String postCreated;
    @SerializedName("date")
    @Expose
    private DateBreaking date;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("subtitle")
    @Expose
    private String subtitle;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("is_live")
    @Expose
    private Integer isLive;
    @SerializedName("image_thumbnail")
    @Expose
    private String imageThumbnail;
    @SerializedName("image_content")
    @Expose
    private String imageContent;
    @SerializedName("image_caption")
    @Expose
    private String imageCaption;
    @SerializedName("image_thumbnail_url")
    @Expose
    private String imageThumbnailUrl;
    @SerializedName("image_content_url")
    @Expose
    private String imageContentUrl;

    public BreakingResponse(Parcel in) {
        postDate = in.readString();
        categoryId = in.readString();
        postId = in.readString();
        type_get = in.readString();
    }

    public BreakingResponse() {

    }

    public String getDiakoran() {
        return diakoran == null ? "" : diakoran;
    }

    public void setDiakoran(String diakoran) {
        this.diakoran = diakoran;
    }

    public String getFull_short_month() {
        return full_short_month;
    }

    public void setFull_short_month(String full_short_month) {
        this.full_short_month = full_short_month;
    }

    public int getImage_ads() {
        return image_ads;
    }

    public void setImage_ads(int image_ads) {
        this.image_ads = image_ads;
    }

    public long getMilis() {
        return milis;
    }

    public void setMilis(long milis) {
        this.milis = milis;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getType_get() {
        return type_get == null ? "" : type_get;
    }

    public void setType_get(String type_get) {
        this.type_get = type_get;
    }

    public String getKanal_id() {
        return kanal_id;
    }

    public void setKanal_id(String kanal_id) {
        this.kanal_id = kanal_id;
    }

    public String getPostId() {
        return postId == null ? "" : postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getCategoryId() {
        return categoryId == null ? "" : categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryLink() {
        return categoryLink;
    }

    public void setCategoryLink(String categoryLink) {
        this.categoryLink = categoryLink;
    }

    public String getCategoryParentName() {
        return categoryParentName == null ? "" : categoryParentName;
    }

    public void setCategoryParentName(String categoryParentName) {
        this.categoryParentName = categoryParentName;
    }

    public String getPostDate() {
        return postDate == null ? "" : postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getPostCreated() {
        return postCreated;
    }

    public void setPostCreated(String postCreated) {
        this.postCreated = postCreated;
    }

    public DateBreaking getDate() {
        return date;
    }

    public void setDate(DateBreaking date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Integer getIsLive() {
        return isLive == null ? 0 : isLive;
    }

    public void setIsLive(Integer isLive) {
        this.isLive = isLive;
    }

    public String getImageThumbnail() {
        return imageThumbnail;
    }

    public void setImageThumbnail(String imageThumbnail) {
        this.imageThumbnail = imageThumbnail;
    }

    public String getImageContent() {
        return imageContent;
    }

    public void setImageContent(String imageContent) {
        this.imageContent = imageContent;
    }

    public String getImageCaption() {
        return imageCaption;
    }

    public void setImageCaption(String imageCaption) {
        this.imageCaption = imageCaption;
    }

    public String getImageThumbnailUrl() {
        return imageThumbnailUrl;
    }

    public void setImageThumbnailUrl(String imageThumbnailUrl) {
        this.imageThumbnailUrl = imageThumbnailUrl;
    }

    public String getImageContentUrl() {
        return imageContentUrl;
    }

    public void setImageContentUrl(String imageContentUrl) {
        this.imageContentUrl = imageContentUrl;
    }

    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(postDate);
        parcel.writeString(categoryId);
        parcel.writeString(postId);
        parcel.writeString(type_get);

    }
}
