package bisnis.com.official.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HotelResponse {

    public static final int type_title = 0;
    public static final int type_list = 1;
    public static final int type_thumbnail = 2;


    public int type;

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    @SerializedName("hotel_id")
    @Expose
    private String hotelId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("address2")
    @Expose
    private String address2;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("fax")
    @Expose
    private String fax;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("prov_id")
    @Expose
    private String provId;
    @SerializedName("prov_nama")
    @Expose
    private String provNama;
    @SerializedName("kab_id")
    @Expose
    private String kabId;
    @SerializedName("kab_nama")
    @Expose
    private String kabNama;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("modified_date")
    @Expose
    private String modifiedDate;
    @SerializedName("image_content")
    @Expose
    private String imageContent;
    @SerializedName("image_thumb")
    @Expose
    private String imageThumb;
    @SerializedName("image_content_url")
    @Expose
    private String imageContentUrl;
    @SerializedName("image_thumbnail_url")
    @Expose
    private String imageThumbnailUrl;

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getProvId() {
        return provId;
    }

    public void setProvId(String provId) {
        this.provId = provId;
    }

    public String getProvNama() {
        return provNama;
    }

    public void setProvNama(String provNama) {
        this.provNama = provNama;
    }

    public String getKabId() {
        return kabId;
    }

    public void setKabId(String kabId) {
        this.kabId = kabId;
    }

    public String getKabNama() {
        return kabNama;
    }

    public void setKabNama(String kabNama) {
        this.kabNama = kabNama;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getImageContent() {
        return imageContent;
    }

    public void setImageContent(String imageContent) {
        this.imageContent = imageContent;
    }

    public String getImageThumb() {
        return imageThumb;
    }

    public void setImageThumb(String imageThumb) {
        this.imageThumb = imageThumb;
    }

    public String getImageContentUrl() {
        return imageContentUrl;
    }

    public void setImageContentUrl(String imageContentUrl) {
        this.imageContentUrl = imageContentUrl;
    }

    public String getImageThumbnailUrl() {
        return imageThumbnailUrl;
    }

    public void setImageThumbnailUrl(String imageThumbnailUrl) {
        this.imageThumbnailUrl = imageThumbnailUrl;
    }


}
