package bisnis.com.official.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import bisnis.com.official.Listener.OnClickAdapterListener;
import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.R;
import bisnis.com.official.ViewHolder.IndexViewHolder;

public class IndexAdapter extends RecyclerView.Adapter<IndexViewHolder> {

    private Context mContext;
    private List<BreakingResponse> mValues = new ArrayList<>();
    int total_types;
    private OnClickAdapterListener listener;

    public IndexAdapter(Context context,OnClickAdapterListener listener) {
        this.mContext = context;
//        this.mValues = items;
        this.listener = listener;
    }

    @Override
    public IndexViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_file, parent, false);

        return new IndexViewHolder(view);
    }

    public void addOn(ArrayList<BreakingResponse> data) {
        mValues.addAll(data);
        total_types = mValues.size();
        notifyDataSetChanged();


    }

    @Override
    public void onBindViewHolder(IndexViewHolder holder, final int position) {

        Typeface bold = Typeface.createFromAsset(mContext.getAssets(), "Font/roboto_bold.ttf");
        final BreakingResponse object = mValues.get(position);
        RequestOptions myoptions = new RequestOptions()
                .placeholder(R.drawable.placeholder_small)
                .error(R.drawable.placeholder_small);

        holder.is_live.setTypeface(bold);
        if (object.getIsLive() == 0) {
            holder.is_live.setVisibility(View.GONE);
        }

        Glide.with(mContext).load(object.getImageContentUrl()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).apply(myoptions).into(holder.img_berita);

        holder.kanal_time.setText(Html.fromHtml(object.getCategoryName() + " | " + object.getFull_short_month()));
        holder.kanal_time.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
        holder.title_berita.setTypeface(bold);
        holder.title_berita.setText(object.getTitle());
        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickAja(String.valueOf(position), object.getType_get(), object.getCategoryId(), (ArrayList<BreakingResponse>) mValues);

            }
        });


    }

    @Override
    public int getItemCount() {
        return total_types;
    }
}
