package bisnis.com.official.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import bisnis.com.official.Fragment.FragmentIndex;
import bisnis.com.official.Listener.OnClickAdapterListener;
import bisnis.com.official.Model.ModelData;
import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.Pojo.HeadlineResponse;
import bisnis.com.official.R;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 09/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class KanalMultiViewAdapter extends RecyclerView.Adapter {
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    Context mContext;
    int total_types;
    private List<BreakingResponse> dataSet = new ArrayList<>();
    private List<HeadlineResponse> dataHeadline = new ArrayList<>();

    private RequestOptions myoptions_thumbnail, myoptions;
    private OnClickAdapterListener listener;

    public KanalMultiViewAdapter(Context context, OnClickAdapterListener listener) {
        this.mContext = context;
//        total_types = dataSet.size();
        this.listener = listener;
    }

    public static CharSequence join(CharSequence delimiter, int length, Object[] tokens) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < tokens.length; i++) {
            if (i > length) {
                break;
            }
            if (i > 0) {
                sb.append(delimiter);
            }
            sb.append(tokens[i]);
        }
        return sb.toString() + " ...";
    }

    public void addOn(ArrayList<BreakingResponse> data) {
//        dataSet = data;
        dataSet.addAll(data);
        total_types = dataSet.size();
        notifyDataSetChanged();
    }

    public void addOnHeadline(ArrayList<HeadlineResponse> data) {
//        dataSet = data;
        dataHeadline.addAll(data);
//        total_types = dataHeadline.size();
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case ModelData.News_Thumbnail:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_thumbnail, parent, false);

                return new NewsTumbnailViewType(view);
            case ModelData.News_list:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_file, parent, false);

                return new NewsListViewType(view);
            case ModelData.News_grid:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_grid, parent, false);

                return new NewsGridType(view);
            case ModelData.News_ads:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_ads, parent, false);

                return new NewsAdsType(view);
            case ModelData.NewsRecycler:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_rv, parent, false);

                return new NewsRVType(view);

            case ModelData.NewsIndex:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_index, parent, false);

                return new NewsIndexType(view);
        }

        return null;

    }

    @Override
    public int getItemViewType(int position) {
        switch (dataSet.get(position).type) {
            case 0:
                return ModelData.News_Thumbnail;
            case 1:
                return ModelData.News_list;
            case 2:
                return ModelData.News_grid;
            case 3:
                return ModelData.News_ads;
            case 4:
                return ModelData.NewsRecycler;
            case 5:
                return ModelData.NewsIndex;

            default:
                return -1;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final BreakingResponse object = dataSet.get(position);
        Typeface bold = Typeface.createFromAsset(mContext.getAssets(), "Font/roboto_bold.ttf");
        Typeface regular = Typeface.createFromAsset(mContext.getAssets(), "Font/roboto_regular.ttf");

        if (object != null) {

            if (!object.getDiakoran().equals("")) {
                myoptions_thumbnail = new RequestOptions()
                        .placeholder(R.drawable.placeholder_koran)
                        .error(R.drawable.placeholder_koran);

                myoptions = new RequestOptions()
                        .placeholder(R.drawable.placeholder_sq_koran)
                        .error(R.drawable.placeholder_sq_koran);
            } else {
                myoptions_thumbnail = new RequestOptions()
                        .placeholder(R.drawable.placeholder_large)
                        .error(R.drawable.placeholder_large);

                myoptions = new RequestOptions()
                        .placeholder(R.drawable.placeholder_small)
                        .error(R.drawable.placeholder_small);
            }


//            if (object.getCategoryParentName().equals("KORAN")) {
//
//
//            }


            switch (object.type) {
                case BreakingResponse.News_Thumbnail:
                    ((NewsTumbnailViewType) holder).title_berita.setText(Html.fromHtml(object.getTitle()));
                    ((NewsTumbnailViewType) holder).title_berita.setTypeface(bold);
                    ((NewsTumbnailViewType) holder).is_live.setTypeface(bold);

                    if (object.getIsLive() == 0) {
                        ((NewsTumbnailViewType) holder).is_live.setVisibility(View.GONE);
                    }

                    Glide.with(mContext).load(object.getImageContentUrl()).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).apply(myoptions_thumbnail).into(((NewsTumbnailViewType) holder).img_berita);

                    if (object.getCategoryParentName().equals("")) {
                        ((NewsTumbnailViewType) holder).kanal_title.setVisibility(View.GONE);
                    } else {

                        ((NewsTumbnailViewType) holder).kanal_title.setText(Html.fromHtml(object.getCategoryName()));
                    }
//                    ((NewsTumbnailViewType) holder).post_date.setText(getTimeAgo(object.getMilis()));
                    if (getTimeAgo(object.getMilis()).contains(" hari yang lalu") || getTimeAgo(object.getMilis()).contains("kemarin")) {
                        ((NewsTumbnailViewType) holder).post_date.setText(object.getFull_short_month());
                    } else {

                        ((NewsTumbnailViewType) holder).post_date.setText(getTimeAgo(object.getMilis()));
                    }
                    //TODO : time postnya belom yak
                    ((NewsTumbnailViewType) holder).ll_parent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            listener.onClickAja(String.valueOf(position), dataSet.get(position).getType_get(), dataSet.get(position).getKanal_id(), (ArrayList<BreakingResponse>) dataSet);

//                            Intent i = new Intent(mContext, ActivityDeatilBerita.class);
//                            i.putExtra("index", String.valueOf(position));
//                            i.putExtra("type_get", dataSet.get(position).getType_get());
//                            i.putExtra("kanal_id", dataSet.get(position).getKanal_id());
//                            mContext.startActivity(i);

                        }
                    });

                    break;
                case BreakingResponse.News_grid:
                    ((NewsGridType) holder).title_berita.setText(Html.fromHtml(object.getTitle()));
                    ((NewsGridType) holder).title_berita.setTypeface(bold);
                    ((NewsGridType) holder).is_live.setTypeface(bold);

                    if (object.getIsLive() == 0) {
                        ((NewsGridType) holder).is_live.setVisibility(View.GONE);
                    }
//                    ((NewsGridType) holder).post_date.setText(getTimeAgo(object.getMilis()));
                    if (getTimeAgo(object.getMilis()).contains(" hari yang lalu") || getTimeAgo(object.getMilis()).contains("kemarin")) {
                        ((NewsGridType) holder).post_date.setText(object.getFull_short_month());
                    } else {

                        ((NewsGridType) holder).post_date.setText(getTimeAgo(object.getMilis()));
                    }
                    Glide.with(mContext).load(object.getImageContentUrl()).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).apply(myoptions).into(((NewsGridType) holder).img_berita);
                    //TODO : time postnya belom yak
                    ((NewsGridType) holder).ll_parent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            listener.onClickAja(String.valueOf(position), dataSet.get(position).getType_get(), dataSet.get(position).getKanal_id(), (ArrayList<BreakingResponse>) dataSet);

//                            Intent i = new Intent(mContext, ActivityDeatilBerita.class);
//                            i.putExtra("index", String.valueOf(position));
//                            i.putExtra("type_get", dataSet.get(position).getType_get());
//                            i.putExtra("kanal_id", dataSet.get(position).getKanal_id());
////                            Log.d("index", String.valueOf(object));
//                            mContext.startActivity(i);
                        }
                    });
                    break;

                case BreakingResponse.News_list:
                    ((NewsListViewType) holder).is_live.setTypeface(bold);

                    if (object.getIsLive() == 0) {
                        ((NewsListViewType) holder).is_live.setVisibility(View.GONE);
                    }
                    int MAX_COUNT = 9;
                    String haha = String.valueOf(Html.fromHtml(object.getTitle()));
                    String[] strings = haha.split(" ");
                    int wordsCount = strings.length;

                    ((NewsListViewType) holder).title_berita.setText(wordsCount > MAX_COUNT ? join(" ", MAX_COUNT, strings) : haha);
                    ((NewsListViewType) holder).title_berita.setText(object.getTitle());
                    ((NewsListViewType) holder).title_berita.setTypeface(bold);
                    ((NewsListViewType) holder).kanal_date.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);


                    if (object.getCategoryParentName().equals("")) {
                        if (getTimeAgo(object.getMilis()).contains(" hari yang lalu") || getTimeAgo(object.getMilis()).contains("kemarin")) {
                            ((NewsListViewType) holder).kanal_date.setText(object.getFull_short_month());
                        } else {

                            ((NewsListViewType) holder).kanal_date.setText(getTimeAgo(object.getMilis()));
                        }

//                        ((NewsListViewType) holder).kanal_date.setText(getTimeAgo(object.getMilis()));

                    } else {

                        if (getTimeAgo(object.getMilis()).contains(" hari yang lalu") || getTimeAgo(object.getMilis()).contains("kemarin")) {
                            ((NewsListViewType) holder).kanal_date.setText(Html.fromHtml(object.getCategoryName() + " | " + object.getFull_short_month()));
                        } else {

                            ((NewsListViewType) holder).kanal_date.setText(Html.fromHtml(object.getCategoryName() + " | " + getTimeAgo(object.getMilis())));
                        }


//                        ((NewsListViewType) holder).kanal_date.setText(Html.fromHtml(object.getCategoryParentName()) + " | " + getTimeAgo(object.getMilis()));
                    }
                    Glide.with(mContext).load(object.getImageContentUrl()).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).apply(myoptions).into(((NewsListViewType) holder).img_berita);
                    ((NewsListViewType) holder).ll_parent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            listener.onClickAja(String.valueOf(position), dataSet.get(position).getType_get(), dataSet.get(position).getKanal_id(), (ArrayList<BreakingResponse>) dataSet);

                        }
                    });
                    break;

                case BreakingResponse.News_ads:

                    Glide.with(mContext).load(object.getImage_ads()).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).apply(myoptions_thumbnail).into(((NewsAdsType) holder).img_ads);

                    ((NewsAdsType) holder).img_ads.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            startNewActivity(mContext, "bisnisindoneisa.epaper");
                        }
                    });
                    break;

                case BreakingResponse.NewsRV:

                    break;


                case BreakingResponse.NewsIndex:

                    ((NewsIndexType) holder).parent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(mContext, FragmentIndex.class);
                            mContext.startActivity(i);
                        }
                    });


                    break;
            }
        }


    }

    public void startNewActivity(Context context, String packageName) {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent == null) {
            // Bring user to the market or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + packageName));
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public String getTimeAgo(long time) {
//        if (time < 1000000000000L) {
//            // if timestamp given in seconds, convert to millis
//            time *= 1000;
//        }

        long now = System.currentTimeMillis();
//        if (time > now || time <= 0) {
//            return null;
//        }

        // TODO: localize
        final long diff = now - time;
        if (diff < 2 * MINUTE_MILLIS) {
            return "1 menit yang lalu";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " menit yang lalu";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "1 jam yang lalu";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " jam yang lalu";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "kemarin";
        } else {
            return diff / DAY_MILLIS + " hari yang lalu";
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class NewsTumbnailViewType extends RecyclerView.ViewHolder {

        TextView title_berita, kanal_title, post_date, is_live;
        ImageView img_berita;
        LinearLayout ll_parent;

        public NewsTumbnailViewType(View itemView) {
            super(itemView);

            this.title_berita = itemView.findViewById(R.id.title_berita);
            this.kanal_title = itemView.findViewById(R.id.kanal_text);
            this.post_date = itemView.findViewById(R.id.date_berita);
            this.img_berita = itemView.findViewById(R.id.img_berita);
            this.ll_parent = itemView.findViewById(R.id.parent);
            this.is_live = itemView.findViewById(R.id.is_live);
        }
    }

    public static class NewsListViewType extends RecyclerView.ViewHolder {

        TextView title_berita, kanal_date, is_live;
        ImageView img_berita;
        LinearLayout ll_parent;

        public NewsListViewType(View itemView) {
            super(itemView);

            this.title_berita = itemView.findViewById(R.id.title_berita);
            this.kanal_date = itemView.findViewById(R.id.kanal_time);
            this.img_berita = itemView.findViewById(R.id.img_berita);
            this.ll_parent = itemView.findViewById(R.id.parent);
            this.is_live = itemView.findViewById(R.id.is_live);
        }
    }

    public static class NewsAdsType extends RecyclerView.ViewHolder {
        ImageView img_ads;

        public NewsAdsType(View itemView) {
            super(itemView);

            this.img_ads = itemView.findViewById(R.id.img_ads);
        }
    }

    public static class NewsGridType extends RecyclerView.ViewHolder {
        TextView title_berita, post_date, is_live;
        ImageView img_berita;
        LinearLayout ll_parent;

        public NewsGridType(View itemView) {
            super(itemView);

            this.title_berita = itemView.findViewById(R.id.title_berita);
            this.post_date = itemView.findViewById(R.id.date_berita);
            this.img_berita = itemView.findViewById(R.id.img_berita);
            this.ll_parent = itemView.findViewById(R.id.parent);
            this.is_live = itemView.findViewById(R.id.is_live);
        }
    }

    public static class NewsRVType extends RecyclerView.ViewHolder {
        RecyclerView rv;

        public NewsRVType(View itemView) {
            super(itemView);

            this.rv = itemView.findViewById(R.id.rv_item);

        }
    }

    public static class NewsIndexType extends RecyclerView.ViewHolder {
        TextView txt_indeks;
        LinearLayout parent;

        public NewsIndexType(View itemView) {
            super(itemView);

            this.txt_indeks = itemView.findViewById(R.id.gotoIndex);
            this.parent = itemView.findViewById(R.id.parent);


        }
    }


}
