package bisnis.com.official.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;

import bisnis.com.official.Interface.LikeInterface;
import bisnis.com.official.Model.ComentModel;
import bisnis.com.official.R;
import bisnis.com.official.ViewHolder.KomenViewHolder;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 27/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class AdapterKomen extends RecyclerView.Adapter<KomenViewHolder> {

    private Context mContext;
    private List<ComentModel> mValues;
    private LikeInterface likeInterface;

    public AdapterKomen(Context context, List<ComentModel> items, LikeInterface likeInterface){
        mContext = context;
        mValues = items;
        this.likeInterface = likeInterface;
    }
    @Override
    public KomenViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);


        return new KomenViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final KomenViewHolder holder, final int position) {

        holder.txt_username_komen.setText(mValues.get(position).getUser_name());
        holder.txt_username_date.setText(mValues.get(position).getComent_date());
        holder.txt_komen.setText(mValues.get(position).getComent_txt());
        holder.txt_coment_count.setText(String.valueOf(mValues.get(position).getComment_count()));
        holder.txt_comment_like.setText(String.valueOf(mValues.get(position).getComment_count()));

        Glide.with(mContext).load(mValues.get(position).getImg_user()).into(holder.user_image_komen);


        if (mValues.get(position).getLike_satus() == 0){
            holder.img_like.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_love_line));
//            Glide.with(mContext).load(R.drawable.ic_love_line).into(holder.img_like);
        }else{
            holder.img_like.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_love_full));
//            Glide.with(mContext).load(R.drawable.ic_love_full).into(holder.img_like);
        }

        holder.ll_komen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });

        holder.ll_likes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mValues.get(position).getLike_satus() == 1){
                    mValues.get(position).setLike_satus(0);
                    notifyDataSetChanged();
                    Glide.with(mContext).load(R.drawable.ic_love_line).into(holder.img_like);
                    likeInterface.onLikeListener(0);
                }else{
                    mValues.get(position).setLike_satus(1);
                    notifyDataSetChanged();
                    Glide.with(mContext).load(R.drawable.ic_love_full).into(holder.img_like);
                    likeInterface.onLikeListener(1);
                }

            }
        });

        holder.ll_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
}
