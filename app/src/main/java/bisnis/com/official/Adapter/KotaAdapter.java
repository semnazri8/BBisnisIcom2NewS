package bisnis.com.official.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bisnis.com.official.Listener.CallListener;
import bisnis.com.official.Model.ModelKota;
import bisnis.com.official.Pojo.HotelResponse;
import bisnis.com.official.R;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 05/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class KotaAdapter extends RecyclerView.Adapter {

    Context mContext;
    int total_types;
    private List<HotelResponse> dataSet;
    private CallListener listener;

    public KotaAdapter(ArrayList<HotelResponse> data, Context context, CallListener listener) {
        this.dataSet = data;
        this.mContext = context;
        total_types = dataSet.size();
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case ModelKota.type_title:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_title_directory, parent, false);
                return new DirectoryTitleViewType(view);
            case ModelKota.type_list:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_list_directory, parent, false);
                return new DirectoryListViewType(view);
            case ModelKota.type_thumbnail:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_image_directory, parent, false);
                return new DirectoryThumbnailViewType(view);
        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        switch (dataSet.get(position).type) {
            case 0:
                return ModelKota.type_title;
            case 1:
                return ModelKota.type_list;
            case 2:
                return ModelKota.type_thumbnail;
            default:
                return -1;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final HotelResponse object = dataSet.get(position);

        if (object != null) {
            switch (object.type) {
                case ModelKota.type_title:
                    ((DirectoryTitleViewType) holder).txt_title.setText(object.getName());
                    break;
                case ModelKota.type_list:
                    ((DirectoryListViewType) holder).txt_title.setText(object.getName());
                    ((DirectoryListViewType) holder).web.setText(object.getWebsite());
                    ((DirectoryListViewType) holder).phone.setText(object.getPhone());
                    ((DirectoryListViewType) holder).loc.setText(object.getAddress1() + "\n" + object.getAddress2());

                    ((DirectoryListViewType) holder).web.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(object.getWebsite()));
                            mContext.startActivity(intent);
                        }
                    });

                    ((DirectoryListViewType) holder).phone.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.call(object.getPhone());
                        }
                    });
                    break;
                case ModelKota.type_thumbnail:
                    Glide.with(mContext).load(object.getImageContentUrl().replace("w=300&h=300", "w=600&h=400")).into(((DirectoryThumbnailViewType) holder).img_directions);
                    ((DirectoryThumbnailViewType) holder).txt_title.setText(object.getName());
                    ((DirectoryThumbnailViewType) holder).web.setText(object.getWebsite());
                    ((DirectoryThumbnailViewType) holder).phone.setText(object.getPhone());
                    ((DirectoryThumbnailViewType) holder).loc.setText(object.getAddress1() + "\n" + object.getAddress2());

                    ((DirectoryThumbnailViewType) holder).web.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(object.getWebsite()));
                            mContext.startActivity(intent);
                        }
                    });
                    ((DirectoryThumbnailViewType) holder).phone.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.call(object.getPhone());
                        }
                    });


                    break;

            }
        }

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void updatelist(List<HotelResponse> list) {
        dataSet = list;
        notifyDataSetChanged();
    }

    public static class DirectoryTitleViewType extends RecyclerView.ViewHolder {
        TextView txt_title;

        public DirectoryTitleViewType(View itemView) {
            super(itemView);

            this.txt_title = itemView.findViewById(R.id.txt_title);
        }
    }

    public static class DirectoryListViewType extends RecyclerView.ViewHolder {
        TextView txt_title, loc, phone, web;

        public DirectoryListViewType(View itemView) {
            super(itemView);

            this.txt_title = itemView.findViewById(R.id.txt_title);
            this.loc = itemView.findViewById(R.id.txt_location);
            this.phone = itemView.findViewById(R.id.txt_phone);
            this.web = itemView.findViewById(R.id.txt_website);
        }
    }

    public static class DirectoryThumbnailViewType extends RecyclerView.ViewHolder {
        TextView txt_title, loc, phone, web;
        ImageView img_directions;

        public DirectoryThumbnailViewType(View itemView) {
            super(itemView);

            this.txt_title = itemView.findViewById(R.id.txt_title);
            this.img_directions = itemView.findViewById(R.id.img_directory);
            this.loc = itemView.findViewById(R.id.txt_location);
            this.phone = itemView.findViewById(R.id.txt_phone);
            this.web = itemView.findViewById(R.id.txt_website);
        }
    }

}
