package bisnis.com.official.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Calendar;
import java.util.List;

import bisnis.com.official.ActivitySingleDetail;
import bisnis.com.official.Pojo.Terkait;
import bisnis.com.official.R;
import bisnis.com.official.ViewHolder.BeritaTerkaitViewHolder;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 26/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class AdapterBeritaTerkait extends RecyclerView.Adapter<BeritaTerkaitViewHolder> {

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    long currentTime = Calendar.getInstance().getTimeInMillis();
    FragmentManager fragmentManager;
    private Context mContext;
    private List<Terkait> mValues;
    private FragmentTransaction ft;

    public AdapterBeritaTerkait(Context context, List<Terkait> items) {
        mContext = context;
        mValues = items;

    }

    public static CharSequence join(CharSequence delimiter, int length, Object[] tokens) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < tokens.length; i++) {
            if (i > length) {
                break;
            }
            if (i > 0) {
                sb.append(delimiter);
            }
            sb.append(tokens[i]);
        }
        return sb.toString() + " ...";
    }

    @Override
    public BeritaTerkaitViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_file_terkait, parent, false);

        return new BeritaTerkaitViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BeritaTerkaitViewHolder holder, final int position) {

        int MAX_COUNT = 9;
        String haha = mValues.get(position).getTitle();
        String[] strings = haha.split(" ");
        int wordsCount = strings.length;

        if (haha.contains("\"")) {

           String newhaha = haha.replaceAll("\\\\", "");
            holder.title_terkait.setText(Html.fromHtml(newhaha));
        }else{
            holder.title_terkait.setText(Html.fromHtml(haha));
        }



        Typeface bold = Typeface.createFromAsset(mContext.getAssets(), "Font/roboto_bold.ttf");
        holder.title_terkait.setTypeface(bold);
        holder.img_terkait.setVisibility(View.GONE);
//        holder.title_terkait.setText(wordsCount > MAX_COUNT ? join(" ", MAX_COUNT, strings) : haha);

//        if (getTimeAgo(mValues.get(position).getMilis_postdate()).contains(" hari yang lalu")) {
        holder.kanal_time.setVisibility(View.GONE);
        holder.kanal_time.setText(Html.fromHtml(mValues.get(position).getFull_short_month()));
//        } else {

//            holder.kanal_time.setText(Html.fromHtml(getTimeAgo(mValues.get(position).getMilis_postdate())));
//        }

        holder.title_terkait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Bundle bundle = new Bundle();
//                bundle.putString(");
//                bundle.putString("categoty_id", mValues.get(position).getCategoryId());
//                bundle.putString("post_id", mValues.get(position).getPostId());
//                Fragment d2 = new Fragment_detail();
//                d2.setArguments(bundle);

                Intent i = new Intent(mContext, ActivitySingleDetail.class);
                i.putExtra("post_date", mValues.get(position).getDate().getYear() + mValues.get(position).getDate().getMonth() + mValues.get(position).getDate().getDay());
                i.putExtra("categoty_id", mValues.get(position).getCategoryId());
                i.putExtra("post_id", mValues.get(position).getPostId());

                mContext.startActivity(i);
            }
        });

    }

    public String getTimeAgo(long time) {
        long now = System.currentTimeMillis();

        // TODO: localize
        final long diff = currentTime - time;
        Log.d("aweu1", String.valueOf(now));
        Log.d("aweu2", String.valueOf(time));
        Log.d("aweu3", String.valueOf(diff));

        if (diff < 2 * MINUTE_MILLIS) {
            return "1 menit yang lalu";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " menit yang lalu";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "1 jam yang lalu";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " jam yang lalu";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "kemarin";
        } else {
            return diff / DAY_MILLIS + " hari yang lalu";
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
}
