package bisnis.com.official.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import bisnis.com.official.R;
import bisnis.com.official.Utilities.Bookmarks;
import bisnis.com.official.ViewHolder.BookmarkViewHolder;

public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkViewHolder> {

    private List<Bookmarks> mValues;
    private Context mContext;

    public BookmarkAdapter(Context context, List<Bookmarks> items) {
        mContext = context;
        mValues = items;
    }

    public static CharSequence join(CharSequence delimiter, int length, Object[] tokens) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < tokens.length; i++) {
            if (i > length) {
                break;
            }
            if (i > 0) {
                sb.append(delimiter);
            }
            sb.append(tokens[i]);
        }
        return sb.toString() + " ...";
    }

    @Override
    public BookmarkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_file, parent, false);


        return new BookmarkViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BookmarkViewHolder holder, int position) {
        final Bookmarks object = mValues.get(position);
        Typeface bold = Typeface.createFromAsset(mContext.getAssets(), "Font/roboto_bold.ttf");
        Typeface regular = Typeface.createFromAsset(mContext.getAssets(), "Font/roboto_regular.ttf");

        RequestOptions myoptions = new RequestOptions()
                .placeholder(R.drawable.placeholder_small)
                .error(R.drawable.placeholder_small);

        int MAX_COUNT = 9;
        String haha = object.getTitle();
        String[] strings = haha.split(" ");
        int wordsCount = strings.length;

        holder.is_live.setTypeface(bold);
        holder.is_live.setVisibility(View.GONE);
        holder.title_berita.setTypeface(bold);
        holder.title_berita.setText(wordsCount > MAX_COUNT ? join(" ", MAX_COUNT, strings) : haha);
        holder.kanal_date.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
        holder.kanal_date.setText(Html.fromHtml(object.getKanal_name() + " | " + object.getTime_news()));

        Glide.with(mContext).load(object.getUrl_img()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).apply(myoptions).into(holder.img_berita);

        holder.ll_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                listener.onClickAja(String.valueOf(position), dataSet.get(position).getType_get(), dataSet.get(position).getKanal_id(), (ArrayList<BreakingResponse>) dataSet);

            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
}
