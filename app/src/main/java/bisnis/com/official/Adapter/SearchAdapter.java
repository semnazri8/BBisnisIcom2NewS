package bisnis.com.official.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bisnis.com.official.Listener.SearchOnClickListener;
import bisnis.com.official.Pojo.Search;
import bisnis.com.official.R;
import bisnis.com.official.ViewHolder.SearchViewHolder;

public class SearchAdapter extends RecyclerView.Adapter<SearchViewHolder> {

    private List<Search> mValues = new ArrayList<>();
    private Context mContext;
    private SearchOnClickListener listener;
    private String date,date2;
    int total_types;

    public SearchAdapter(Context context, SearchOnClickListener listener) {
        mContext = context;
//        mValues = items;
        this.listener = listener;

    }

    @Override
    public SearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_file, parent, false);


        return new SearchViewHolder(view);
    }

    public void addOn(ArrayList<Search> data) {
        mValues.addAll(data);
        total_types = mValues.size();
        notifyDataSetChanged();


    }

    @Override
    public void onBindViewHolder(SearchViewHolder holder, final int position) {

        final Search object = mValues.get(position);
        RequestOptions myoptions = new RequestOptions()
                .placeholder(R.drawable.placeholder_small)
                .error(R.drawable.placeholder_small);

        Typeface bold = Typeface.createFromAsset(mContext.getAssets(), "Font/roboto_bold.ttf");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date mDate = sdf.parse(mValues.get(position).getPostDate());

            DateFormat format = new SimpleDateFormat("dd MMM yyyy, HH:mm");
            String newformat = format.format(mDate);
            date2 = newformat;
            Log.d("date2", date2);

        } catch (ParseException e) {
            e.printStackTrace();
        }


        Glide.with(mContext).load(object.getImageThumbnailUrl().get(0)).apply(myoptions).into(holder.img_berita);

//        holder.kanal_time.setText(Html.fromHtml(mValues.get(position).getCategoryId().getCategoryName() + " | " + object.getFull_short_month()));
        holder.kanal_time.setText(Html.fromHtml(date2 + " WIB"));
        holder.kanal_time.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
        holder.title_berita.setTypeface(bold);
        holder.title_berita.setText(object.getTitle().get(0));
        holder.is_live.setVisibility(View.GONE);
        holder.ll_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    Date mDate = sdf.parse(mValues.get(position).getPostDate());

                    DateFormat format = new SimpleDateFormat("yyyyMMdd");
                    String newformat = format.format(mDate);
                    date = newformat;
                    Log.d("datesa", date);

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                listener.OnCick(

                        date,
                        mValues.get(position).getCategoryId(),
                        mValues.get(position).getPostId(), (ArrayList<Search>) mValues, String.valueOf(position));
            }
        });


    }

    @Override
    public int getItemCount() {
        return total_types;
    }
}
