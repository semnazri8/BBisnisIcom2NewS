package bisnis.com.official.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import bisnis.com.official.Listener.OnClickAdapterListener;
import bisnis.com.official.Pojo.BreakingResponse;
import bisnis.com.official.R;
import bisnis.com.official.ViewHolder.HeadlineViewHolder;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 21/03/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class HeadlineAdater extends RecyclerView.Adapter<HeadlineViewHolder> {
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private Context mConctext;
    private List<BreakingResponse> mValues;
    private RequestOptions myoptions_thumbnail, myoptions;
    private OnClickAdapterListener listener;

    public HeadlineAdater(Context context, List<BreakingResponse> items,OnClickAdapterListener listener) {
        mConctext = context;
        mValues = items;
        this.listener = listener;

    }

    @Override
    public HeadlineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_thumbnail_headline, parent, false);


        return new HeadlineViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HeadlineViewHolder holder, final int position) {
        Typeface bold = Typeface.createFromAsset(mConctext.getAssets(), "Font/roboto_bold.ttf");
        Typeface regular = Typeface.createFromAsset(mConctext.getAssets(), "Font/roboto_regular.ttf");

        if (mValues.get(position).getKanal_id().equals("242")) {
            myoptions_thumbnail = new RequestOptions()
                    .placeholder(R.drawable.placeholder_koran)
                    .error(R.drawable.placeholder_koran);

            myoptions = new RequestOptions()
                    .placeholder(R.drawable.placeholder_sq_koran)
                    .error(R.drawable.placeholder_sq_koran);
        } else {
            myoptions_thumbnail = new RequestOptions()
                    .placeholder(R.drawable.placeholder_large)
                    .error(R.drawable.placeholder_large);

            myoptions = new RequestOptions()
                    .placeholder(R.drawable.placeholder_small)
                    .error(R.drawable.placeholder_small);
        }


        Glide.with(mConctext).load(mValues.get(position).getImageContentUrl()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).apply(myoptions_thumbnail).into(holder.img_berita);


//        Glide.with(mConctext).load(mValues.get(position).getImageContentUrl()).into(holder.img_berita);
        holder.title_berita.setText(mValues.get(position).getTitle());
        holder.title_berita.setTypeface(bold);
        holder.date_berita.setText(getTimeAgo(mValues.get(position).getMilis()));
        holder.date_berita.setVisibility(View.GONE);

        holder.img_berita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(String.valueOf(position), mValues.get(position).getType_get(), mValues.get(position).getKanal_id());

//                Intent i = new Intent(mConctext, ActivityDeatilBerita.class);
//                i.putExtra("index", String.valueOf(position));
//                i.putExtra("type_get", mValues.get(position).getType_get());
//                i.putExtra("kanal_id", mValues.get(position).getKanal_id());
////                            Log.d("index", String.valueOf(object));
//                mConctext.startActivity(i);
            }
        });

    }

    public String getTimeAgo(long time) {
//        if (time < 1000000000000L) {
//            // if timestamp given in seconds, convert to millis
//            time *= 1000;
//        }

        long now = System.currentTimeMillis();
//        if (time > now || time <= 0) {
//            return null;
//        }

        // TODO: localize
        final long diff = now - time;
        if (diff < 2 * MINUTE_MILLIS) {
            return "1 menit yang lalu";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " menit yang lalu";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "1 jam yang lalu";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " jam yang lalu";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "kemarin";
        } else {
            return diff / DAY_MILLIS + " hari yang lalu";
        }

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
}
