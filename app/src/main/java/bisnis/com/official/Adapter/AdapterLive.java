package bisnis.com.official.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import bisnis.com.official.Pojo.Live;
import bisnis.com.official.R;
import bisnis.com.official.ViewHolder.LiveViewHolder;

public class AdapterLive extends RecyclerView.Adapter<LiveViewHolder> {

    private Context mContext;
    private List<Live> mValues;

    public AdapterLive(Context context, List<Live> items) {
        this.mContext = context;
        this.mValues = items;

    }

    @Override
    public LiveViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_live, parent, false);


        return new LiveViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final LiveViewHolder holder, int position) {
        Typeface bold = Typeface.createFromAsset(mContext.getAssets(), "Font/roboto_bold.ttf");
        Typeface regular = Typeface.createFromAsset(mContext.getAssets(), "Font/roboto_regular.ttf");

        String css = "<style>\n" +
                ".wrapper {\n" +
                "    line-height: 1.8\n" +
                "}\n" +
                "</style>";

        String css_content = "<style>" +
                "img{" +
                "width:100% !important;\n" +
                "}" +
                "table{" +
                "width:100% !important;\n" +
                "}</style>";


        holder.txt_live_time.setTypeface(bold);
        holder.txt_live_title.setTypeface(bold);

        holder.txt_live_time.setText("LIVE | " + mValues.get(position).getDate().getHour() + ":" + mValues.get(position).getDate().getMinute() + " wib");
        holder.txt_live_title.setText(mValues.get(position).getPliveTitle());

        RequestOptions myoptions_thumbnail = new RequestOptions()
                .placeholder(R.drawable.placeholder_large)
                .error(R.drawable.placeholder_large);

        Glide.with(mContext).load(mValues.get(position).getPliveImageThumb()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                holder.img_live.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).apply(myoptions_thumbnail).into(holder.img_live);

        String content = mValues.get(position).getPliveSummary();
        String content_berita = css_content + "";

//        holder.webview_live.getSettings().setJavaScriptEnabled(true);

//        holder.webview_live.loadData(css + "<div class=\"wrapper\">" + css_content + content+"</div>", "text/html", "utf-8");
        holder.webview_live.setText(Html.fromHtml(String.valueOf(Html.fromHtml(content))));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
}
