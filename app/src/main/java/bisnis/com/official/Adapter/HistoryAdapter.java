package bisnis.com.official.Adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import bisnis.com.official.Listener.DeleteListener;
import bisnis.com.official.Model.SearchModel;
import bisnis.com.official.R;
import bisnis.com.official.ViewHolder.HistoryViewHolder;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryViewHolder> {
    private List<SearchModel> mValues;
    private Context mContext;
    private DeleteListener listener;

    public HistoryAdapter(Context context, List<SearchModel> items, DeleteListener listener) {
        this.mValues = items;
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recent_srch, parent, false);


        return new HistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HistoryViewHolder holder, final int position) {
        holder.history_title.setText(mValues.get(position).getKeyword());

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
}
