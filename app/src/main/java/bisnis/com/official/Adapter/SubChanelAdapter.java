package bisnis.com.official.Adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bisnis.com.official.Fragment.Fragment_kanalParent;
import bisnis.com.official.MainActivity;
import bisnis.com.official.Model.ModelChanel;
import bisnis.com.official.Model.ModelSubChanel;
import bisnis.com.official.R;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 09/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class SubChanelAdapter extends RecyclerView.Adapter {
    Context mContext;
    int total_types;
    FragmentManager fragmentManager;
    private List<ModelSubChanel> dataSet;
    private FragmentTransaction ft;

    public SubChanelAdapter(ArrayList<ModelSubChanel> data, Context context) {
        this.dataSet = data;
        this.mContext = context;
        total_types = dataSet.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case ModelChanel.ChanelGrid:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_grid_chanel, parent, false);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                    }
                });

                return new ChanelGricViewType(view);
            case ModelChanel.ChanelList:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_list_chanel, parent, false);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        fragmentManager = ((MainActivity) mContext).getSupportFragmentManager();
//                        ft = fragmentManager.beginTransaction();
//                        ft.replace(R.id.container_body, new Fragment_kanalParent(), "home").addToBackStack("menu");
//                        ft.commit();
                    }
                });
                return new ChanelListViewType(view);


        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        switch (dataSet.get(position).type) {
            case 0:
                return ModelChanel.ChanelList;
            case 1:
                return ModelChanel.ChanelGrid;
            default:
                return -1;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        final ModelSubChanel object = dataSet.get(position);
        if (object != null) {
            switch (object.type) {

                case ModelChanel.ChanelList:
                    ((ChanelListViewType) holder).title_berita.setText(object.text);
                    Glide.with(mContext).load(object.data).into(((ChanelListViewType) holder).imageView);

                    ((ChanelListViewType) holder).imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String kanal_id = String.valueOf(object.kanal_id);
                            Bundle bundle = new Bundle();
                            bundle.putString("kanalid", kanal_id);
                            Fragment d2 = new Fragment_kanalParent();
                            d2.setArguments(bundle);

                            fragmentManager = ((MainActivity) mContext).getSupportFragmentManager();
                            ft = fragmentManager.beginTransaction();
                            ft.replace(R.id.container_body, d2, "chanel").addToBackStack("chanel");
                            ft.commit();
                        }
                    });
                    break;

                case ModelChanel.ChanelGrid:
                    ((ChanelGricViewType) holder).title_berita.setText(object.text);
                    Glide.with(mContext).load(object.data).into(((ChanelGricViewType) holder).imageView);

                    ((ChanelGricViewType) holder).imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

//                            Toast.makeText(mContext, String.valueOf(object.kanal_id), Toast.LENGTH_SHORT).show();
                            String kanal_id = String.valueOf(object.kanal_id);
                            Bundle bundle = new Bundle();
                            bundle.putString("kanalid", kanal_id);
                            Fragment d2 = new Fragment_kanalParent();
                            d2.setArguments(bundle);

                            fragmentManager = ((MainActivity) mContext).getSupportFragmentManager();
                            ft = fragmentManager.beginTransaction();
                            ft.replace(R.id.container_body, d2, "chanel").addToBackStack("chanel");
                            ft.commit();
                        }
                    });
                    break;


            }
        }

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ChanelGricViewType extends RecyclerView.ViewHolder {

        TextView title_berita;
        ImageView imageView;
        LinearLayout linearLayout;

        public ChanelGricViewType(View itemView) {
            super(itemView);

            title_berita = itemView.findViewById(R.id.title_listchanel);
            imageView = itemView.findViewById(R.id.img_listchanel);
        }
    }

    public static class ChanelListViewType extends RecyclerView.ViewHolder {

        TextView title_berita;
        ImageView imageView;
        LinearLayout linearLayout;

        public ChanelListViewType(View itemView) {
            super(itemView);

            title_berita = itemView.findViewById(R.id.title_listchanel);
            imageView = itemView.findViewById(R.id.img_listchanel);
        }
    }


}
