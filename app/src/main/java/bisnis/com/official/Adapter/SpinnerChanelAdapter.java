package bisnis.com.official.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import bisnis.com.official.Model.ModelKota;
import bisnis.com.official.Model.ModelSpinnerKota;
import bisnis.com.official.R;


/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 10/2/17.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class SpinnerChanelAdapter extends BaseAdapter implements android.widget.SpinnerAdapter {

    private Context mContext;
    private List<ModelSpinnerKota> mValues;

    public SpinnerChanelAdapter(Context context, List<ModelSpinnerKota> items) {
        mContext = context;
        mValues = items;

    }

    @Override
    public int getCount() {
        return mValues.size();
    }

    @Override
    public Object getItem(int position) {
        return mValues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_spinner_index, parent, false);
        }

        TextView kota_name = convertView.findViewById(R.id.item_name);
        TextView kota_id = convertView.findViewById(R.id.id_item);

        kota_name.setText(mValues.get(position).getItem_name());
        kota_id.setText(String.valueOf(mValues.get(position).getItem_id()));


        return convertView;
    }
}
