package bisnis.com.official.Adapter;

import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import bisnis.com.official.Fragment.FragmentDetailPromo;
import bisnis.com.official.Model.ModelPromo;
import bisnis.com.official.Pojo.PromoResponse;
import bisnis.com.official.R;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 09/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class PromoAdapter extends RecyclerView.Adapter {
    Context mContext;
    int total_types;
    FragmentManager fragmentManager;
    private List<PromoResponse> dataSet;
    private FragmentTransaction ft;

    public PromoAdapter(ArrayList<PromoResponse> data, Context context) {
        this.dataSet = data;
        this.mContext = context;
        total_types = dataSet.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case ModelPromo.PromoGrid:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_thumbnail_promo, parent, false);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                    }
                });

                return new PromoGricViewType(view);
            case ModelPromo.PromoList:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_thumbnail_promo, parent, false);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                return new PromoListViewType(view);


        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        switch (dataSet.get(position).type) {
            case 0:
                return ModelPromo.PromoList;
            case 1:
                return ModelPromo.PromoGrid;
            default:
                return -1;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {


        final PromoResponse object = dataSet.get(position);
        if (object != null) {
            switch (object.type) {

                case ModelPromo.PromoList:
                    ((PromoListViewType) holder).title_promo.setText(object.getTitle());
                    ((PromoListViewType) holder).from_promo.setText(object.getMerchantName());
                    Glide.with(mContext).load(object.getImageContentUrl()).into(((PromoListViewType) holder).imageView);
                    ((PromoListViewType) holder).img_more.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            PopupMenu popupMenu = new PopupMenu(mContext, ((PromoListViewType) holder).img_more);
                            popupMenu.inflate(R.menu.item_menu);
                            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    switch (item.getItemId()) {
                                        case R.id.share:

                                            Intent i = new Intent(Intent.ACTION_SEND);
                                            i.setType("text/plain");

                                            i.putExtra(Intent.EXTRA_TEXT, object.getTitle() + "\n" + object.getPromoUrl());
                                            mContext.startActivity(Intent.createChooser(i, "Share Via"));
                                            break;

                                    }
                                    return false;
                                }
                            });
                            popupMenu.show();

                        }
                    });

                    ((PromoListViewType) holder).imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String merchant = String.valueOf(object.getMerchantName());
                            String image = String.valueOf(object.getImageContentUrl());
                            String summary = String.valueOf(object.getSummary());
                            String detail = String.valueOf(object.getDetail());
                            String title = String.valueOf(object.getTitle());
                            String url_promo = String.valueOf(object.getPromoUrl());

                            Intent i = new Intent(mContext, FragmentDetailPromo.class);
                            i.putExtra("merchant", merchant);
                            i.putExtra("image", image);
                            i.putExtra("summary", summary);
                            i.putExtra("detail", detail);
                            i.putExtra("title", title);
                            i.putExtra("url_promo", url_promo);
                            mContext.startActivity(i);

//                            Bundle bundle = new Bundle();
//                            bundle.putString();
//                            bundle.putString();
//                            bundle.putString();
//                            bundle.putString();
//                            bundle.putString();
//                            bundle.putString();
//
//                            Fragment d2 = new FragmentDetailPromo();
//                            d2.setArguments(bundle);
//
//                            fragmentManager = ((MainActivity) mContext).getSupportFragmentManager();
//                            ft = fragmentManager.beginTransaction();
//                            ft.replace(R.id.container_body, d2, "home").addToBackStack("menu");
//                            ft.commit();
                        }
                    });
                    break;

                case ModelPromo.PromoGrid:
                    ((PromoGricViewType) holder).title_promo.setText(object.getTitle());
                    ((PromoGricViewType) holder).from_promo.setText(object.getMerchantName());
                    Glide.with(mContext).load(object.getImageContentUrl()).into(((PromoGricViewType) holder).imageView);
                    ((PromoGricViewType) holder).img_more.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            PopupMenu popupMenu = new PopupMenu(mContext, ((PromoGricViewType) holder).img_more);
                            popupMenu.inflate(R.menu.item_menu);
                            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    switch (item.getItemId()) {
                                        case R.id.share:

                                            Intent i = new Intent(Intent.ACTION_SEND);
                                            i.setType("text/plain");

                                            i.putExtra(Intent.EXTRA_TEXT, object.getTitle() + "\n" + object.getPromoUrl());
                                            mContext.startActivity(Intent.createChooser(i, "Share Via"));
                                            break;

                                    }
                                    return false;
                                }
                            });
                            popupMenu.show();

                        }
                    });
                    ((PromoGricViewType) holder).imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String merchant = String.valueOf(object.getMerchantName());
                            String image = String.valueOf(object.getImageContentUrl());
                            String summary = String.valueOf(object.getSummary());
                            String detail = String.valueOf(object.getDetail());
                            String url_promo = String.valueOf(object.getPromoUrl());
                            String title = String.valueOf(object.getTitle());

                            Intent i = new Intent(mContext, FragmentDetailPromo.class);
                            i.putExtra("merchant", merchant);
                            i.putExtra("image", image);
                            i.putExtra("summary", summary);
                            i.putExtra("detail", detail);
                            i.putExtra("title", title);
                            i.putExtra("url_promo", url_promo);
                            mContext.startActivity(i);
                        }
                    });
                    break;


            }
        }

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class PromoGricViewType extends RecyclerView.ViewHolder {

        TextView title_promo, from_promo;
        ImageView imageView, img_more;
        LinearLayout linearLayout;

        public PromoGricViewType(View itemView) {
            super(itemView);

            title_promo = itemView.findViewById(R.id.title_promo);
            from_promo = itemView.findViewById(R.id.promo_from);
            imageView = itemView.findViewById(R.id.img_promo);
            img_more = itemView.findViewById(R.id.img_more);
        }
    }

    public static class PromoListViewType extends RecyclerView.ViewHolder {

        TextView title_promo, from_promo;
        ImageView imageView, img_more;
        LinearLayout linearLayout;

        public PromoListViewType(View itemView) {
            super(itemView);

            title_promo = itemView.findViewById(R.id.title_promo);
            from_promo = itemView.findViewById(R.id.promo_from);
            imageView = itemView.findViewById(R.id.img_promo);
            img_more = itemView.findViewById(R.id.img_more);

        }
    }


}
