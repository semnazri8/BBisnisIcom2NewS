package bisnis.com.official.ViewHolder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import bisnis.com.official.R;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 27/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class KomenViewHolder extends RecyclerView.ViewHolder {
    public TextView txt_username_komen, txt_username_date, txt_komen, txt_coment_count, txt_comment_like;
    public ImageView user_image_komen,img_like;
    public LinearLayout ll_komen, ll_likes, ll_report;

    public KomenViewHolder(View itemView) {
        super(itemView);

        txt_username_komen = itemView.findViewById(R.id.txt_komen_name);
        txt_username_date = itemView.findViewById(R.id.tgl_komen);
        txt_komen = itemView.findViewById(R.id.user_komen);
        txt_coment_count = itemView.findViewById(R.id.coment_count);
        txt_comment_like = itemView.findViewById(R.id.likes_count);
        user_image_komen = itemView.findViewById(R.id.usr_img);
        ll_komen = itemView.findViewById(R.id.ll_comment);
        ll_likes = itemView.findViewById(R.id.ll_like);
        ll_report = itemView.findViewById(R.id.ll_report);
        img_like = itemView.findViewById(R.id.img_like);


    }
}
