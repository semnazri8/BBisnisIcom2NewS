package bisnis.com.official.ViewHolder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import bisnis.com.official.R;

public class BookmarkViewHolder extends RecyclerView.ViewHolder {

    public TextView title_berita, kanal_date, is_live;
    public ImageView img_berita;
    public LinearLayout ll_parent;

    public BookmarkViewHolder(View itemView) {
        super(itemView);
        title_berita = itemView.findViewById(R.id.title_berita);
        kanal_date = itemView.findViewById(R.id.kanal_time);
        img_berita = itemView.findViewById(R.id.img_berita);
        ll_parent = itemView.findViewById(R.id.parent);
        is_live = itemView.findViewById(R.id.is_live);
    }
}
