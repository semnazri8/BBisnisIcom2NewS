package bisnis.com.official.ViewHolder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import bisnis.com.official.R;

public class SearchViewHolder extends RecyclerView.ViewHolder{

    public ImageView img_berita;
    public TextView is_live,title_berita,kanal_time;
    public LinearLayout ll_parent;

    public SearchViewHolder(View itemView) {
        super(itemView);

        img_berita = itemView.findViewById(R.id.img_berita);
        ll_parent = itemView.findViewById(R.id.parent);
        is_live = itemView.findViewById(R.id.is_live);
        title_berita = itemView.findViewById(R.id.title_berita);
        kanal_time = itemView.findViewById(R.id.kanal_time);
    }
}
