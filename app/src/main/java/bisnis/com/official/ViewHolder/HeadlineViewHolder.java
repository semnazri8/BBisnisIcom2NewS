package bisnis.com.official.ViewHolder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import bisnis.com.official.R;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 27/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class HeadlineViewHolder extends RecyclerView.ViewHolder {
    public TextView title_berita, date_berita;
    public ImageView img_berita;

    public HeadlineViewHolder(View itemView) {
        super(itemView);
        img_berita = itemView.findViewById(R.id.img_berita);
        title_berita = itemView.findViewById(R.id.title_berita);
        date_berita = itemView.findViewById(R.id.date_berita);
    }
}
