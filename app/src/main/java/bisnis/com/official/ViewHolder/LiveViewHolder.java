package bisnis.com.official.ViewHolder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import bisnis.com.official.R;

public class LiveViewHolder extends RecyclerView.ViewHolder {

    public TextView txt_live_time,txt_live_title,webview_live;
    public ImageView img_live;
//    public WebView ;

    public LiveViewHolder(View itemView) {
        super(itemView);

        txt_live_time = itemView.findViewById(R.id.txt_live_time);
        txt_live_title = itemView.findViewById(R.id.txt_live_title);
        img_live = itemView.findViewById(R.id.img_live);
        webview_live = itemView.findViewById(R.id.webview_live);
    }
}
