package bisnis.com.official.ViewHolder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import bisnis.com.official.R;

public class HistoryViewHolder extends RecyclerView.ViewHolder{

    public ImageView remove;
    public TextView history_title;

    public HistoryViewHolder(View itemView) {
        super(itemView);

        remove = itemView.findViewById(R.id.img_clear);
        history_title = itemView.findViewById(R.id.src_txt);

    }
}
