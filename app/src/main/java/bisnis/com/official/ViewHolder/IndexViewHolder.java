package bisnis.com.official.ViewHolder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import bisnis.com.official.R;

public class IndexViewHolder extends RecyclerView.ViewHolder {
    public ImageView img_berita;
    public TextView is_live,title_berita,kanal_time;
    public LinearLayout ll;

    public IndexViewHolder(View itemView) {
        super(itemView);

        img_berita = itemView.findViewById(R.id.img_berita);
        is_live = itemView.findViewById(R.id.is_live);
        title_berita = itemView.findViewById(R.id.title_berita);
        kanal_time = itemView.findViewById(R.id.kanal_time);
        ll = itemView.findViewById(R.id.parent);
    }
}
