package bisnis.com.official.ViewHolder;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import bisnis.com.official.R;

/**
 * Created by Semmy
 * mr.shanky08@gmail.com on 26/02/18.
 *
 * @copyright 2016
 * PT.Bisnis Indonesia Sibertama
 */

public class BeritaTerkaitViewHolder extends RecyclerView.ViewHolder{
    public ImageView img_terkait;
    public TextView title_terkait,kanal_time;
    public BeritaTerkaitViewHolder(View itemView) {
        super(itemView);

        img_terkait = itemView.findViewById(R.id.img_berita);
        title_terkait = itemView.findViewById(R.id.title_berita);
        kanal_time = itemView.findViewById(R.id.kanal_time);

    }
}
