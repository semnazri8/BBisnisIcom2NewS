package bisnis.com.official.Listener;

import java.util.ArrayList;

import bisnis.com.official.Pojo.BreakingResponse;

public interface OnClickAdapterListener {

    void onClick(String position, String type_get, String kanal_id);
    void onClickAja(String position, String type_get, String kanal_id, ArrayList<BreakingResponse> dataSet);

    void onClickHeadline(String position, String type_get, String kanal_id);

}
