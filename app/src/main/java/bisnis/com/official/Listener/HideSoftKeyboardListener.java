package bisnis.com.official.Listener;

import androidx.appcompat.app.AppCompatActivity;

public interface HideSoftKeyboardListener {

    void hidesoftKeyboard(AppCompatActivity activity);
}
